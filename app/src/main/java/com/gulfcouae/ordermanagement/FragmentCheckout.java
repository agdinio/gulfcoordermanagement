package com.gulfcouae.ordermanagement;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.gulfcouae.ordermanagement.R;
import com.gulfcouae.ordermanagement.beans.Cart;
import com.gulfcouae.ordermanagement.beans.Customer;
import com.gulfcouae.ordermanagement.helpers.Common;
import com.gulfcouae.ordermanagement.helpers.Currency;
import com.gulfcouae.ordermanagement.interfaces.BaseFragmentImpl;
import com.gulfcouae.ordermanagement.interfaces.CheckoutImpl;
import com.gulfcouae.ordermanagement.persistents.CartDataSource;
import com.gulfcouae.ordermanagement.persistents.SharedPreferences;
import com.gulfcouae.ordermanagement.statics.Global;
import com.gulfcouae.ordermanagement.sync.ServiceOrder;
import com.gulfcouae.ordermanagement.utils.FontUtils;

import org.w3c.dom.Text;

import java.util.List;

/**
 * Created by relly on 8/2/15.
 */
public class FragmentCheckout extends Fragment implements BaseFragmentImpl {

    private View rootView;
    private LinearLayout shippingInfoContainer;
    private LinearLayout waitContainer;
    private TextView lblShippingAddress;
    private TextView txtShippingAddress;
    private TextView lblPayMethod;
    private TextView txtPayMethod;
    private TextView lblDeliveryTime;
    private TextView txtDeliveryTime;
    private TextView lblCheckoutTotal;
    private TextView txtCheckoutTotal;
    private ImageButton btnEditShippingAddress;
    private Button btnPlaceOrder;
    private SharedPreferences prefs;
    private Customer cust;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        View actionBarView = actionBar.getCustomView();
        ((TextView) actionBarView.findViewById(R.id.txtActionBarTitle)).setText(getString(R.string.actionbar_title_checkout));

        rootView = inflater.inflate(R.layout.fragment_checkout, container, false);

        shippingInfoContainer = (LinearLayout) rootView.findViewById(R.id.shippingInfoContainer);
        waitContainer = (LinearLayout) rootView.findViewById(R.id.waitContainer);
        lblShippingAddress = (TextView) rootView.findViewById(R.id.lblShippingAddress);
        lblPayMethod = (TextView) rootView.findViewById(R.id.lblPayMethod);
        lblDeliveryTime = (TextView) rootView.findViewById(R.id.lblDeliveryTime);
        lblCheckoutTotal = (TextView) rootView.findViewById(R.id.lblCheckoutTotal);

        txtShippingAddress = (TextView) rootView.findViewById(R.id.txtShippingAddress);
        txtPayMethod = (TextView) rootView.findViewById(R.id.txtPayMethod);
        txtDeliveryTime = (TextView) rootView.findViewById(R.id.txtDeliveryTime);
        txtCheckoutTotal = (TextView) rootView.findViewById(R.id.txtCheckoutTotal);
        btnEditShippingAddress = (ImageButton) rootView.findViewById(R.id.btnEditShippingAddress);
        btnEditShippingAddress.setOnClickListener(btnEditAddressClickListener);
        btnPlaceOrder = (Button) rootView.findViewById(R.id.btnPlaceOrder);
        btnPlaceOrder.setOnClickListener(btnPlaceOrderClickListener);
        prefs = new SharedPreferences(rootView.getContext());

        LayoutInflater inf = getLayoutInflater(savedInstanceState).cloneInContext(rootView.getContext());

        initValues();

        return rootView;
    }

    private void initValues() {
        cust = prefs.getCustomerInfo();
        if (cust != null) {

            CartDataSource dao = new CartDataSource(rootView.getContext());
            dao.open(false);
            List<Cart> carts = dao.getCartByCustomer(cust.getId());
            dao.close();

            double totalAmount = 0;
            if (carts != null && carts.size() > 0) {
                for (Cart c : carts) {
                    totalAmount += (c.getPrice() * c.getQuantity());
                }
            }

            txtShippingAddress.setText(String.format("%s, %s %s", cust.getAddress(), cust.getAreaName(), cust.getEmiratesName()));
            txtPayMethod.setText("Cash Only");
            txtDeliveryTime.setText("30 - 45 MINS");
            txtCheckoutTotal.setText(Currency.format(totalAmount));
        }
    }

    @SuppressLint("NewApi")
    private void placeOrder() {
        new ServiceOrder(rootView.getContext(), new CheckoutImpl() {
            @Override
            public void redirectPage(String result) {
                if (result != null) {

                    CartDataSource dao = new CartDataSource(rootView.getContext());
                    dao.open(true);
                    dao.clearItems(cust.getId());
                    dao.close();
                    Common.updateBadge(rootView.getContext());

                    FragmentManager fragmentManager = ((FragmentActivity) rootView.getContext()).getSupportFragmentManager();

                    FragmentTransaction trans = fragmentManager.beginTransaction();

                    Fragment frag = fragmentManager.findFragmentByTag(Global.FRAG_PLACEORDER);
                    if (frag == null) {
                        frag = new FragmentOrderConfirmation();
                    } else {
                        fragmentManager.beginTransaction().remove(frag).commit();
                        frag = new FragmentOrderConfirmation();
                    }

                    Bundle args = new Bundle();
                    args.putString("trans_no", result);
                    frag.setArguments(args);

                    trans.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                    trans.replace(R.id.content_frame, frag, Global.FRAG_PLACEORDER);
                    trans.addToBackStack(Global.FRAG_PLACEORDER);
                    trans.commit();

                } else {
                    new AlertDialog.Builder(rootView.getContext())
                            .setTitle("Failed")
                            .setMessage("Place Order failed. Please try again.")
                            .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .create()
                            .show();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private View.OnClickListener btnEditAddressClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction trans = fragmentManager.beginTransaction();

            Fragment frag = fragmentManager.findFragmentByTag(Global.FRAG_EDITSHIPPINGADDRESS);
            if (frag == null) {
                frag = new FragmentShippingInfo();
            }
            trans.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left);
            trans.replace(R.id.content_frame, frag, Global.FRAG_EDITSHIPPINGADDRESS);
            trans.addToBackStack(Global.FRAG_EDITSHIPPINGADDRESS);
            trans.commit();

        }
    };

    private View.OnClickListener btnPlaceOrderClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            new AlertDialog.Builder(rootView.getContext())
                    .setCancelable(false)
                    .setTitle("Confirm")
                    .setMessage("You are about to place an order. Do you want to continue?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            shippingInfoContainer.setVisibility(View.GONE);
                            waitContainer.setVisibility(View.VISIBLE);
                            placeOrder();
                        }
                    })
                    .setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.cancel();
                        }
                    })
                    .create()
                    .show();


        }
    };
}
