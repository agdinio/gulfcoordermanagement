package com.gulfcouae.ordermanagement.persistents;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by relly on 8/1/15.
 */
public class JAMSqlHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "ordermanagement.db";
    private static final int DATABASE_VERSION = 1;

    public static final String TBL_ORDER = "tbl_order";
    public static final String COL_ORD_ID = "_id";
    public static final String COL_ORD_CUSTOMER_ID = "customer_id";
    public static final String COL_ORD_ORDER_DATE = "order_date";
    public static final String COL_ORD_SHIPPING_ADDRESS = "shipping_address";
    public static final String COL_ORD_SHIPPING_DATE = "shipping_date";
    public static final String COL_ORD_SHIPPING_STATUS = "shipping_status";
    private static final String CREATE_TBL_ORDER = "create table "
            + TBL_ORDER + "( "
            + COL_ORD_ID + " integer primary key autoincrement, "
            + COL_ORD_CUSTOMER_ID + " integer not null, "
            + COL_ORD_ORDER_DATE + " date null, "
            + COL_ORD_SHIPPING_ADDRESS + " text null, "
            + COL_ORD_SHIPPING_DATE + " date null, "
            + COL_ORD_SHIPPING_STATUS + " text null "
            + ");";

    public static final String TBL_ORDER_DETAILS = "tbl_order_details";
    public static final String COL_ODD_ID = "_id";
    public static final String COL_ODD_ORDER_ID = "order_id";
    public static final String COL_ODD_PRODUCT_ID = "product_id";
    public static final String COL_ODD_QUANTITY = "quantity";
    private static final String CREATE_TBL_ORDER_DETAILS = "create table "
            + TBL_ORDER_DETAILS + "( "
            + COL_ODD_ID + " integer primary key autoincrement, "
            + COL_ODD_ORDER_ID + " integer not null, "
            + COL_ODD_PRODUCT_ID + " integer not null, "
            + COL_ODD_QUANTITY + " integer"
            + ");";


    public static final String TBL_CART = "tbl_cart";
    public static final String COL_CART_ID = "_id";
    public static final String COL_CART_CUSTOMER_ID = "customer_id";
    public static final String COL_CART_PRODUCT_ID = "product_id";
    public static final String COL_CART_PRODUCT_NAME = "product_name";
    public static final String COL_CART_PRICE = "price";
    public static final String COL_CART_QUANTITY = "quantity";
    public static final String COL_CART_IMAGE = "image";
    public static final String COL_CART_UOM_ID = "uom_id";
    public static final String CREATE_TBL_CART = "create table "
            + TBL_CART + "( "
            + COL_CART_ID + " integer primary key autoincrement, "
            + COL_CART_CUSTOMER_ID + " integer not null, "
            + COL_CART_PRODUCT_ID + " integer not null, "
            + COL_CART_PRODUCT_NAME + " integer not null, "
            + COL_CART_PRICE + " decimal(10,2) not null, "
            + COL_CART_QUANTITY + " integer not null, "
            + COL_CART_IMAGE + " text null, "
            + COL_CART_UOM_ID + " integer "
            + ");";

    public static final String TBL_PRODUCTUOM = "tbl_product_uom";
    public static final String COL_PRODUCTUOM_ID = "_id";
    public static final String COL_PRODUCTUOM_CART_ID = "cart_id";
    public static final String COL_PRODUCTUOM_PRODUCT_ID = "product_id";
    public static final String COL_PRODUCTUOM_UOM_ID = "uom_id";
    public static final String COL_PRODUCTUOM_UOM_CODE = "uom_code";
    public static final String COL_PRODUCTUOM_REGULAR_PRICE = "regular_price";
    public static final String COL_PRODUCTUOM_PROMO_PRICE = "promo_price";
    public static final String COL_PRODUCTUOM_PROMO_NAME = "promo_name";
    public static final String COL_PRODUCTUOM_PROMO_DETAILS = "promo_details";
    public static final String COL_PRODUCTUOM_QUANTITY = "quantity";
    public static final String CREATE_TBL_PRODUCTUOM = "create  table "
            + TBL_PRODUCTUOM + "( "
            + COL_PRODUCTUOM_ID + " integer primary key autoincrement, "
            + COL_PRODUCTUOM_CART_ID + " integer null, "
            + COL_PRODUCTUOM_PRODUCT_ID + " integer null, "
            + COL_PRODUCTUOM_UOM_ID + " integer null, "
            + COL_PRODUCTUOM_UOM_CODE + " text null, "
            + COL_PRODUCTUOM_REGULAR_PRICE + " decimal(10,2) null, "
            + COL_PRODUCTUOM_PROMO_PRICE + " decimal(10,2) null, "
            + COL_PRODUCTUOM_PROMO_NAME + " text null, "
            + COL_PRODUCTUOM_PROMO_DETAILS + " text null, "
            + COL_PRODUCTUOM_QUANTITY + " integer null "
            + ");";

    public JAMSqlHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //db.execSQL(CREATE_TBL_ORDER);
        //db.execSQL(CREATE_TBL_ORDER_DETAILS);
        db.execSQL(CREATE_TBL_CART);
        db.execSQL(CREATE_TBL_PRODUCTUOM);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(JAMSqlHelper.class.getName(),
                "Upgrading database from version " + oldVersion + " to "
                        + newVersion + ", which will destroy all old data");
        //db.execSQL("DROP TABLE IF EXISTS " + TBL_ORDER);
        //db.execSQL("DROP TABLE IF EXISTS " + TBL_ORDER_DETAILS);
        db.execSQL("DROP TABLE IF EXISTS " + TBL_CART);
        db.execSQL("DROP TABLE IF EXISTS " + TBL_PRODUCTUOM);
        onCreate(db);
    }
}
