package com.gulfcouae.ordermanagement.persistents;

import android.content.Context;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;

import com.gulfcouae.ordermanagement.beans.Customer;
import com.google.gson.Gson;
import com.google.gson.internal.Primitives;

import java.util.Map;
import java.util.Set;

/**
 * Created by relly on 7/26/15.
 */
public class SharedPreferences implements android.content.SharedPreferences {

    private Context context;
    private Editor editor;


    public SharedPreferences(Context context) {
        this.context = context;
    }


    public void putString(String key, String value) {
        editor.putString(key, value);
    }

    public void putBoolean(String key, boolean value) {
        editor.putBoolean(key, value);
    }

    public void putLong(String key, long value) {
        editor.putLong(key, value);
    }

    public void editMode() {
        editor = PreferenceManager.getDefaultSharedPreferences(context).edit();
    }
    public void commitMode() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.GINGERBREAD) {
            editor.apply();
        } else {
            editor.commit();
        }
    }

    @Override
    public Map<String, ?> getAll() {
        return null;
    }

    @Nullable
    @Override
    public String getString(String key, String defValue) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getString(key, defValue);
    }

    @Nullable
    @Override
    public Set<String> getStringSet(String key, Set<String> defValues) {
        return null;
    }

    @Override
    public int getInt(String key, int defValue) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getInt(key, defValue);
    }

    @Override
    public long getLong(String key, long defValue) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getLong(key, defValue);
    }

    @Override
    public float getFloat(String key, float defValue) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getFloat(key, defValue);
    }

    @Override
    public boolean getBoolean(String key, boolean defValue) {
        return PreferenceManager.getDefaultSharedPreferences(context)
                .getBoolean(key, defValue);
    }

    @Override
    public boolean contains(String key) {
        return false;
    }

    @Override
    public Editor edit() {
        return null;
    }


    @Override
    public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {

    }

    @Override
    public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener listener) {

    }

    public String toJson(Object src) {
        Gson gson = new Gson();
        return gson.toJson(src);
    }

    public <T> T fromJson(String json, Class<T> classOfT) {
        Gson gson = new Gson();

        Object object = gson.fromJson(json, classOfT);

        return Primitives.wrap(classOfT).cast(object);
    }

    public Customer getCustomerInfo() {

        String json = getString("customerinfo", "");
        Customer cust = fromJson(json, Customer.class);
        if (cust != null) {
            return cust;
        }

        return null;
    }

    public boolean isLoggedIn() {
        if (getCustomerInfo() != null) {
            return true;
        } else {
            return false;
        }
    }

    public boolean isRememberMe() {
        return getBoolean("rememberme", false);
    }

}
