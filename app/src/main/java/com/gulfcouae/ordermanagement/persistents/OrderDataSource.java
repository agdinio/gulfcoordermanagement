package com.gulfcouae.ordermanagement.persistents;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.gulfcouae.ordermanagement.beans.Order;

import java.sql.Date;

/**
 * Created by relly on 8/1/15.
 */
public class OrderDataSource {

    private SQLiteDatabase database;
    private JAMSqlHelper dbHelper;
    private String[] allOrderColumns = {
            JAMSqlHelper.COL_ORD_ID,
            JAMSqlHelper.COL_ORD_CUSTOMER_ID,
            JAMSqlHelper.COL_ORD_ORDER_DATE,
            JAMSqlHelper.COL_ORD_SHIPPING_ADDRESS,
            JAMSqlHelper.COL_ORD_SHIPPING_DATE,
            JAMSqlHelper.COL_ORD_SHIPPING_STATUS
    };
    private String[] allOrderDetailsColumns = {
            JAMSqlHelper.COL_ODD_ID,
            JAMSqlHelper.COL_ODD_ORDER_ID,
            JAMSqlHelper.COL_ODD_PRODUCT_ID,
            JAMSqlHelper.COL_ODD_QUANTITY
    };

    public OrderDataSource(Context context) {
        dbHelper = new JAMSqlHelper(context);
    }

    public void open(boolean writable) throws SQLException {
        if (writable) {
            database = dbHelper.getWritableDatabase();
        } else {
            database = dbHelper.getReadableDatabase();
        }
    }

    public void close() {
        dbHelper.close();
    }

    public Order createOrder(Order o) {
        ContentValues values = new ContentValues();
        values.put(JAMSqlHelper.COL_ORD_CUSTOMER_ID, o.getCustomerId());
        values.put(JAMSqlHelper.COL_ORD_ORDER_DATE, String.valueOf(o.getOrderDate()));
        values.put(JAMSqlHelper.COL_ORD_SHIPPING_ADDRESS, o.getShippingAddress());
        values.put(JAMSqlHelper.COL_ORD_SHIPPING_DATE, String.valueOf(o.getShippingDate()));
        values.put(JAMSqlHelper.COL_ORD_SHIPPING_STATUS, o.getShippingStatus());

        long insertId = database.insert(JAMSqlHelper.TBL_ORDER, null, values);
        Cursor cursor = database.query(JAMSqlHelper.TBL_ORDER,
                                        allOrderColumns,
                                        JAMSqlHelper.COL_ORD_ID + " = " + insertId,
                                        null, null, null, null);

        cursor.moveToFirst();
        Order order = new Order();
        order.setCustomerId(cursor.getInt(cursor.getColumnIndex(JAMSqlHelper.COL_ORD_ID)));
        order.setOrderDate(Date.valueOf(cursor.getString(cursor.getColumnIndex(JAMSqlHelper.COL_ORD_ORDER_DATE))));
        order.setShippingAddress(cursor.getString(cursor.getColumnIndex(JAMSqlHelper.COL_ORD_SHIPPING_ADDRESS)));
        order.setShippingDate(Date.valueOf(cursor.getString(cursor.getColumnIndex(JAMSqlHelper.COL_ORD_SHIPPING_DATE))));
        order.setShippingStatus(cursor.getInt(cursor.getColumnIndex(JAMSqlHelper.COL_ORD_SHIPPING_STATUS)));
        cursor.close();

        return order;
    }
}
