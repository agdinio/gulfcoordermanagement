package com.gulfcouae.ordermanagement.persistents;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.gulfcouae.ordermanagement.beans.Cart;
import com.gulfcouae.ordermanagement.beans.ProductUOM;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by relly on 8/1/15.
 */
public class CartDataSource {

    private SQLiteDatabase database;
    private JAMSqlHelper dbHelper;
    private String[] allColumnsCart = {
            JAMSqlHelper.COL_CART_ID,
            JAMSqlHelper.COL_CART_CUSTOMER_ID,
            JAMSqlHelper.COL_CART_PRODUCT_ID,
            JAMSqlHelper.COL_CART_PRODUCT_NAME,
            JAMSqlHelper.COL_CART_PRICE,
            JAMSqlHelper.COL_CART_QUANTITY,
            JAMSqlHelper.COL_CART_IMAGE,
            JAMSqlHelper.COL_CART_UOM_ID
    };
    private String[] allColumnsProductUOM = {
            JAMSqlHelper.COL_PRODUCTUOM_ID,
            JAMSqlHelper.COL_PRODUCTUOM_CART_ID,
            JAMSqlHelper.COL_PRODUCTUOM_PRODUCT_ID,
            JAMSqlHelper.COL_PRODUCTUOM_UOM_ID,
            JAMSqlHelper.COL_PRODUCTUOM_UOM_CODE,
            JAMSqlHelper.COL_PRODUCTUOM_REGULAR_PRICE,
            JAMSqlHelper.COL_PRODUCTUOM_PROMO_PRICE,
            JAMSqlHelper.COL_PRODUCTUOM_PROMO_NAME,
            JAMSqlHelper.COL_PRODUCTUOM_PROMO_DETAILS,
            JAMSqlHelper.COL_PRODUCTUOM_QUANTITY

    };

    public CartDataSource(Context context) {
        dbHelper = new JAMSqlHelper(context);
    }

    public void open(boolean writable) throws SQLException {
        if (writable) {
            database = dbHelper.getWritableDatabase();
        } else {
            database = dbHelper.getReadableDatabase();
        }
    }

    public void close() {
        dbHelper.close();
    }

    public long addToCart(Cart cart) {

        ContentValues values = new ContentValues();
        values.put(JAMSqlHelper.COL_CART_CUSTOMER_ID, cart.getCustomerId());
        values.put(JAMSqlHelper.COL_CART_PRODUCT_ID, cart.getProductId());
        values.put(JAMSqlHelper.COL_CART_PRODUCT_NAME, cart.getProductName());
        values.put(JAMSqlHelper.COL_CART_PRICE, cart.getPrice());
        values.put(JAMSqlHelper.COL_CART_QUANTITY, cart.getQuantity());
        values.put(JAMSqlHelper.COL_CART_IMAGE, cart.getImage());
        values.put(JAMSqlHelper.COL_CART_UOM_ID, cart.getUomId());

        long insertId = database.insert(JAMSqlHelper.TBL_CART, null, values);
        if (insertId > 0) {
            InsertProductUOM(insertId, cart.getProductUOMList());
        }

        return insertId;
    }

    public int updateCartQuantity(Cart cart) {

        ContentValues values = new ContentValues();
        values.put(JAMSqlHelper.COL_CART_QUANTITY, cart.getQuantity());

        String condition = String.format("%s=%d AND %s=%d AND %s=%d",
                JAMSqlHelper.COL_CART_CUSTOMER_ID, cart.getCustomerId(),
                JAMSqlHelper.COL_CART_PRODUCT_ID, cart.getProductId(),
                JAMSqlHelper.COL_CART_ID, cart.getId(),
                JAMSqlHelper.COL_CART_UOM_ID, cart.getUomId());
        //String condition = String.format("%s=%d", );

        return database.update(JAMSqlHelper.TBL_CART, values, condition, null);
    }

    public int deleteItem(Cart cart) {
        String condition = String.format("%s=%d AND %s=%d AND %s=%d AND %s=%d",
                JAMSqlHelper.COL_CART_CUSTOMER_ID, cart.getCustomerId(),
                JAMSqlHelper.COL_CART_PRODUCT_ID, cart.getProductId(),
                JAMSqlHelper.COL_CART_ID, cart.getId(),
                JAMSqlHelper.COL_CART_UOM_ID, cart.getUomId());
        return database.delete(JAMSqlHelper.TBL_CART, condition, null);
    }

    public int deleteProductUOM(Cart cart) {
        String condition = String.format("%s=%d AND %s=%d",
                JAMSqlHelper.COL_PRODUCTUOM_CART_ID, cart.getId(),
                JAMSqlHelper.COL_PRODUCTUOM_PRODUCT_ID, cart.getProductId());
        return database.delete(JAMSqlHelper.TBL_PRODUCTUOM, condition, null);
    }

    public Cart getCartByCartId(long cartId) {
        Cart c = null;
        Cursor cursor = database.query(JAMSqlHelper.TBL_CART,
                allColumnsCart,
                JAMSqlHelper.COL_CART_ID + "=" + cartId,
                null, null, null, null);

        if (cursor.moveToFirst()) {
            c = new Cart();
            c.setId(cursor.getInt(0));
            c.setCustomerId(cursor.getInt(cursor.getColumnIndex(JAMSqlHelper.COL_CART_CUSTOMER_ID)));
            c.setProductId(cursor.getInt(cursor.getColumnIndex(JAMSqlHelper.COL_CART_PRODUCT_ID)));
            c.setProductName(cursor.getString(cursor.getColumnIndex(JAMSqlHelper.COL_CART_PRODUCT_NAME)));
            c.setPrice(cursor.getDouble(cursor.getColumnIndex(JAMSqlHelper.COL_CART_PRICE)));
            c.setQuantity(cursor.getInt(cursor.getColumnIndex(JAMSqlHelper.COL_CART_QUANTITY)));
            c.setImage(cursor.getString(cursor.getColumnIndex(JAMSqlHelper.COL_CART_IMAGE)));
            c.setUomId(cursor.getInt(cursor.getColumnIndex(JAMSqlHelper.COL_CART_UOM_ID)));
        }

        return c;
    }

    public Cart getCartByProductId(int customerId, int productId, int uomId) {
        Cart c = null;
        Cursor cursor = database.query(JAMSqlHelper.TBL_CART,
                allColumnsCart,
                JAMSqlHelper.COL_CART_PRODUCT_ID + "=" + productId +
                        " AND " + JAMSqlHelper.COL_CART_CUSTOMER_ID + "=" + customerId +
                        " AND " + JAMSqlHelper.COL_CART_UOM_ID + "=" + uomId,
                null, null, null, null);

        if (cursor.moveToFirst()) {
            c = new Cart();
            c.setId( cursor.getInt(0)  );
            c.setCustomerId(cursor.getInt(cursor.getColumnIndex(JAMSqlHelper.COL_CART_CUSTOMER_ID)));
            c.setProductId(cursor.getInt(cursor.getColumnIndex(JAMSqlHelper.COL_CART_PRODUCT_ID)));
            c.setProductName(cursor.getString(cursor.getColumnIndex(JAMSqlHelper.COL_CART_PRODUCT_NAME)));
            c.setPrice(cursor.getDouble(cursor.getColumnIndex(JAMSqlHelper.COL_CART_PRICE)));
            c.setQuantity(cursor.getInt(cursor.getColumnIndex(JAMSqlHelper.COL_CART_QUANTITY)));
            c.setImage(cursor.getString(cursor.getColumnIndex(JAMSqlHelper.COL_CART_IMAGE)));
        }

        return c;
    }

    public List<Cart> getCartByCustomer(int customerId) {
        List<Cart> carts = null;

        Cursor cursor = database.query(JAMSqlHelper.TBL_CART,
                allColumnsCart,
                JAMSqlHelper.COL_CART_CUSTOMER_ID + " = " + customerId,
                null, null, null, null);

        if (cursor.moveToFirst()) {
            carts = new ArrayList<>();
            do {
                Cart c = new Cart();
                c.setId(cursor.getInt(0));
                c.setCustomerId(cursor.getInt(cursor.getColumnIndex(JAMSqlHelper.COL_CART_CUSTOMER_ID)));
                c.setProductId(cursor.getInt(cursor.getColumnIndex(JAMSqlHelper.COL_CART_PRODUCT_ID)));
                c.setProductName(cursor.getString(cursor.getColumnIndex(JAMSqlHelper.COL_CART_PRODUCT_NAME)));
                c.setPrice(cursor.getDouble(cursor.getColumnIndex(JAMSqlHelper.COL_CART_PRICE)));
                c.setQuantity(cursor.getInt(cursor.getColumnIndex(JAMSqlHelper.COL_CART_QUANTITY)));
                c.setImage(cursor.getString(cursor.getColumnIndex(JAMSqlHelper.COL_CART_IMAGE)));
                c.setUomId(cursor.getInt(cursor.getColumnIndex(JAMSqlHelper.COL_CART_UOM_ID)));

                int cartId = cursor.getInt(0);
                int prodId = cursor.getInt(cursor.getColumnIndex(JAMSqlHelper.COL_CART_PRODUCT_ID));

                c.setProductUOMList(getProductUOMList(cartId, customerId, prodId));

                carts.add(c);

            } while (cursor.moveToNext());
        }

        return carts;
    }

    public int clearItems(int customerId) {
        String condition = String.format("%s=%d",
                JAMSqlHelper.COL_CART_CUSTOMER_ID, customerId);
        return database.delete(JAMSqlHelper.TBL_CART, condition, null);
    }

    //====================================== PRODUCT UOM ===============================================

    public int InsertProductUOM(long insertId, List<ProductUOM> list) {

        for (ProductUOM o : list) {
            ContentValues values = new ContentValues();

            values.put(JAMSqlHelper.COL_PRODUCTUOM_CART_ID, insertId);
            values.put(JAMSqlHelper.COL_PRODUCTUOM_PRODUCT_ID, o.getProductId());
            values.put(JAMSqlHelper.COL_PRODUCTUOM_UOM_ID, o.getUomId());
            values.put(JAMSqlHelper.COL_PRODUCTUOM_UOM_CODE, o.getUomCode());
            values.put(JAMSqlHelper.COL_PRODUCTUOM_REGULAR_PRICE, o.getRegularPrice());
            values.put(JAMSqlHelper.COL_PRODUCTUOM_PROMO_NAME, o.getPromoName());
            values.put(JAMSqlHelper.COL_PRODUCTUOM_PROMO_PRICE, o.getPromoPrice());
            values.put(JAMSqlHelper.COL_PRODUCTUOM_PROMO_DETAILS, o.getPromoDetails());
            values.put(JAMSqlHelper.COL_PRODUCTUOM_QUANTITY, o.getQuantity());

            database.insert(JAMSqlHelper.TBL_PRODUCTUOM, null, values);
        }


        return 0;
    }

    public List<ProductUOM> getProductUOMList(int cartId, int custId, int prodId) {
        List<ProductUOM> list = null;

        String condition = String.format("%s=%d AND %s=%d",
                JAMSqlHelper.COL_PRODUCTUOM_CART_ID, cartId,
                JAMSqlHelper.COL_PRODUCTUOM_PRODUCT_ID, prodId);

        Cursor cursor = database.query(JAMSqlHelper.TBL_PRODUCTUOM,
                allColumnsProductUOM,
                condition,
                null, null, null, null);

        if (cursor.moveToFirst()) {
            list = new ArrayList<>();
            do {
                ProductUOM o = new ProductUOM();
                o.setProductId(cursor.getInt(cursor.getColumnIndex(JAMSqlHelper.COL_PRODUCTUOM_PRODUCT_ID)));
                o.setUomId(cursor.getInt(cursor.getColumnIndex(JAMSqlHelper.COL_PRODUCTUOM_UOM_ID)));
                o.setUomCode(cursor.getString(cursor.getColumnIndex(JAMSqlHelper.COL_PRODUCTUOM_UOM_CODE)));
                o.setRegularPrice(cursor.getDouble(cursor.getColumnIndex(JAMSqlHelper.COL_PRODUCTUOM_REGULAR_PRICE)));
                o.setPromoPrice(cursor.getDouble(cursor.getColumnIndex(JAMSqlHelper.COL_PRODUCTUOM_PROMO_PRICE)));
                o.setPromoName(cursor.getString(cursor.getColumnIndex(JAMSqlHelper.COL_PRODUCTUOM_PROMO_NAME)));
                o.setPromoDetails(cursor.getString(cursor.getColumnIndex(JAMSqlHelper.COL_PRODUCTUOM_PROMO_DETAILS)));
                o.setQuantity(cursor.getInt(cursor.getColumnIndex(JAMSqlHelper.COL_PRODUCTUOM_QUANTITY)));
                list.add(o);
            } while (cursor.moveToNext());
        }

        return list;
    }

}
