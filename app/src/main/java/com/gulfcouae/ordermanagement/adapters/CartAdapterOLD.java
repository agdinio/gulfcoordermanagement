package com.gulfcouae.ordermanagement.adapters;

import android.content.Context;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.gulfcouae.ordermanagement.R;
import com.gulfcouae.ordermanagement.beans.Cart;
import com.gulfcouae.ordermanagement.beans.Customer;
import com.gulfcouae.ordermanagement.beans.ProductUOM;
import com.gulfcouae.ordermanagement.helpers.Common;
import com.gulfcouae.ordermanagement.helpers.Currency;
import com.gulfcouae.ordermanagement.helpers.ThreadHandler;
import com.gulfcouae.ordermanagement.interfaces.MyCartDetailsImpl;
import com.gulfcouae.ordermanagement.persistents.CartDataSource;
import com.gulfcouae.ordermanagement.persistents.SharedPreferences;
import com.gulfcouae.ordermanagement.sync.DownloadImage;

import java.util.List;

/**
 * Created by relly on 8/2/15.
 */
public class CartAdapterOLD extends BaseAdapter {
    private Context context;
    private List<Cart> carts;
    private MyCartDetailsImpl callback;

    public CartAdapterOLD(Context context, MyCartDetailsImpl callback, List<Cart> carts) {
        this.context = context;
        this.carts = carts;
        this.callback = callback;
    }

    @Override
    public int getCount() {
        return carts.size();
    }

    @Override
    public Object getItem(int position) {
        return carts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final int finalPos = position;
        CartHolder holder = new CartHolder();
        Cart c = carts.get(position);

        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.mycart_list_item, null);

            holder.cboQuantity = (Spinner) v.findViewById(R.id.cboQuantity);
            holder.btnUpdate = (Button) v.findViewById(R.id.btnUpdate);
            holder.btnRemove = (Button) v.findViewById(R.id.btnRemove);
            holder.txtProductName = (TextView) v.findViewById(R.id.txtProductName);
            holder.txtProductPrice = (TextView) v.findViewById(R.id.txtProductPrice);
            holder.imgProduct = (ImageView) v.findViewById(R.id.imgProduct);
            holder.txtCartId = (TextView) v.findViewById(R.id.txtCartId);
            holder.txtProductId = (TextView) v.findViewById(R.id.txtProductId);
            holder.cboUOM = (Spinner) v.findViewById(R.id.cboUOM);
            holder.cboUOM.setEnabled(false);

            v.setTag(holder);
        } else {
            holder = (CartHolder) v.getTag();
        }

        holder.txtProductName.setText(c.getProductName());
        holder.txtProductPrice.setText(Currency.format(c.getPrice()));
        holder.txtCartId.setText(String.valueOf(c.getId()));
        holder.txtProductId.setText(String.valueOf(c.getProductId()));
        holder.btnUpdate.setTag(position);
        holder.btnUpdate.setOnClickListener(updateClickListener);
        holder.btnRemove.setTag(position);
        holder.btnRemove.setOnClickListener(removeClickListener);
        new DownloadImage(holder.imgProduct, context).execute(c.getImage());

        final CartHolder finalHolder = holder;
        final int finalQty = c.getQuantity();

        holder.cboQuantity.setAdapter(Common.quantityArrayAdapter(context));
        holder.cboQuantity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                int new_qty = Integer.parseInt(finalHolder.cboQuantity.getSelectedItem().toString());

                ProductUOM o = (ProductUOM) finalHolder.cboUOM.getSelectedItem();
                double totalPrice = (o.getPromoPrice() > 0 ? o.getPromoPrice() : o.getRegularPrice()) * Integer.parseInt(finalHolder.cboQuantity.getSelectedItem().toString());

                finalHolder.txtProductPrice.setText(Currency.format(totalPrice));

                carts.get(finalPos).setQuantity(new_qty);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        new ThreadHandler(context).post(new Runnable() {
            @Override
            public void run() {
                final int pos = ((ArrayAdapter<Integer>) finalHolder.cboQuantity.getAdapter()).getPosition(finalQty);
                finalHolder.cboQuantity.setSelection(pos);
            }
        });

        int apos = 0;
        for (ProductUOM o : c.getProductUOMList()) {
            if (o.getUomId() == c.getUomId()) {
                apos = c.getProductUOMList().indexOf(o);
                break;
            }
        }


        ArrayAdapter<ProductUOM> prom_adapter = new ArrayAdapter(context,
                                                            R.layout.base_dropdown,
                                                            c.getProductUOMList());
        holder.cboUOM.setAdapter(prom_adapter);
        holder.cboUOM.setSelection(apos);
        holder.cboUOM.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                try {
                    ProductUOM selProm = (ProductUOM) parent.getItemAtPosition(position);

                    double totalPrice = selProm.getPromoPrice() * Integer.parseInt(finalHolder.cboQuantity.getSelectedItem().toString());

                    carts.get(finalPos).setUomId(selProm.getUomId());

                    finalHolder.txtProductPrice.setText(Currency.format(totalPrice));

                } catch (Exception e) {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


//        final CartHolder finalHolder = holder;
//        holder.txtQuantity.addTextChangedListener(new TextWatcher() {
//            @Override
//            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//            }
//
//            @Override
//            public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//            }
//
//            @Override
//            public void afterTextChanged(Editable s) {
//                int pos = finalHolder.txtQuantity.getId();
//                int initval = 0;
//                if (TextUtils.isEmpty(finalHolder.txtQuantity.getText())
//                        || finalHolder.txtQuantity.getText().toString().equals("0")) {
//                    initval = 1;
//                } else {
//                    initval = Integer.parseInt(finalHolder.txtQuantity.getText().toString());
//                }
//                carts.get(position).setQuantity(initval);
//            }
//        });


        return v;
    }


    private View.OnClickListener updateClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Common.hideKeyboard(context);
            int pos = (int) v.getTag();

            Cart cart = carts.get(pos);
            SharedPreferences prefs = new SharedPreferences(context);
            Customer cust = prefs.getCustomerInfo();

            if (cust != null) {
                if (cart.getQuantity() < 1) {
                    new ThreadHandler(context).sendEmptyMessage(ThreadHandler.CART_QUANTITY_ZERO);
                    return;
                }

                CartDataSource dao = new CartDataSource(context);
                try {
                    dao.open(true);
                    cart.setCustomerId(cust.getId());
                    cart.setProductId(cart.getProductId());
                    cart.setQuantity(cart.getQuantity());
                    int result = dao.updateCartQuantity(cart);
                    if (result < 1) {
                        new ThreadHandler(context).sendMessage(Message.obtain(null, ThreadHandler.CART_UPDATE_FAILED, cart));
                    } else {
                        double totalAmount = 0;
                        for (Cart c : carts) {
                            totalAmount += (c.getPrice() * c.getQuantity());
                        }
                        callback.refreshCartSubtotal(carts.size(), totalAmount);

                        new ThreadHandler(context).sendEmptyMessage(ThreadHandler.CART_UPDATE_SUCCESS);
                    }

                } catch (Exception e) {
                    Toast.makeText(context, "Saving failed. Please try again.", Toast.LENGTH_LONG).show();
                } finally {
                    dao.close();
                }

            }


        }
    };

    private View.OnClickListener removeClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Common.hideKeyboard(context);
            int pos = (int) v.getTag();

            Cart cart = carts.get(pos);
            SharedPreferences prefs = new SharedPreferences(context);
            Customer cust = prefs.getCustomerInfo();

            if (cust != null) {

                CartDataSource dao = new CartDataSource(context);
                try {
                    dao.open(true);
                    dao.deleteItem(cart);
                    dao.deleteProductUOM(cart);

                    double totalAmount = 0;
                    carts = dao.getCartByCustomer(cust.getId());
                    if (carts != null && carts.size() > 0) {
                        for (Cart c : carts) {
                            totalAmount += (c.getPrice() * c.getQuantity());
                        }

                        dao.close();

                        callback.refreshCartSubtotal(carts.size(), totalAmount);
                        callback.refreshCartList();
                    } else {
                        //TODO:
                        callback.refreshLayout(0);
                        Common.updateBadge(context);
                    }

                } catch (Exception e) {
                    Toast.makeText(context, "Deletion failed. Please try again.", Toast.LENGTH_LONG).show();
                } finally {
                    dao.close();
                }

            }

        }
    };

    private class CartHolder {
        private Button btnUpdate;
        private Button btnRemove;
        private TextView txtProductName;
        private TextView txtProductPrice;
        private ImageView imgProduct;
        private TextView txtCartId;
        private TextView txtProductId;
        private Spinner cboQuantity;
        private Spinner cboUOM;

    }
}
