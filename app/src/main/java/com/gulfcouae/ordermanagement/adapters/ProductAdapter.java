package com.gulfcouae.ordermanagement.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gulfcouae.ordermanagement.R;
import com.gulfcouae.ordermanagement.beans.Product;
import com.gulfcouae.ordermanagement.helpers.Currency;
import com.gulfcouae.ordermanagement.sync.DownloadImage;

import java.util.List;

/**
 * Created by relly on 7/29/15.
 */
public class ProductAdapter extends BaseAdapter {

    private Context context;
    private List<Product> products;

    public ProductAdapter(Context context, List<Product> products) {
        this.context = context;
        this.products = products;
    }

    @Override
    public int getCount() {
        return products.size();
    }

    @Override
    public Object getItem(int position) {
        return products.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ProductHolder holder = new ProductHolder();
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.product_list_item, null);

            holder.txtProductId = (TextView) v.findViewById(R.id.txtProductId);
            holder.txtName = (TextView) v.findViewById(R.id.txtName);
            holder.txtDescription = (TextView) v.findViewById(R.id.txtDescription);
            holder.imgProduct = (ImageView) v.findViewById(R.id.imgProduct);
            holder.imgPromo = (ImageView) v.findViewById(R.id.imgPromo);

            v.setTag(holder);
        } else {
            holder = (ProductHolder) v.getTag();
        }

        Product p = products.get(position);
        holder.txtProductId.setText(String.valueOf(p.getId()));
        holder.txtName.setText(p.getName());
        holder.txtDescription.setText(p.getDescription());
        if (p.isPromo()) {
            holder.imgPromo.setVisibility(View.VISIBLE);
        } else {
            holder.imgPromo.setVisibility(View.GONE);
        }

        new DownloadImage(holder.imgProduct, context).execute(p.getImage());

        return v;
    }

    private static class ProductHolder {
        public TextView txtProductId;
        public TextView txtName;
        public TextView txtDescription;
        public ImageView imgProduct;
        public ImageView imgPromo;
    }

}
