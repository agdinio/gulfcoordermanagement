package com.gulfcouae.ordermanagement.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gulfcouae.ordermanagement.R;
import com.gulfcouae.ordermanagement.beans.Emirate;

import java.util.List;

/**
 * Created by relly on 8/5/15.
 */
public class EmirateAdapter extends ArrayAdapter<Emirate> {

    private Context context;
    private List<Emirate> emirates;


    public EmirateAdapter(Context context, int resource, List<Emirate> objects) {
        super(context, resource, objects);
        this.context = context;
        this.emirates = objects;
    }


    @Override
    public int getCount() {
        return emirates.size();
    }


    @Override
    public long getItemId(int position) {
        return 0;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.dropdown_emirate, null);

            TextView txtEmirate = (TextView) v.findViewById(R.id.txtEmirate);
            txtEmirate.setText(emirates.get(position).getName());
        }


        return v;
    }
}
