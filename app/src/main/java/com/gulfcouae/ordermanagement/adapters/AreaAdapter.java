package com.gulfcouae.ordermanagement.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gulfcouae.ordermanagement.R;
import com.gulfcouae.ordermanagement.beans.Area;

import java.util.List;

/**
 * Created by relly on 8/5/15.
 */
public class AreaAdapter extends BaseAdapter {


    private Context context;
    private List<Area> areas;

    public AreaAdapter(Context context, List<Area> objects) {
        this.context = context;
        this.areas = objects;
    }

    @Override
    public int getCount() {
        return areas.size();
    }

    @Override
    public Object getItem(int position) {
        return areas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.dropdown_area, null);

            TextView txtArea = (TextView) convertView.findViewById(R.id.txtArea);
            txtArea.setText(areas.get(position).getName());

        }

        return convertView;
    }


}
