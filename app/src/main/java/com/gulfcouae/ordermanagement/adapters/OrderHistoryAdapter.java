package com.gulfcouae.ordermanagement.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.gulfcouae.ordermanagement.R;
import com.gulfcouae.ordermanagement.beans.Order;
import com.gulfcouae.ordermanagement.helpers.Currency;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by relly on 8/11/15.
 */
public class OrderHistoryAdapter extends BaseAdapter {

    private Context context;
    private List<Order> orders;

    public OrderHistoryAdapter(Context context, List<Order> orders) {
        this.context = context;
        this.orders = orders;
    }

    @Override
    public int getCount() {
        return orders.size();
    }

    @Override
    public Object getItem(int position) {
        return orders.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        OrderHistoryHolder holder = new OrderHistoryHolder();
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.myorderhistory_list_item, null);

            holder.txtOrderId = (TextView) v.findViewById(R.id.txtOrderId);
            holder.txtTransNo = (TextView) v.findViewById(R.id.txtTransNo);
            holder.txtOrderDate = (TextView) v.findViewById(R.id.txtOrderDate);
            holder.txtStatus = (TextView) v.findViewById(R.id.txtStatus);
            holder.txtTotalAmount = (TextView) v.findViewById(R.id.txtTotalAmount);

            v.setTag(holder);

        } else {
            holder = (OrderHistoryHolder) v.getTag();
        }

        SimpleDateFormat dateformatter = new SimpleDateFormat("MMM dd, yyyy HH:mm");

        Order order = orders.get(position);
        holder.txtOrderId.setText(String.valueOf(order.getId()));
        holder.txtTransNo.setText(order.getTransactionNo());
        holder.txtOrderDate.setText(dateformatter.format(order.getOrderDate()));
        holder.txtTotalAmount.setText(Currency.format(order.getTotalAmount()));
        holder.txtStatus.setText(order.getShippingStatusName());
        if (order.getShippingStatusName().equalsIgnoreCase("NEW"))
            holder.txtStatus.setTextColor(context.getResources().getColor(R.color.accent));
        else
            holder.txtStatus.setTextColor(context.getResources().getColor(R.color.primary));


        return v;
    }

    public static class OrderHistoryHolder {
        public TextView txtOrderId;
        public TextView txtTransNo;
        public TextView txtOrderDate;
        public TextView txtStatus;
        public TextView txtTotalAmount;
    }

}
