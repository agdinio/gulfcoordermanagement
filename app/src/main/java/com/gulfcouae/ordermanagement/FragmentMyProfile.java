package com.gulfcouae.ordermanagement;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.gulfcouae.ordermanagement.R;
import com.gulfcouae.ordermanagement.adapters.AreaAdapter;
import com.gulfcouae.ordermanagement.beans.Area;
import com.gulfcouae.ordermanagement.beans.Customer;
import com.gulfcouae.ordermanagement.beans.Emirate;
import com.gulfcouae.ordermanagement.helpers.ThreadHandler;
import com.gulfcouae.ordermanagement.interfaces.AreaImpl;
import com.gulfcouae.ordermanagement.interfaces.BaseFragmentImpl;
import com.gulfcouae.ordermanagement.interfaces.CustomerImpl;
import com.gulfcouae.ordermanagement.interfaces.EmirateImpl;
import com.gulfcouae.ordermanagement.persistents.SharedPreferences;
import com.gulfcouae.ordermanagement.sync.ServiceArea;
import com.gulfcouae.ordermanagement.sync.ServiceEmirates;
import com.gulfcouae.ordermanagement.sync.ServiceUpateCustomer;
import com.gulfcouae.ordermanagement.sync.ServiceUpateCustomerPassword;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by relly on 7/27/15.
 */
public class FragmentMyProfile extends Fragment implements BaseFragmentImpl {

    private ScrollView rootContainer;
    private SharedPreferences prefs;
    private View rootView;
    private EditText txtFirstName;
    private EditText txtLastName;
    private EditText txtMobile;
    private EditText txtEmail;
    private EditText txtUsername;
    private EditText txtPassword;
    private Spinner cboEmirates;
    private Spinner cboArea;
    private EditText txtAddress;
    private ProgressBar pbArea;

    private Button btnUpdate;
    private ImageView btnEditPassword;
    private Customer activeCustomer;
    private int epos;
    private int apos;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        View actionBarView = actionBar.getCustomView();
        ((TextView) actionBarView.findViewById(R.id.txtActionBarTitle)).setText(getString(R.string.actionbar_title_myprofile));

        rootView = inflater.inflate(R.layout.fragment_myprofile, container, false);

        rootContainer = (ScrollView) rootView.findViewById(R.id.rootContainer);
        txtFirstName = (EditText) rootView.findViewById(R.id.txtFirstName);
        txtLastName = (EditText) rootView.findViewById(R.id.txtLastName);
        txtMobile = (EditText) rootView.findViewById(R.id.txtMobile);
        txtEmail = (EditText) rootView.findViewById(R.id.txtEmail);
        txtUsername = (EditText) rootView.findViewById(R.id.txtUsername);
        txtPassword = (EditText) rootView.findViewById(R.id.txtPassword);
        txtAddress = (EditText) rootView.findViewById(R.id.txtAddress);
        cboEmirates = (Spinner) rootView.findViewById(R.id.cboEmirate);
        cboArea = (Spinner) rootView.findViewById(R.id.cboArea);
        pbArea = (ProgressBar) rootView.findViewById(R.id.pbArea);

        btnUpdate = (Button) rootView.findViewById(R.id.btnUpdate);
        btnUpdate.setOnClickListener(btnUpdateClickListener);

        btnEditPassword = (ImageButton) rootView.findViewById(R.id.btnEditPassword);
        btnEditPassword.setOnClickListener(btnEditPasswordClickListener);

        prefs = new SharedPreferences(rootView.getContext());
        activeCustomer = prefs.getCustomerInfo();
        if (activeCustomer != null) {
            txtFirstName.setText(activeCustomer.getFirstName());
            txtLastName.setText(activeCustomer.getLastName());
            txtMobile.setText(activeCustomer.getMobile());
            txtEmail.setText(activeCustomer.getEmail());
            txtUsername.setText(activeCustomer.getUsername());
            txtPassword.setText("01010101010101010101");
            txtAddress.setText(activeCustomer.getAddress());
            btnUpdate.setEnabled(true);
        } else {
            btnUpdate.setEnabled(false);
        }

        new ThreadHandler(rootView.getContext()).postDelayed(new Runnable() {
            @Override
            public void run() {
                initEmirate();
            }
        }, 500);


        return rootView;
    }

    private void initEmirate() {
        new ServiceEmirates(rootView.getContext(), new EmirateImpl.Null() {
            @Override
            public void refreshList(List<Emirate> emirates) {
                if (emirates != null && emirates.size() > 0) {

                    rootContainer.setVisibility(View.VISIBLE);

                    Emirate blank = new Emirate();
                    blank.setId(0);
                    blank.setName("None");
                    emirates.add(0, blank);

                    Emirate currEmirate = null;
                    if (activeCustomer.getEmirate() != null) {
                        for (Emirate e : emirates) {
                            if (e.getId() == activeCustomer.getEmirate().getId()) {
                                epos = emirates.indexOf(e);
                                currEmirate = e;
                                break;
                            }
                        }
                    }

                    ArrayAdapter<Emirate> adapter = new ArrayAdapter(rootView.getContext(),
                            R.layout.dropdown_emirate,
                            emirates);
                    cboEmirates.setAdapter(adapter);
                    initDefaultArea(currEmirate);

                    cboEmirates.setSelection(epos);
                } else {
                    rootContainer.setVisibility(View.INVISIBLE);
                }
                initEmptyArea();
            }
        }).execute();
    }

    private void initEmptyArea() {
        Area blank = new Area();
        blank.setId(0);
        blank.setName("None");
        List<Area> areas = new ArrayList();
        areas.add(blank);

        ArrayAdapter<Area> adapter = new ArrayAdapter(rootView.getContext(),
                R.layout.dropdown_area,
                areas);

        cboArea.setAdapter(adapter);

    }

    private void initDefaultArea(Emirate emirate) {
        if (emirate != null && emirate.getId() > 0) {

            new ServiceArea(rootView.getContext(), pbArea, new AreaImpl() {
                @Override
                public void refreshList(List<Area> areas) {
                    if (areas != null) {
                        for (Area a : areas) {
                            if (a.getId() == activeCustomer.getArea().getId()) {
                                apos = areas.indexOf(a);
                                break;
                            }
                        }

                        AreaAdapter adapter = new AreaAdapter(rootView.getContext(), areas);
                        cboArea.setAdapter(adapter);
                        cboArea.setSelection(apos);
                    } else {
                        initEmptyArea();
                    }

                    cboEmirates.setOnItemSelectedListener(cboEmirateSelectedListener);
                }
            }).execute(emirate.getId());

        } else {
            initEmptyArea();
        }

    }

    private void loadAreas(Emirate emirate) {

        if (emirate != null && emirate.getId() > 0) {

            new ServiceArea(rootView.getContext(), pbArea, new AreaImpl() {
                @Override
                public void refreshList(List<Area> areas) {
                    if (areas != null) {
                        ArrayAdapter<Area> adapter = new ArrayAdapter(rootView.getContext(),
                                R.layout.dropdown_area,
                                areas);

                        cboArea.setAdapter(adapter);
                    } else {
                        initEmptyArea();
                    }

                }
            }).execute(emirate.getId());

        } else {
            initEmptyArea();
        }
    }

    AdapterView.OnItemSelectedListener cboEmirateSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Emirate emr = (Emirate) parent.getItemAtPosition(position);
            loadAreas(emr);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    View.OnClickListener btnEditPasswordClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            final Dialog dialog = new Dialog(rootView.getContext());
            dialog.getWindow();
            dialog.setCancelable(true);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_password_change);

            final Window w = dialog.getWindow();
            w.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            w.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            final TextView txtOldPassword = (TextView) w.findViewById(R.id.txtOldPassword);
            final TextView txtNewPassword = (TextView) w.findViewById(R.id.txtNewPassword);
            final TextView txtConfirmNewPassword = (TextView) w.findViewById(R.id.txtConfirmNewPassword);
            Button btnChangePasword = (Button) w.findViewById(R.id.btnChangePassword);
            btnChangePasword.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    StringBuilder sb = new StringBuilder();
                    if (TextUtils.isEmpty(txtOldPassword.getText())) {
                        sb.append("Old Password\n");
                    }
                    if (TextUtils.isEmpty(txtNewPassword.getText())) {
                        sb.append("New Password\n");
                    }
                    if (TextUtils.isEmpty(txtConfirmNewPassword.getText())) {
                        sb.append("Confirm New Password\n");
                    }

                    if (sb.toString().length() > 0) {
                        alertEmpty(sb.toString());
                        return;
                    }

                    if (!activeCustomer.getPassword().trim().equals(txtOldPassword.getText().toString().trim())) {
                        alertOldPasswordError();
                        return;
                    }
                    if (!txtNewPassword.getText().toString().trim().equals(txtConfirmNewPassword.getText().toString().trim())) {
                        alertNewPasswordMismatch();
                        return;
                    }

                    Customer cust = new Customer();
                    cust.setId(activeCustomer.getId());
                    cust.setPassword(txtNewPassword.getText().toString().trim());

                    saveNewPassword(dialog, cust);
                }
            });

            dialog.show();

        }
    };

    private void alertEmpty(String field) {
        new AlertDialog.Builder(rootView.getContext())
                .setCancelable(false)
                .setTitle("Required")
                .setMessage("Required field(s):\n\n" + field.toString())
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create()
                .show();
    }

    private void alertOldPasswordError() {
        new AlertDialog.Builder(rootView.getContext())
                .setCancelable(false)
                .setTitle("Invalid")
                .setMessage("Password not valid")
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create()
                .show();
    }

    private void alertNewPasswordMismatch() {
        new AlertDialog.Builder(rootView.getContext())
                .setCancelable(false)
                .setTitle("Password mismatch")
                .setMessage("New Password do not match.")
                .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                })
                .create()
                .show();
    }

    private void saveNewPassword(final Dialog dialog, Customer cust) {
        new ServiceUpateCustomerPassword(rootView.getContext(), new CustomerImpl.Null() {
            @Override
            public void closePassDialog() {
                activeCustomer = prefs.getCustomerInfo();
                dialog.dismiss();
            }
        }).execute(cust);
    }

    View.OnClickListener btnUpdateClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            StringBuilder builder = new StringBuilder();

            if (TextUtils.isEmpty(txtFirstName.getText())) {
                builder.append("First Name\n");
            }
            if (TextUtils.isEmpty(txtLastName.getText())) {
                builder.append("Last Name\n");
            }
            if (TextUtils.isEmpty(txtMobile.getText())) {
                builder.append("Mobile Number\n");
            }
            if (TextUtils.isEmpty(txtEmail.getText())) {
                builder.append("Email Address\n");
            }

            if (TextUtils.isEmpty(txtUsername.getText())) {
                builder.append("Username\n");
            }
            if (TextUtils.isEmpty(txtPassword.getText())) {
                builder.append("Password\n");
            }

            Emirate emirate = (Emirate) cboEmirates.getSelectedItem();
            if (emirate == null || emirate.getId() < 1) {
                builder.append("Emirate\n");
            }
            Area area = (Area) cboArea.getSelectedItem();
            if (area == null || area.getId() < 1) {
                builder.append("Area\n");
            }

            if (TextUtils.isEmpty(txtAddress.getText())) {
                builder.append("Full Address\n");
            }

            if (builder.toString().length() > 5) {
                new AlertDialog.Builder(rootView.getContext())
                        .setCancelable(false)
                        .setTitle("Required")
                        .setMessage("Required field(s):\n\n" + builder.toString())
                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create()
                        .show();
                return;
            }

            Emirate newEmirate = new Emirate();
            newEmirate.setId(emirate.getId());
            newEmirate.setCode(emirate.getCode());
            newEmirate.setName(emirate.getName());

            Area newArea = new Area();
            newArea.setId(area.getId());
            newArea.setEmiratesId(area.getEmiratesId());
            newArea.setName(area.getName());

            Customer newCustomer = new Customer();
            newCustomer.setId(activeCustomer.getId());
            newCustomer.setFirstName(txtFirstName.getText().toString());
            newCustomer.setLastName(txtLastName.getText().toString());
            newCustomer.setMobile(txtMobile.getText().toString());
            newCustomer.setEmail(txtEmail.getText().toString());
            newCustomer.setEmiratesId(((Emirate) cboEmirates.getSelectedItem()).getId());
            newCustomer.setAreaId(((Area) cboArea.getSelectedItem()).getId());
            newCustomer.setEmiratesCode(((Emirate) cboEmirates.getSelectedItem()).getCode());
            newCustomer.setEmiratesName(((Emirate) cboEmirates.getSelectedItem()).getName());
            newCustomer.setAreaName(((Area) cboArea.getSelectedItem()).getName());
            newCustomer.setAddress(txtAddress.getText().toString());
            newCustomer.setMinAmount(200.00);
            newCustomer.setType("perrier");
            newCustomer.setEmirate(newEmirate);
            newCustomer.setArea(newArea);

            new ServiceUpateCustomer(rootView.getContext()).execute(newCustomer);
        }
    };

}
