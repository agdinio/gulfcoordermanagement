package com.gulfcouae.ordermanagement;

import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.gulfcouae.ordermanagement.R;
import com.gulfcouae.ordermanagement.abstracts.BaseFragment;
import com.gulfcouae.ordermanagement.adapters.ProductAdapter;
import com.gulfcouae.ordermanagement.beans.Product;
import com.gulfcouae.ordermanagement.helpers.ThreadHandler;
import com.gulfcouae.ordermanagement.interfaces.BackHandlerInterface;
import com.gulfcouae.ordermanagement.interfaces.BaseFragmentImpl;
import com.gulfcouae.ordermanagement.interfaces.ProductsImpl;
import com.gulfcouae.ordermanagement.statics.Global;
import com.gulfcouae.ordermanagement.sync.ServiceProducts;

import java.util.List;

/**
 * Created by relly on 7/26/15.
 */
public class FragmentProductList extends ListFragment implements BackHandlerInterface {

    private View rootView;
    private LinearLayout filledContainer;
    private LinearLayout emptyContainer;
    private TextView txtSearchString;
    private Button btnContinueSearch;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        View actionBarView = actionBar.getCustomView();
        ((TextView) actionBarView.findViewById(R.id.txtActionBarTitle)).setText(getString(R.string.actionbar_title_productlist));

        rootView = inflater.inflate(R.layout.fragment_product_list, container, false);

        filledContainer = (LinearLayout) rootView.findViewById(R.id.filledContainer);
        emptyContainer = (LinearLayout) rootView.findViewById(R.id.emptyContainer);
        txtSearchString = (TextView) rootView.findViewById(R.id.txtSearchString);
        btnContinueSearch = (Button) rootView.findViewById(R.id.btnContinueSearch);
        btnContinueSearch.setOnClickListener(btnSearchClickListener);

        return rootView;
    }


    @Override
    public void onStart() {
        super.onStart();

        String searchString = null;
        Bundle args = getArguments();
        if (args != null)
            searchString = args.getString("searchstring", "");

        final String finalSearchString = searchString;
        new ServiceProducts(rootView.getContext(), getListView(), new ProductsImpl() {
            @Override
            public void showList(List<Product> products) {
                if (products != null && products.size() > 0) {
                    filledContainer.setVisibility(View.VISIBLE);
                    emptyContainer.setVisibility(View.GONE);

                    final List<Product> final_products = products;
                    new ThreadHandler(rootView.getContext()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            ProductAdapter adapter = new ProductAdapter(rootView.getContext(), final_products);
                            setListAdapter(adapter);
                        }
                    }, 500);
                } else {
                    txtSearchString.setText("\"" + finalSearchString + "\"");
                    filledContainer.setVisibility(View.GONE);
                    emptyContainer.setVisibility(View.VISIBLE);
                }

            }
        }).execute(searchString);
    }


    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {

        TextView txtProductId = (TextView) v.findViewById(R.id.txtProductId);

        Bundle args = new Bundle();
        args.putString("productId", txtProductId.getText().toString());

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction trans = fragmentManager.beginTransaction();

        Fragment frag = new FragmentProductDetails();
        frag.setArguments(args);
        trans.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left);
        trans.replace(R.id.content_frame, frag, Global.FRAG_PRODUCT_DETAILS);
        trans.addToBackStack(Global.FRAG_PRODUCT_DETAILS);
        trans.commit();

    }

    private View.OnClickListener btnSearchClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    FragmentManager fragmentManager = ((FragmentActivity) rootView.getContext()).getSupportFragmentManager();

                    FragmentTransaction trans = fragmentManager.beginTransaction();

                    Fragment frag = new FragmentSearch();
                    trans.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left);
                    trans.replace(R.id.content_frame, frag, Global.FRAG_SEARCH);
                    trans.addToBackStack(Global.FRAG_SEARCH);
                    trans.commit();

                }
            }, 500);
        }
    };


    @Override
    public void onBackPressed() {

    }
}
