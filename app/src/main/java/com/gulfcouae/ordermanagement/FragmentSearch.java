package com.gulfcouae.ordermanagement;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.gulfcouae.ordermanagement.R;
import com.gulfcouae.ordermanagement.helpers.Common;
import com.gulfcouae.ordermanagement.interfaces.BaseFragmentImpl;
import com.gulfcouae.ordermanagement.statics.Global;

/**
 * Created by relly on 7/28/15.
 */
public class FragmentSearch extends Fragment implements BaseFragmentImpl {

    private View rootView;
    private EditText txtSearch;
    private Button btnSearch;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        View actionBarView = actionBar.getCustomView();
        ((TextView) actionBarView.findViewById(R.id.txtActionBarTitle)).setText(getString(R.string.actionbar_title_search));

        rootView = inflater.inflate(R.layout.fragment_search, container, false);

        txtSearch = (EditText) rootView.findViewById(R.id.txtSearch);
        txtSearch.setOnEditorActionListener(searchListener);

        btnSearch = (Button) rootView.findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(btnSearchClickListener);

        return rootView;
    }


    private TextView.OnEditorActionListener searchListener = new TextView.OnEditorActionListener() {
        @Override
        public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                Common.hideKeyboard(rootView.getContext());

                Bundle args = new Bundle();
                args.putString("searchstring", txtSearch.getText().toString());

                FragmentManager fragmentManager = ((FragmentActivity)rootView.getContext()).getSupportFragmentManager();
                FragmentTransaction trans = fragmentManager.beginTransaction();

                Fragment frag = new FragmentProductList();
                frag.setArguments(args);
                trans.replace(R.id.content_frame, frag, Global.FRAG_PRODUCT_LIST);
                trans.commit();

                return true;
            }
            return false;
        }
    };

    private View.OnClickListener btnSearchClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Common.hideKeyboard(rootView.getContext());

            Bundle args = new Bundle();
            args.putString("searchstring", txtSearch.getText().toString());

            FragmentManager fragmentManager = ((FragmentActivity)rootView.getContext()).getSupportFragmentManager();
            FragmentTransaction trans = fragmentManager.beginTransaction();

            Fragment frag = new FragmentProductList();
            frag.setArguments(args);
            trans.replace(R.id.content_frame, frag, Global.FRAG_PRODUCT_LIST);
            trans.commit();
        }
    };

}
