package com.gulfcouae.ordermanagement;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.Paint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.gulfcouae.ordermanagement.beans.Cart;
import com.gulfcouae.ordermanagement.beans.Customer;
import com.gulfcouae.ordermanagement.beans.Product;
import com.gulfcouae.ordermanagement.beans.ProductUOM;
import com.gulfcouae.ordermanagement.helpers.Common;
import com.gulfcouae.ordermanagement.helpers.Currency;
import com.gulfcouae.ordermanagement.helpers.ThreadHandler;
import com.gulfcouae.ordermanagement.interfaces.BackHandlerInterface;
import com.gulfcouae.ordermanagement.interfaces.BaseFragmentImpl;
import com.gulfcouae.ordermanagement.interfaces.ProductDetailsImpl;
import com.gulfcouae.ordermanagement.persistents.CartDataSource;
import com.gulfcouae.ordermanagement.persistents.SharedPreferences;
import com.gulfcouae.ordermanagement.statics.Global;
import com.gulfcouae.ordermanagement.sync.DownloadImage;
import com.gulfcouae.ordermanagement.utils.FontUtils;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by relly on 7/31/15.
 */
public class FragmentProductDetails extends Fragment implements BackHandlerInterface {

    private View actionBarView;
    private View rootView;
    private TextView txtProductId;
    private TextView txtName;
    private TextView txtDescription;
    private TextView txtPrice;
    //private TextView txtAvailability;
    private ImageView imgProductDetail;
    private EditText txtQuantity;
    private Button btnAddToCart;
    private Button btnGoToItems;
    private CartDataSource dao;
    private Spinner cboQuantity;
    private Spinner cboUOM;
    private TextView txtRegularPrice;

    private Product productDetail;
    private ProductUOM selProm;
    private LinearLayout rootContainer;
    private TableRow rowPromo;
    private TableRow rowPromoDesc;
    private TextView txtPromoDesc;
    private TableRow rowDiscount;
    private TextView txtDiscount;

    //private double totalPrice;
    private List<ProductUOM> final_product_uom_list;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        actionBarView = actionBar.getCustomView();
        ((TextView) actionBarView.findViewById(R.id.txtActionBarTitle)).setText(getString(R.string.actionbar_title_productdetails));

        rootView = inflater.inflate(R.layout.fragment_product_details, container, false);

        rootContainer = (LinearLayout) rootView.findViewById(R.id.rootContainer);

        txtProductId = (TextView) rootView.findViewById(R.id.txtProductId);
        txtName = (TextView) rootView.findViewById(R.id.txtName);
        txtDescription = (TextView) rootView.findViewById(R.id.txtDescription);
        txtPrice = (TextView) rootView.findViewById(R.id.txtPrice);
        imgProductDetail = (ImageView) rootView.findViewById(R.id.imgProductDetail);
        //txtAvailability = (TextView) rootView.findViewById(R.id.txtAvailability);
        txtQuantity = (EditText) rootView.findViewById(R.id.txtQuantity);
        cboQuantity = (Spinner) rootView.findViewById(R.id.cboQuantity);
        cboUOM = (Spinner) rootView.findViewById(R.id.cboUOM);
        txtRegularPrice = (TextView) rootView.findViewById(R.id.txtRegularPrice);
        rowPromo = (TableRow) rootView.findViewById(R.id.rowPromo);
        rowPromoDesc = (TableRow) rootView.findViewById(R.id.rowPromoDesc);
        txtPromoDesc = (TextView) rootView.findViewById(R.id.txtPromoDesc);
        rowDiscount = (TableRow) rootView.findViewById(R.id.rowDiscount);
        txtDiscount = (TextView) rootView.findViewById(R.id.txtDiscount);

        btnAddToCart = (Button) rootView.findViewById(R.id.btnAddToCart);
        btnAddToCart.setOnClickListener(btnAddToCartClickListener);
        btnGoToItems = (Button) rootView.findViewById(R.id.btnGoToItems);
        btnGoToItems.setOnClickListener(btnGoToItemsClickListener);

        loadQuantities();

        Bundle args = getArguments();
        String productId = args.getString("productId");

        new ProductTask(rootView.getContext(), new ProductDetailsImpl() {
            @Override
            public void getProductFromWS(Product product) {
                if (product != null) {
                    final_product_uom_list = product.getProductUOMList();

                    productDetail = product;
                    txtProductId.setText(String.valueOf(product.getId()));
                    txtName.setText(product.getName());
                    txtDescription.setText(product.getDescription());

                    rootContainer.setVisibility(View.VISIBLE);
                    txtQuantity.setEnabled(true);
                    btnAddToCart.setEnabled(true);
                    cboQuantity.setEnabled(true);
                    cboQuantity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            computeTotal(final_product_uom_list, selProm);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });


                    if (final_product_uom_list != null) {
                        ArrayAdapter<ProductUOM> adapter = new ArrayAdapter(rootView.getContext(),
                                R.layout.base_dropdown,
                                final_product_uom_list);

                        cboUOM.setAdapter(adapter);
                        cboUOM.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                selProm = (ProductUOM) parent.getItemAtPosition(position);
                                computeTotal(final_product_uom_list, selProm);
                            }
                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {
                                
                            }
                        });
                    }

                    new DownloadImage(imgProductDetail, rootView.getContext()).execute(product.getImage());
                } else {
                    txtQuantity.setEnabled(false);
                    cboQuantity.setEnabled(false);
                    btnAddToCart.setEnabled(false);
                }
            }
        }).execute(Integer.parseInt(productId));


        return rootView;
    }

    private void computeTotal(List<ProductUOM> list, ProductUOM selected) {
        if (selected != null) {
            for (ProductUOM prom : list) {
                if (prom.getUomId() == selected.getUomId()) {
                    double regPrice = 0;
                    double promPrice = 0;
                    double discount = 0;
                    if (prom.getPromoPrice() > 0) {
                        // promo
                        rowPromo.setVisibility(View.VISIBLE);
                        rowPromoDesc.setVisibility(View.VISIBLE);
                        rowDiscount.setVisibility(View.VISIBLE);

                        regPrice = prom.getRegularPrice() * Integer.parseInt(cboQuantity.getSelectedItem().toString());
                        promPrice = prom.getPromoPrice() * Integer.parseInt(cboQuantity.getSelectedItem().toString());
                        discount = (1 - (promPrice / regPrice));

                        txtRegularPrice.setPaintFlags(txtRegularPrice.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
                        txtRegularPrice.setText(Currency.format(regPrice));

                        txtPrice.setText(Currency.format(promPrice));
                        txtPromoDesc.setText(prom.getPromoDetails());
                        txtDiscount.setText(Currency.format(regPrice - promPrice) + " (" + Currency.formatPercent(discount) + ")");
                    } else {
                        // regular
                        rowPromo.setVisibility(View.GONE);
                        rowPromoDesc.setVisibility(View.GONE);
                        rowDiscount.setVisibility(View.GONE);
                        regPrice = prom.getRegularPrice() * Integer.parseInt(cboQuantity.getSelectedItem().toString());
                        txtRegularPrice.setPaintFlags(txtRegularPrice.getPaintFlags() & (~ Paint.STRIKE_THRU_TEXT_FLAG));
                        txtRegularPrice.setText(Currency.format(regPrice));
                    }

//                    if (prom.getQuantity() > 0) {
//                        txtAvailability.setTextColor(getResources().getColor(R.color.primary));
//                        txtAvailability.setText("In stock");
//                        txtPrice.setText(Currency.format(totalPrice));
//                    } else {
//                        txtAvailability.setTextColor(Color.parseColor("#FF0000"));
//                        txtAvailability.setText("Out of stock");
//                        txtPrice.setText(Currency.format(0.00));
//                    }
                    break;
                }
            }

        }

    }

    @Override
    public void onStart() {
        super.onStart();
        dao = new CartDataSource(rootView.getContext());
    }

    View.OnClickListener btnAddToCartClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            hideKeyboard();

            if (productDetail != null) {
//                if (productDetail.getQuantity() < Double.parseDouble(txtQuantity.getText().toString())) {
//                    new AlertDialog.Builder(rootView.getContext())
//                    .setCancelable(false)
//                    .setTitle("Insufficient")
//                    .setMessage("The quantity you have entered is greater than the quantity available in stock. " +
//                                "Please enter new quantity.")
//                    .setNegativeButton("OK", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialog, int which) {
//                            dialog.dismiss();
//                        }
//                    })
//                    .create()
//                    .show();
//
//                    return;
//                }
                if (TextUtils.isEmpty(txtQuantity.getText().toString())) {
                    Toast.makeText(rootView.getContext(), "Please specify quantity.", Toast.LENGTH_LONG).show();
                    return;
                }
            }

            addToCart();
        }
    };

    View.OnClickListener btnGoToItemsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            FragmentManager fragmentManager = ((FragmentActivity)rootView.getContext()).getSupportFragmentManager();
            FragmentTransaction trans = fragmentManager.beginTransaction();

            Fragment frag = new FragmentProductList();
            trans.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_right);
            trans.replace(R.id.content_frame, frag, Global.FRAG_PRODUCT_LIST);
            trans.commit();
        }
    };


    private void loadQuantities() {
        cboQuantity.setAdapter(Common.quantityArrayAdapter(rootView.getContext()));
        cboQuantity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }
            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
    }

    private void hideKeyboard() {
        View softview = getActivity().getCurrentFocus();
        if (softview != null) {
            InputMethodManager im = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(softview.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }


    private void addToCart() {

        try {
            SharedPreferences prefs = new SharedPreferences(rootView.getContext());
            String jsonString = prefs.getString("customerinfo", "");
            Customer cust = prefs.fromJson(jsonString, Customer.class);
            Cart cart = null;

            if (cust != null) {

                if (selProm == null) {
                    new AlertDialog.Builder(rootView.getContext())
                            .setCancelable(false)
                            .setTitle("Warning")
                            .setMessage("Product price is not yet implemented.")
                            .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .create()
                            .show();
                    return;
                }
                if (selProm != null && selProm.getRegularPrice() < 1 && selProm.getPromoPrice() < 1) {
                    new AlertDialog.Builder(rootView.getContext())
                            .setCancelable(false)
                            .setTitle("Warning")
                            .setMessage("Product price is not yet implemented.")
                            .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.dismiss();
                                }
                            })
                            .create()
                            .show();
                    return;
                }


                Cart existingCart = getCartByProductId(cust.getId(), productDetail.getId(), selProm.getUomId());

                dao.open(true);
                if (existingCart != null) {
                    int qty = (existingCart.getQuantity() + Integer.parseInt(cboQuantity.getSelectedItem().toString()));

                    cart = new Cart();
                    cart.setCustomerId(cust.getId());
                    cart.setProductId(productDetail.getId());
                    cart.setProductName(existingCart.getProductName());
                    cart.setImage(existingCart.getImage());
                    cart.setId(existingCart.getId());
                    cart.setUomId(selProm.getUomId());
                    cart.setQuantity(qty);
                    int result = dao.updateCartQuantity(cart);
                    if (result < 1) {
                        new ThreadHandler(rootView.getContext()).sendMessage(Message.obtain(null, ThreadHandler.CART_UPDATE_FAILED, cart));
                        dao.close();
                        return;
                    }
                } else {
                    cart = new Cart();
                    cart.setCustomerId(cust.getId());
                    cart.setProductId(productDetail.getId());
                    cart.setProductName(productDetail.getName());
                    cart.setPrice(selProm.getPromoPrice() > 0 ? selProm.getPromoPrice() : selProm.getRegularPrice());
                    cart.setQuantity(Integer.parseInt(cboQuantity.getSelectedItem().toString()));
                    cart.setImage(productDetail.getImage());
                    cart.setUomId(selProm.getUomId());
                    cart.setProductUOMList(final_product_uom_list);
                    long insertId = dao.addToCart(cart);
                    if (insertId < 1) {
                        new ThreadHandler(rootView.getContext()).sendMessage(Message.obtain(null, ThreadHandler.CART_ADD_FAILED, cart));
                        dao.close();
                        return;
                    }
                }

                new ThreadHandler(rootView.getContext()).sendMessage(Message.obtain(null, ThreadHandler.CART_ADD_SUCCESS, cart));
                Common.updateBadge(rootView.getContext());
            }

        } catch (Exception e) {
            Toast.makeText(rootView.getContext(), "Add to cart failed. Please try again.", Toast.LENGTH_LONG).show();
        } finally {
            dao.close();
        }

    }

    private Cart getCartByProductId(int custId, int prodId, int uomId) {
        Cart cart = null;
        dao.open(false);
        cart = dao.getCartByProductId(custId, prodId, uomId);
        dao.close();
        return cart;
    }

    @Override
    public void onBackPressed() {

    }

    private class ProductTask extends AsyncTask<Integer, Void, Product> {

        private Integer productId;
        private ProductDetailsImpl impl;
        private Context context;

        public ProductTask(Context context, ProductDetailsImpl impl) {

            this.impl = impl;
            this.context = context;

            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_SHOW);
        }

        @Override
        protected Product doInBackground(Integer... params) {
            productId = params[0];

            Product product = null;

//            SoapObject request = new SoapObject(Global.NAMESPACE, com.gulfcouae.ordermanagement.statics.Product.METHOD_NAME_GETPRODUCTBYID);
//            request.addProperty("productId", productId);
//
//            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//            envelope.dotNet = true;
//            envelope.setOutputSoapObject(request);
//
//            HttpTransportSE androidHttpTransport = new HttpTransportSE(Global.SERVICE_URL);
//            try {
//                androidHttpTransport.call(String.format("%s%s", Global.SOAP_ACTION, com.gulfcouae.ordermanagement.statics.Product.SOAP_ACTION_GETPRODUCTBYID), envelope);
//
//                SoapObject item = (SoapObject) envelope.getResponse();
//                if (item != null) {
//                    product = new Product();
//                    product.setId(Integer.parseInt(item.getProperty("Id").toString()));
//                    product.setName(item.getProperty("Name") == null ? "" : item.getProperty("Name").toString());
//                    product.setDescription(item.getProperty("Description") == null ? "" : item.getProperty("Description").toString());
//                    product.setCategoryId(item.getProperty("CategoryId") == null ? 0 : Integer.parseInt(item.getProperty("CategoryId").toString()));
//                    product.setCategoryName(item.getProperty("CategoryName") == null ? "" : item.getProperty("CategoryName").toString());
//                    product.setBrandId(item.getProperty("BrandId") == null ? 0 : Integer.parseInt(item.getProperty("BrandId").toString()));
//                    product.setBrandName(item.getProperty("BrandName") == null ? "" : item.getProperty("BrandName").toString());
//                    product.setImage(item.getProperty("Image") == null ? "" : item.getProperty("Image").toString());
//                    product.setCode(item.getProperty("Code") == null ? "" : item.getProperty("Code").toString());
//                    product.setIsPromo(item.getProperty("IsPromo") == null ? false : Boolean.parseBoolean(item.getProperty("IsPromo").toString()));
//
//                    SoapObject soapDetails = (SoapObject) item.getProperty("ProductUOMList");
//                    if (soapDetails != null && soapDetails.getPropertyCount() > 0) {
//                        List<ProductUOM> prod_uom_list = new ArrayList<>();
//                        for (int i = 0; i<soapDetails.getPropertyCount(); i++) {
//                            SoapObject soapdetail = (SoapObject) soapDetails.getProperty(i);
//                            ProductUOM detail = new ProductUOM();
//                            detail.setProductId(soapdetail.getProperty("ProductId") == null ? 0 : Integer.parseInt(soapdetail.getProperty("ProductId").toString()));
//                            detail.setUomId(soapdetail.getProperty("UOMId") == null ? 0 : Integer.parseInt(soapdetail.getProperty("UOMId").toString()));
//                            detail.setUomCode(soapdetail.getProperty("UOMCode") == null ? "" : soapdetail.getProperty("UOMCode").toString());
//                            detail.setRegularPrice(soapdetail.getProperty("RegularPrice") == null ? 0 : Double.parseDouble(soapdetail.getProperty("RegularPrice").toString()));
//                            detail.setPromoPrice(soapdetail.getProperty("PromoPrice") == null ? 0 : Double.parseDouble(soapdetail.getProperty("PromoPrice").toString()));
//                            detail.setPromoName(soapdetail.getProperty("PromoName") == null ? "" : soapdetail.getProperty("PromoName").toString());
//                            detail.setPromoDetails(soapdetail.getProperty("PromoDetails") == null ? "" : soapdetail.getProperty("PromoDetails").toString());
//                            detail.setQuantity(soapdetail.getProperty("Quantity") == null ? 0 : Integer.parseInt(soapdetail.getProperty("Quantity").toString()));
//                            prod_uom_list.add(detail);
//                        }
//                        product.setProductUOMList(prod_uom_list);
//                    }
//                }
//
//            } catch (Exception e) {
//                new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_HIDE);
//                new ThreadHandler(context).sendEmptyMessage(ThreadHandler.SERVER_FAILED);
//                return null;
//            } finally {
//                new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_HIDE);
//            }
//----------------------------------------------------------------------------
            //productdetails
            int id[] = {9,8,2,1,11,10,6,5};
            String name[] = {"CATERIPILLAR ISA 8173","CATERPILLAR 3HD 2117","Falcon Lighter","Falcon-Nemesis","Fossil Mens' Blue Leather","Fossil Men's Brown Leather","Perrier 3000","Purie"};
            String desc[] = {"Cat Watches Powertech Reserve / Alarm","Cat Watches Texas","Falcon Lighters","Falcon Spring Drinking water.","Fossil Grant for Men Chronograph","Fossil Men's White Dial Leather Brand","Perrier 3000","Pure Water Technology "};
            int catId[] = {0,0,0,0,0,0,0,0,0,0};
            String catName[] = {"","","","","","","",""};
            int brandId[] = {0,0,1,1,0,0,1,1};
            String brandName[] = {"","","Perrier","Perrier","","","Perrier","Perrier"};
            String img[] = {"SetWidth290-SB.145.11.127.jpg","SetWidth290-SD.161.34.132.jpg","","Falcon.png","FS4835.jpg","FS4735.jpg","","purie.jpg"};
            String code[] = {"CAT","CAT","P0002","P0001","FOSSIL","FOSSIL","P0003","P0004"};
            boolean ispromo[] = {false,false,true,true,false,true,false,false};

            for (int j=0; j<id.length; j++) {
                if (id[j] == productId) {
                    product = new Product();
                    product.setId(id[j]);
                    product.setName(name[j]);
                    product.setDescription(desc[j]);
                    product.setCategoryId(catId[j]);
                    product.setCategoryName(catName[j]);
                    product.setBrandId(brandId[j]);
                    product.setBrandName(brandName[j]);
                    product.setImage(img[j]);
                    product.setCode(code[j]);
                    product.setIsPromo(ispromo[j]);


                    //uom
                    int prodid[] = {1,1,2,1,5,3,7,7,8,9,10,11};
                    int uomid[] = {2,1,1,3,2,1,1,2,1,1,1,1};
                    double regprice[] = {222.00,22.00,1000.00,333.00,120.00,5.95,10.0,23.00,870.00,940.00,490.00,530.00};
                    double promprice[] = {190.00,20.00,850.00,0,0,0,9.00,41.00,0,0,400.00,0};
                    String uomcode[] = {"BOX","EAC","EAC","CRT","BOX","EAC","EACH","BOX","EAC","EAC","EAC","EAC"};
                    int qty[] = {1000,100,23,10000,560,23,23,23,5,5,5,5};
                    String promname[] = {"Promotion2 August 2014","Promotion1 August 2014","Promotion3 August 2014","PROMO 003","","","Promotion3 August 2014","Promotion5 August 2014","","","",""};
                    String promdetails[] = {"Buy any 3 eac from this group get 1 eac foc","Buy any 5 eac from this group get 1 eac foc","Buy any 4 eac from this group get 1 ","PROMO DETAILS 003","","","Buy any 4 eac from this group get 1 ","Promotion5 August 2014","","","Sale up to 50% on selected item",""};
                    List<ProductUOM> prod_uom_list = new ArrayList<>();

                    for (int i=0; i<uomid.length; i++) {
                        if (productId == prodid[i]) {
                            ProductUOM detail = new ProductUOM();
                            detail.setProductId(productId);
                            detail.setUomId(uomid[i]);
                            detail.setUomCode(uomcode[i]);
                            detail.setRegularPrice(regprice[i]);
                            detail.setPromoPrice(promprice[i]);
                            detail.setPromoName(promname[i]);
                            detail.setPromoDetails(promdetails[i]);
                            detail.setQuantity(qty[i]);
                            prod_uom_list.add(detail);
                        }
                    }
                    product.setProductUOMList(prod_uom_list);
                    break;
                }
            }



            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_HIDE);

            return product;
        }

        @Override
        protected void onPostExecute(Product product) {
            final Product final_product = product;
            new ThreadHandler(context).postDelayed(new Runnable() {
                @Override
                public void run() {
                    new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_HIDE);
                    impl.getProductFromWS(final_product);
                }
            }, 500);
        }
    }

}
