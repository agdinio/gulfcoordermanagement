package com.gulfcouae.ordermanagement;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.gulfcouae.ordermanagement.R;
import com.gulfcouae.ordermanagement.adapters.AreaAdapter;
import com.gulfcouae.ordermanagement.beans.Area;
import com.gulfcouae.ordermanagement.beans.Customer;
import com.gulfcouae.ordermanagement.beans.Emirate;
import com.gulfcouae.ordermanagement.interfaces.AreaImpl;
import com.gulfcouae.ordermanagement.interfaces.BaseFragmentImpl;
import com.gulfcouae.ordermanagement.interfaces.EmirateImpl;
import com.gulfcouae.ordermanagement.persistents.SharedPreferences;
import com.gulfcouae.ordermanagement.sync.ServiceArea;
import com.gulfcouae.ordermanagement.sync.ServiceEmirates;
import com.gulfcouae.ordermanagement.sync.ShippingInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by relly on 8/4/15.
 */
public class FragmentShippingInfo extends Fragment implements
                                        BaseFragmentImpl,
                                        View.OnClickListener{

    private ScrollView rootContainer;
    private View rootView;
    private EditText txtMobile;
    private EditText txtEmail;
    private Spinner cboEmirates;
    private Spinner cboArea;
    private EditText txtAddress;
    private Button btnSave;
    private ProgressBar pbArea;
    private SharedPreferences prefs;
    private Customer activeCustomer;
    private int epos;
    private int apos;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        View actionBarView = actionBar.getCustomView();
        ((TextView) actionBarView.findViewById(R.id.txtActionBarTitle)).setText(getString(R.string.actionbar_title_shippinginfo));

        rootView = inflater.inflate(R.layout.fragment_shipping_info, container, false);

        rootContainer = (ScrollView) rootView.findViewById(R.id.rootContainer);
        txtMobile = (EditText) rootView.findViewById(R.id.txtMobile);
        txtEmail = (EditText) rootView.findViewById(R.id.txtEmail);
        txtAddress = (EditText) rootView.findViewById(R.id.txtAddress);
        cboEmirates = (Spinner) rootView.findViewById(R.id.cboEmirate);
        cboArea = (Spinner) rootView.findViewById(R.id.cboArea);
        pbArea = (ProgressBar) rootView.findViewById(R.id.pbArea);

        btnSave = (Button) rootView.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(FragmentShippingInfo.this);

        prefs = new SharedPreferences(rootView.getContext());
        activeCustomer = prefs.getCustomerInfo();
        if (activeCustomer != null) {
            txtMobile.setText(activeCustomer.getMobile());
            txtEmail.setText(activeCustomer.getEmail());
            txtAddress.setText(activeCustomer.getAddress());
        }

        initEmirate();

        return rootView;
    }

    private void initEmirate() {
        new ServiceEmirates(rootView.getContext(), new EmirateImpl.Null() {
            @Override
            public void refreshList(List<Emirate> emirates) {

                if (emirates != null && emirates.size() > 0) {

                    rootContainer.setVisibility(View.VISIBLE);

                    Emirate blank = new Emirate();
                    blank.setId(0);
                    blank.setCode("");
                    blank.setName("None");
                    emirates.add(0, blank);

                    Emirate currEmirate = null;
                    if (activeCustomer.getEmirate() != null) {
                        for (Emirate e : emirates) {
                            if (e.getId() == activeCustomer.getEmirate().getId()) {
                                epos = emirates.indexOf(e);
                                currEmirate = e;
                                break;
                            }
                        }
                    }

                    ArrayAdapter<Emirate> adapter = new ArrayAdapter(rootView.getContext(),
                            R.layout.dropdown_emirate,
                            emirates);
                    cboEmirates.setAdapter(adapter);
                    initDefaultArea(currEmirate);

                    cboEmirates.setSelection(epos);
                } else {
                    rootContainer.setVisibility(View.INVISIBLE);
                }


                initEmptyArea();

            }
        }).execute();
    }

    private void initEmptyArea() {
        Area blank = new Area();
        blank.setId(0);
        blank.setName("None");
        List<Area> blankareas = new ArrayList();
        blankareas.add(blank);

        ArrayAdapter<Area> adapter = new ArrayAdapter(rootView.getContext(),
                R.layout.dropdown_area,
                blankareas);

        cboArea.setAdapter(adapter);

    }

    private void initDefaultArea(Emirate emirate) {
        if (emirate != null && emirate.getId() > 0) {

            new ServiceArea(rootView.getContext(), pbArea, new AreaImpl() {
                @Override
                public void refreshList(List<Area> areas) {
                    if (areas != null) {
                        for (Area a : areas) {
                            if (a.getId() == activeCustomer.getArea().getId()) {
                                apos = areas.indexOf(a);
                                break;
                            }
                        }

                        AreaAdapter adapter = new AreaAdapter(rootView.getContext(), areas);
                        cboArea.setAdapter(adapter);
                        cboArea.setSelection(apos);
                    } else {
                        initEmptyArea();
                    }

                    cboEmirates.setOnItemSelectedListener(cboEmirateSelectedListener);
                }
            }).execute(emirate.getId());

        } else {
            initEmptyArea();
        }

    }

    private void loadAreas(Emirate emirate) {
        if (emirate != null && emirate.getId() > 0) {

            new ServiceArea(rootView.getContext(), pbArea, new AreaImpl() {
                @Override
                public void refreshList(List<Area> areas) {
                    if (areas != null) {
                        ArrayAdapter<Area> adapter = new ArrayAdapter(rootView.getContext(),
                                R.layout.dropdown_area,
                                areas);

                        cboArea.setAdapter(adapter);
                    } else {
                        initEmptyArea();
                    }

                }
            }).execute(emirate.getId());

        } else {
            initEmptyArea();
        }

    }

    AdapterView.OnItemSelectedListener cboEmirateSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Emirate emr = (Emirate) parent.getItemAtPosition(position);
            loadAreas(emr);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };


    @Override
    public void onClick(View v) {
        StringBuilder builder = new StringBuilder();

        if (TextUtils.isEmpty(txtMobile.getText())) {
            builder.append("Mobile Number\n");
        }
        if (TextUtils.isEmpty(txtEmail.getText())) {
            builder.append("Email Address\n");
        }

        Emirate emirate = (Emirate) cboEmirates.getSelectedItem();
        if (emirate == null || emirate.getId() < 1) {
            builder.append("Emirate\n");
        }
        Area area = (Area) cboArea.getSelectedItem();
        if (area == null || area.getId() < 1) {
            builder.append("Area\n");
        }

        if (TextUtils.isEmpty(txtAddress.getText())) {
            builder.append("Full Address\n");
        }

        if (builder.toString().length() > 5) {
            new AlertDialog.Builder(rootView.getContext())
                    .setCancelable(false)
                    .setTitle("Required")
                    .setMessage("Required field(s):\n\n" + builder.toString())
                    .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    })
                    .create()
                    .show();
            return;
        }

        Customer cust = new Customer();
        cust.setId(activeCustomer.getId());
        cust.setMobile(txtMobile.getText().toString());
        cust.setEmail(txtEmail.getText().toString());
        cust.setEmiratesId(((Emirate) cboEmirates.getSelectedItem()).getId());
        cust.setEmiratesCode(((Emirate) cboEmirates.getSelectedItem()).getCode());
        cust.setEmiratesName(((Emirate) cboEmirates.getSelectedItem()).getName());
        cust.setAreaId(((Area) cboArea.getSelectedItem()).getId());
        cust.setAreaName(((Area) cboArea.getSelectedItem()).getName());
        cust.setAddress(txtAddress.getText().toString());

        new ShippingInfo(rootView.getContext()).execute(cust);

    }
}
