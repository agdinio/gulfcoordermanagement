package com.gulfcouae.ordermanagement;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.gulfcouae.ordermanagement.R;
import com.gulfcouae.ordermanagement.adapters.OrderHistoryAdapter;
import com.gulfcouae.ordermanagement.beans.Customer;
import com.gulfcouae.ordermanagement.beans.Order;
import com.gulfcouae.ordermanagement.helpers.ThreadHandler;
import com.gulfcouae.ordermanagement.interfaces.BaseFragmentImpl;
import com.gulfcouae.ordermanagement.interfaces.OrderHistoryImpl;
import com.gulfcouae.ordermanagement.persistents.SharedPreferences;
import com.gulfcouae.ordermanagement.statics.Global;
import com.gulfcouae.ordermanagement.sync.ServiceOrderHistory;

import java.util.List;

/**
 * Created by relly on 7/27/15.
 */
public class FragmentMyOrderHistory extends ListFragment implements BaseFragmentImpl {

    private View rootView;
    private Customer activeCustomer;
    private SharedPreferences prefs;
    private List<Order> finalOrders;
    private Button btnContinueShopping;
    private LinearLayout filledContainer;
    private LinearLayout emptyContainer;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        View actionBarView = actionBar.getCustomView();
        ((TextView) actionBarView.findViewById(R.id.txtActionBarTitle)).setText(getString(R.string.actionbar_title_myorders));

        rootView = inflater.inflate(R.layout.fragment_myorderhistory, container, false);
        filledContainer = (LinearLayout) rootView.findViewById(R.id.filledContainer);
        emptyContainer = (LinearLayout) rootView.findViewById(R.id.emptyContainer);
        btnContinueShopping = (Button) rootView.findViewById(R.id.btnContinueShopping);
        btnContinueShopping.setOnClickListener(gotoItemsClickListener);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
        new ThreadHandler(rootView.getContext()).postDelayed(new Runnable() {
            @Override
            public void run() {
                loadOrderHistory();
            }
        }, 500);
    }

    private void loadOrderHistory() {
        prefs = new SharedPreferences(rootView.getContext());

        activeCustomer = prefs.getCustomerInfo();
        if (activeCustomer != null) {
            new ServiceOrderHistory(rootView.getContext(), new OrderHistoryImpl.Null() {
                @Override
                public void showList(List<Order> orders) {
                    finalOrders = orders;
                    if (finalOrders != null && finalOrders.size() > 0) {
                        filledContainer.setVisibility(View.VISIBLE);
                        emptyContainer.setVisibility(View.GONE);
                        OrderHistoryAdapter adapter = new OrderHistoryAdapter(rootView.getContext(), finalOrders);
                        setListAdapter(adapter);
                    } else {
                        filledContainer.setVisibility(View.GONE);
                        emptyContainer.setVisibility(View.VISIBLE);
                    }
                }
            }).execute(activeCustomer.getId());
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Order order = finalOrders.get(position);

        if (order != null) {
            Bundle args = new Bundle();
            args.putSerializable("order", order);

            FragmentManager fragmentManager = getFragmentManager();
            FragmentTransaction trans = fragmentManager.beginTransaction();

            Fragment frag = fragmentManager.findFragmentByTag(Global.FRAG_ORDERDETAILSHISTORY);
            if (frag == null) {
                frag = new FragmentMyOrderDetailsHistory();
            } else {
                fragmentManager.beginTransaction().remove(frag).commit();
                frag = new FragmentMyOrderDetailsHistory();
            }
            frag.setArguments(args);
            trans.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left);
            trans.replace(R.id.content_frame, frag, Global.FRAG_ORDERDETAILSHISTORY);
            trans.addToBackStack(Global.FRAG_ORDERDETAILSHISTORY);
            trans.commit();
        } else {
            Toast.makeText(rootView.getContext(), "Id is not a valid number format", Toast.LENGTH_LONG).show();
        }

    }

    private View.OnClickListener gotoItemsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            FragmentManager fragmentManager = ((FragmentActivity)rootView.getContext()).getSupportFragmentManager();
            FragmentTransaction trans = fragmentManager.beginTransaction();

            Fragment frag = new FragmentProductList();
            trans.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            trans.replace(R.id.content_frame, frag);
            trans.commit();
        }
    };

}
