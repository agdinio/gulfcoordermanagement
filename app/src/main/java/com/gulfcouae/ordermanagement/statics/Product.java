package com.gulfcouae.ordermanagement.statics;

/**
 * Created by relly on 7/29/15.
 */
public class Product {

    public static final String PREFS_NAME = "GULFCO_PRODUCT";

    public static final String URL = "SvcCustomer.svc";
    public static final String SOAP_ACTION_GETIMAGE = "IOrderManagement/GetImage";
    public static final String METHOD_NAME_GETIMAGE = "GetImage";

    public static final String SOAP_ACTION_GETPRODUCTS = "IOrderManagement/GetProducts";
    public static final String METHOD_NAME_GETPRODUCTS = "GetProducts";

    public static final String SOAP_ACTION_GETPRODUCTBYID = "IOrderManagement/GetProductById";
    public static final String METHOD_NAME_GETPRODUCTBYID = "GetProductById";

    public static final String SOAP_ACTION_GETPRODUCTSBYKEYWORD = "IOrderManagement/GetProductsByKeyword";
    public static final String METHOD_NAME_GETPRODUCTSBYKEYWORD = "GetProductsByKeyword";

}
