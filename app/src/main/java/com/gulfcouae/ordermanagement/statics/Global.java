package com.gulfcouae.ordermanagement.statics;

import android.widget.ProgressBar;

/**
 * Created by relly on 7/26/15.
 */
public class Global {

    // COMPANY

    public static String NAMESPACE = "http://tempuri.org/";
    public static String SERVICE_URL = "http://192.168.56.195/CNWebService/GULFCO/OrderManagement.svc";
    //public static String SERVICE_URL = "http://192.168.56.245/hermes_net_v5/CNWebService/GULFCO/OrderManagement.svc";
    //public static String SERVICE_URL = "http://213.42.83.172/hermes_net_v5/CNWebService/GULFCO/OrderManagement.svc";
    public static String SOAP_ACTION = "http://tempuri.org/";


    // LIVE FREEE HOSTING
//    public static String NAMESPACE = "http://tempuri.org/";
//    public static String SERVICE_URL = "http://jam.somee.com/GULFCO/Services/OrderManagement.svc";
//    public static String SOAP_ACTION = "http://tempuri.org/";

    public static ProgressBar Bar;

    public static String FRAG_PRODUCT_LIST = "FRAG_PRODUCT_LIST";
    public static String FRAG_PRODUCT_DETAILS = "FRAG_PRODUCT_DETAILS";
    public static String FRAG_ABOUT = "FRAG_ABOUT";
    public static String FRAG_MYPROFILE = "FRAG_MYPROFILE";
    public static String FRAG_MYORDERS = "FRAG_MYORDERS";
    public static String FRAG_MYCART = "FRAG_MYCART";
    public static String FRAG_SEARCH = "FRAG_SEARCH";
    public static String FRAG_ORDERDETAILSHISTORY = "FRAG_ORDERDETAILSHISTORY";
    public static String FRAG_CHECKOUT = "FRAG_CHECKOUT";
    public static String FRAG_PLACEORDER = "FRAG_PLACEORDER";
    public static String FRAG_EDITSHIPPINGADDRESS = "FRAG_EDITSHIPPINGADDRESS";

    public static final String SOAP_ACTION_GETORDERSTATUS = "IOrderManagement/GetOrderStatus";
    public static final String METHOD_NAME_GETORDERSTATUS = "GetOrderStatus";

}
