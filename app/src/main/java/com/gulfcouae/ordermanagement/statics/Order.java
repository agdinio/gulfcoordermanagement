package com.gulfcouae.ordermanagement.statics;

/**
 * Created by relly on 8/9/15.
 */
public class Order {

    public static final String SOAP_ACTION_PLACEORDER = "IOrderManagement/PlaceOrder";
    public static final String METHOD_NAME_PLACEORDER = "PlaceOrder";

    public static final String SOAP_ACTION_GETORDERHISTORY = "IOrderManagement/GetOrderHistoryByCustomer";
    public static final String METHOD_NAME_GETORDERHISTORY = "GetOrderHistoryByCustomer";

    public static final String SOAP_ACTION_GETORDERDETAILSHISTORY = "IOrderManagement/GetOrderDetailsHistoryByOrderId";
    public static final String METHOD_NAME_GETORDERDETAILSHISTORY = "GetOrderDetailsHistoryByOrderId";
}
