package com.gulfcouae.ordermanagement.statics;

/**
 * Created by relly on 7/26/15.
 */
public class Customer {

    public static final String PREFS_NAME = "GULFCO_CUSTOMER";
    public static final String CUSTOMER_INFO = "customerinfo";
    public static final String REMEMBER_ME = "rememberme";

    public static final String SOAP_ACTION_REGISTER = "IOrderManagement/Register";
    public static final String METHOD_NAME_REGISTER = "Register";

    public static final String SOAP_ACTION_AUTHENTICATE = "IOrderManagement/Authenticate";
    public static final String METHOD_NAME_AUTHENTICATE = "Authenticate";

    public static final String SOAP_ACTION_UPDATE_SHIPPING_INFO = "IOrderManagement/UpdateShippingInfo";
    public static final String METHOD_NAME_UPDATE_SHIPPING_INFO = "UpdateShippingInfo";

    public static final String SOAP_ACTION_UPDATE_CUSTOMER = "IOrderManagement/UpdateCustomer";
    public static final String METHOD_NAME_UPDATE_CUSTOMER = "UpdateCustomer";

    public static final String SOAP_ACTION_UPDATE_PASSWORD = "IOrderManagement/UpdateCustomerPassword";
    public static final String METHOD_NAME_UPDATE_PASSWORD = "UpdateCustomerPassword";

    public static final String SOAP_ACTION_GET_USERNAME = "IOrderManagement/GetUsername";
    public static final String METHOD_NAME_GET_USERNAME = "GetUsername";

    public static final String SOAP_ACTION_EMAIL_PASSWORD = "IOrderManagement/EmailPassword";
    public static final String METHOD_NAME_EMAIL_PASSWORD = "EmailPassword";
}
