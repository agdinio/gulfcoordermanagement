package com.gulfcouae.ordermanagement.beans;

import java.util.List;

/**
 * Created by relly on 7/28/15.
 */
public class Product {

    private int id;
    private String name;
    private String description;
    private int categoryId;
    private String categoryName;
    private int brandId;
    private String brandName;
    private String image;
    private String code;
    private boolean isPromo;
    private List<ProductUOM> productUOMList;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isPromo() {
        return isPromo;
    }

    public void setIsPromo(boolean isPromo) {
        this.isPromo = isPromo;
    }

    public List<ProductUOM> getProductUOMList() {
        return productUOMList;
    }

    public void setProductUOMList(List<ProductUOM> productUOMList) {
        this.productUOMList = productUOMList;
    }
}
