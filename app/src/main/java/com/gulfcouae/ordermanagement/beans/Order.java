package com.gulfcouae.ordermanagement.beans;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by relly on 7/30/15.
 */
public class Order implements Serializable {

    private int id;
    private int customerId;
    private Date orderDate;
    private String shippingAddress;
    private Date shippingDate;
    private int shippingStatus;
    private String shippingStatusName;
    private String shippingStatusType;
    private int channelId;
    private int agentId;
    private int salesExecutiveId;
    private String transactionNo;
    private double totalAmount;
    private List<OrderDetails> orderDetails;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public Date getOrderDate() {
        return orderDate;
    }

    public void setOrderDate(Date orderDate) {
        this.orderDate = orderDate;
    }

    public String getShippingAddress() {
        return shippingAddress;
    }

    public void setShippingAddress(String shippingAddress) {
        this.shippingAddress = shippingAddress;
    }

    public Date getShippingDate() {
        return shippingDate;
    }

    public void setShippingDate(Date shippingDate) {
        this.shippingDate = shippingDate;
    }

    public int getShippingStatus() {
        return shippingStatus;
    }

    public void setShippingStatus(int shippingStatus) {
        this.shippingStatus = shippingStatus;
    }

    public int getChannelId() {
        return channelId;
    }

    public void setChannelId(int channelId) {
        this.channelId = channelId;
    }

    public int getAgentId() {
        return agentId;
    }

    public void setAgentId(int agentId) {
        this.agentId = agentId;
    }

    public int getSalesExecutiveId() {
        return salesExecutiveId;
    }

    public void setSalesExecutiveId(int salesExecutiveId) {
        this.salesExecutiveId = salesExecutiveId;
    }

    public List<OrderDetails> getOrderDetails() {
        return orderDetails;
    }

    public void setOrderDetails(List<OrderDetails> orderDetails) {
        this.orderDetails = orderDetails;
    }

    public String getTransactionNo() {
        return transactionNo;
    }

    public void setTransactionNo(String transactionNo) {
        this.transactionNo = transactionNo;
    }

    public String getShippingStatusName() {
        return shippingStatusName;
    }

    public void setShippingStatusName(String shippingStatusName) {
        this.shippingStatusName = shippingStatusName;
    }

    public String getShippingStatusType() {
        return shippingStatusType;
    }

    public void setShippingStatusType(String shippingStatusType) {
        this.shippingStatusType = shippingStatusType;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }
}
