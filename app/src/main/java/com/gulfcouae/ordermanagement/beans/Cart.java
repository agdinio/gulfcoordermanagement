package com.gulfcouae.ordermanagement.beans;

import java.io.Serializable;
import java.util.List;

/**
 * Created by relly on 8/1/15.
 */
public class Cart implements Serializable {

    private int id;
    private int customerId;
    private int productId;
    private String productName;
    private double price;
    private int quantity;
    private String image;
    private int uomId;
    private List<ProductUOM> productUOMList;


    public int getCustomerId() {
        return customerId;
    }

    public void setCustomerId(int customerId) {
        this.customerId = customerId;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getUomId() {
        return uomId;
    }

    public void setUomId(int uomId) {
        this.uomId = uomId;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public List<ProductUOM> getProductUOMList() {
        return productUOMList;
    }

    public void setProductUOMList(List<ProductUOM> productUOMList) {
        this.productUOMList = productUOMList;
    }
}
