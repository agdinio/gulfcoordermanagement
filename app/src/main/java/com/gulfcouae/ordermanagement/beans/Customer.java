package com.gulfcouae.ordermanagement.beans;

import java.io.Serializable;

/**
 * Created by relly on 7/26/15.
 */
public class Customer implements Serializable {

    private int id;
    private String firstName;
    private String lastName;
    private String mobile;
    private String email;
    private String username;
    private String password;
    private int emiratesId;
    private int areaId;
    private String address;
    private String type;
    private String emiratesCode;
    private String emiratesName;
    private String areaName;
    private Emirate emirate;
    private Area area;
    private double minAmount;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getEmiratesId() {
        return emiratesId;
    }

    public void setEmiratesId(int emiratesId) {
        this.emiratesId = emiratesId;
    }

    public int getAreaId() {
        return areaId;
    }

    public void setAreaId(int areaId) {
        this.areaId = areaId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmiratesCode() {
        return emiratesCode;
    }

    public void setEmiratesCode(String emiratesCode) {
        this.emiratesCode = emiratesCode;
    }

    public String getEmiratesName() {
        return emiratesName;
    }

    public void setEmiratesName(String emiratesName) {
        this.emiratesName = emiratesName;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public Emirate getEmirate() {
        return emirate;
    }

    public void setEmirate(Emirate emirate) {
        this.emirate = emirate;
    }

    public Area getArea() {
        return area;
    }

    public void setArea(Area area) {
        this.area = area;
    }

    public double getMinAmount() {
        return minAmount;
    }

    public void setMinAmount(double minAmount) {
        this.minAmount = minAmount;
    }
}
