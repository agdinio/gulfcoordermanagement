package com.gulfcouae.ordermanagement.beans;

/**
 * Created by relly on 8/4/15.
 */
public class Area {

    private int id;
    private int emiratesId;
    private String name;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getEmiratesId() {
        return emiratesId;
    }

    public void setEmiratesId(int emiratesId) {
        this.emiratesId = emiratesId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString() {
        return name;
    }
}
