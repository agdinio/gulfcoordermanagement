package com.gulfcouae.ordermanagement;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.ListFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.gulfcouae.ordermanagement.R;
import com.gulfcouae.ordermanagement.beans.Cart;
import com.gulfcouae.ordermanagement.beans.Customer;
import com.gulfcouae.ordermanagement.helpers.Currency;
import com.gulfcouae.ordermanagement.helpers.ThreadHandler;
import com.gulfcouae.ordermanagement.interfaces.BaseFragmentImpl;
import com.gulfcouae.ordermanagement.interfaces.MyCartDetailsImpl;
import com.gulfcouae.ordermanagement.persistents.CartDataSource;
import com.gulfcouae.ordermanagement.persistents.SharedPreferences;
import com.gulfcouae.ordermanagement.statics.Global;
import com.gulfcouae.ordermanagement.sync.ServiceMyCart;
import com.gulfcouae.ordermanagement.utils.FontUtils;

import java.lang.reflect.Field;
import java.util.List;

/**
 * Created by relly on 7/31/15.
 */
public class FragmentMyCart extends ListFragment implements BaseFragmentImpl {


    private View rootView;
    private TextView txtCartSubtotal;
    private TextView txtTotalAmount;
    private Button btnCheckout;
    private LinearLayout filledContainer;
    private LinearLayout emptyContainer;
    private Button btnGoToItems;
    private Button btnContinueShopping;
    private boolean isMinAmountReached;
    private double minAmount;
    private double minAmountBalance;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        View actionBarView = actionBar.getCustomView();
        ((TextView) actionBarView.findViewById(R.id.txtActionBarTitle)).setText(getString(R.string.actionbar_title_mycart));

        rootView = inflater.inflate(R.layout.fragment_mycart, container, false);

        txtCartSubtotal = (TextView) rootView.findViewById(R.id.txtCartSubtotal);
        txtTotalAmount = (TextView) rootView.findViewById(R.id.txtTotalAmount);
        btnCheckout = (Button) rootView.findViewById(R.id.btnCheckout);
        btnCheckout.setOnClickListener(checkoutClickListener);
        filledContainer = (LinearLayout) rootView.findViewById(R.id.filledContainer);
        emptyContainer = (LinearLayout) rootView.findViewById(R.id.emptyContainer);

        btnContinueShopping = (Button) rootView.findViewById(R.id.btnContinueShopping);
        btnContinueShopping.setOnClickListener(gotoItemsClickListener);
        btnGoToItems = (Button) rootView.findViewById(R.id.btnGoToItems);
        btnGoToItems.setOnClickListener(gotoItemsClickListener);

        new ThreadHandler(rootView.getContext()).postDelayed(new Runnable() {
            @Override
            public void run() {
                loadMyCart();
            }
        }, 500);

        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();
    }


    private void loadMyCart() {

        SharedPreferences prefs = new SharedPreferences(rootView.getContext());
        final Customer cust = prefs.fromJson(prefs.getString("customerinfo", ""), Customer.class);
        if (cust != null) {

            CartDataSource dao = new CartDataSource(rootView.getContext());
            dao.open(false);
            List<Cart> carts = dao.getCartByCustomer(cust.getId());
            dao.close();

            if (carts == null || carts.size() < 1) {
                filledContainer.setVisibility(View.GONE);
                emptyContainer.setVisibility(View.VISIBLE);
                return;
            }


            if (carts != null && carts.size() > 0) {

                try {
                    filledContainer.setVisibility(View.VISIBLE);
                    emptyContainer.setVisibility(View.GONE);

                    new ServiceMyCart(rootView.getContext(), getListView(), new MyCartDetailsImpl() {
                        @Override
                        public void refreshCartSubtotal(int items, double totalAmount) {
                            String numItems = String.format("Cart subtotal(%d): ", items);
                            txtCartSubtotal.setText(numItems);
                            txtTotalAmount.setText(Currency.format(totalAmount));
                            minAmount = cust.getMinAmount();

                            if (cust.getMinAmount() > totalAmount) {
                                minAmountBalance = cust.getMinAmount() - totalAmount;
                                isMinAmountReached = false;
                                //btnCheckout.setText("Add another " + Currency.format(bal) + " to checkout");
                            } else {
                                isMinAmountReached = true;
                                //btnCheckout.setText(getString(R.string.proceed_to_checkout));
                            }
                        }

                        @Override
                        public void refreshCartList() {
                            getListView().invalidateViews();
                        }

                        @Override
                        public void refreshLayout(int items) {
                            if (items > 0) {
                                filledContainer.setVisibility(View.VISIBLE);
                                emptyContainer.setVisibility(View.GONE);
                            } else {
                                filledContainer.setVisibility(View.GONE);
                                emptyContainer.setVisibility(View.VISIBLE);
                            }
                        }
                    }).execute();
                } catch(Exception e) {
                    Log.e("MY CART ERROR", e.getMessage());
                }
            }
        }
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        EditText txtQuantity = (EditText) v.findViewById(R.id.txtQuantity);
        txtQuantity.requestFocus();
    }

    private View.OnClickListener checkoutClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            if (isMinAmountReached) {
                FragmentManager fragmentManager = ((FragmentActivity)rootView.getContext()).getSupportFragmentManager();
                FragmentTransaction trans = fragmentManager.beginTransaction();

                Fragment frag = fragmentManager.findFragmentByTag(Global.FRAG_CHECKOUT);
                if (frag == null) {
                    frag = new FragmentCheckout();
                } else {
                    fragmentManager.beginTransaction().remove(frag).commit();
                    frag = new FragmentCheckout();
                }
                trans.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
                trans.replace(R.id.content_frame, frag, Global.FRAG_CHECKOUT);
                trans.addToBackStack(Global.FRAG_CHECKOUT);
                trans.commit();
            } else {
                new AlertDialog.Builder(rootView.getContext())
                        .setCancelable(false)
                        .setTitle("Warning")
                        .setMessage("Your minimum order amount is "
                                + Currency.format(minAmount)
                                + ". Please add another item worth "
                                + Currency.format(minAmountBalance)
                                + " to checkout.")
                        .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create()
                        .show();
            }

        }
    };

    private View.OnClickListener gotoItemsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            FragmentManager fragmentManager = ((FragmentActivity)rootView.getContext()).getSupportFragmentManager();
            FragmentTransaction trans = fragmentManager.beginTransaction();

            Fragment frag = fragmentManager.findFragmentByTag(Global.FRAG_PRODUCT_LIST);
            if (frag == null) {
                frag = new FragmentProductList();
            } else {
                fragmentManager.beginTransaction().remove(frag).commit();
                frag = new FragmentProductList();
            }
            trans.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            trans.replace(R.id.content_frame, frag, Global.FRAG_PRODUCT_LIST);
            trans.commit();
        }
    };

}
