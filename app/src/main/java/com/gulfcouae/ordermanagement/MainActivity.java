package com.gulfcouae.ordermanagement;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.gulfcouae.ordermanagement.R;
import com.gulfcouae.ordermanagement.abstracts.BaseFragment;
import com.gulfcouae.ordermanagement.drawer.NavDrawerAdapter;
import com.gulfcouae.ordermanagement.drawer.NavDrawerItemImpl;
import com.gulfcouae.ordermanagement.drawer.NavMenuItem;
import com.gulfcouae.ordermanagement.helpers.BaseActionBar;
import com.gulfcouae.ordermanagement.helpers.Common;
import com.gulfcouae.ordermanagement.interfaces.BackHandlerInterface;
import com.gulfcouae.ordermanagement.persistents.SharedPreferences;
import com.gulfcouae.ordermanagement.statics.Global;
import com.gulfcouae.ordermanagement.utils.FontUtils;

import java.lang.reflect.Field;
import java.util.List;


/**
 * Created by relly on 7/27/15.
 */

@SuppressLint("NewApi")
public class MainActivity extends ActionBarActivity implements BackHandlerInterface {

    private SharedPreferences prefs;
    private Context context = this;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private BaseActionBar actionBar;
    private CharSequence mMenuTitle;
    private CharSequence mMenuName;
    private String[] mMenuTitles;
    private NavDrawerItemImpl[] menu;
    private BaseFragment selectedFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FontUtils.setDefaultFont(MainActivity.this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Global.Bar = (ProgressBar) findViewById(R.id.pbMain);

        mMenuTitles = getResources().getStringArray(R.array.menu_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        prefs = new SharedPreferences(context);

        if (!prefs.isLoggedIn()) {
            Intent loginIntent = new Intent(MainActivity.this, LoginActivity.class);
            startActivity(loginIntent);
        }

        // set a custom shadow that overlays the main content when the drawer opens
        //mDrawerLayout.setDrawerShadow(R.mipmap.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        menu = new NavDrawerItemImpl[] {
                NavMenuItem.create(101, "Welcome " + prefs.getCustomerInfo().getFirstName(), 0, "productlist", "PRODUCT LIST"),
                NavMenuItem.create(102,"About", 0, "about", "ABOUT"),
                NavMenuItem.create(103, "My Profile", 0, "myprofile", "MY PROFILE"),
                NavMenuItem.create(104, "My Orders", 0, "myorders", "MY ORDERS"),
                NavMenuItem.create(105, "800PERRIER", 0, "tollfree", ""),
                NavMenuItem.create(106, "Log Out", 0, "logout", "Log Out")};
        mDrawerList.setAdapter(new NavDrawerAdapter(this, 0, menu));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(this,
                                                mDrawerLayout,
                                                R.mipmap.ic_drawer,
                                                R.string.drawer_open,
                                                R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                actionBar.toggleSearchButtonVisibility(0);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                actionBar.toggleSearchButtonVisibility(1);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        adjustDrawerWidth();
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        actionBar = new BaseActionBar(MainActivity.this, mDrawerLayout);

        if (savedInstanceState == null) {
            selectFragment(null, 0);
        }

    }


    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectFragment(parent, position);
        }
    }


    private void selectFragment(AdapterView<?> parent, int position) {

        Fragment frag;
        FragmentManager fragmentManager = getSupportFragmentManager();

        if (parent == null) {
            frag = new FragmentProductList();
            fragmentManager
                    .beginTransaction()
                    .replace(R.id.content_frame, frag, Global.FRAG_PRODUCT_LIST)
                    //.addToBackStack(Global.FRAG_PRODUCT_LIST)
                    .commit();
            mMenuTitle = "Product List";
            mMenuName = "productlist";
        } else {
            NavMenuItem item = (NavMenuItem) parent.getAdapter().getItem(position);
            mMenuName = item.getName();
            mMenuTitle = item.getTitle();

            if (mMenuName.toString().equalsIgnoreCase("PRODUCTLIST")) {
                frag = fragmentManager.findFragmentByTag(Global.FRAG_PRODUCT_LIST);
                if (frag == null) {
                    frag = new FragmentProductList();
                } else {
                    fragmentManager.beginTransaction().remove(frag).commit();
                    frag = new FragmentProductList();
                }
                frag = new FragmentProductList();
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.content_frame, frag, Global.FRAG_PRODUCT_LIST)
                        .commit();
            } else if (mMenuName.toString().equalsIgnoreCase("ABOUT")) {
                frag = fragmentManager.findFragmentByTag(Global.FRAG_ABOUT);
                if (frag == null) {
                    frag = new FragmentAbout();
                } else {
                    fragmentManager.beginTransaction().remove(frag).commit();
                    frag = new FragmentAbout();
                }
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.content_frame, frag, Global.FRAG_ABOUT)
                        .addToBackStack(Global.FRAG_ABOUT)
                        .commit();
            } else if (mMenuName.toString().equalsIgnoreCase("MYPROFILE")) {
                frag = fragmentManager.findFragmentByTag(Global.FRAG_MYPROFILE);
                if (frag == null) {
                    frag = new FragmentMyProfile();
                } else {
                    fragmentManager.beginTransaction().remove(frag).commit();
                    frag = new FragmentMyProfile();
                }
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.content_frame, frag, Global.FRAG_MYPROFILE)
                        .addToBackStack(Global.FRAG_MYPROFILE)
                        .commit();
            } else if (mMenuName.toString().equalsIgnoreCase("MYORDERS")) {
                frag = fragmentManager.findFragmentByTag(Global.FRAG_MYORDERS);
                if (frag == null) {
                    frag = new FragmentMyOrderHistory();
                } else {
                    fragmentManager.beginTransaction().remove(frag).commit();
                    frag = new FragmentMyOrderHistory();
                }
                fragmentManager
                        .beginTransaction()
                        .replace(R.id.content_frame, frag, Global.FRAG_MYORDERS)
                        .addToBackStack(Global.FRAG_MYORDERS)
                        .commit();
            } else if (mMenuName.toString().equalsIgnoreCase("TOLLFREE")) {
                Intent intentCall = new Intent(Intent.ACTION_CALL);
                intentCall.setData(Uri.parse("tel:800" + Common.convertToPhone("PERRIER")));
                startActivity(intentCall);
            } else if (mMenuName.toString().equalsIgnoreCase("LOGOUT")) {
                confirmLogout();
            }
        }

        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
        mDrawerLayout.closeDrawer(mDrawerList);

    }


    private boolean onBackPressed(FragmentManager fm) {
        if (fm != null) {
            if (fm.getBackStackEntryCount() > 0) {
                    fm.popBackStack();
                    return true;
            }

            List<Fragment> fragments = fm.getFragments();
            if (fragments != null && fragments.size() > 0) {
                for (Fragment frag : fragments) {
                    if (frag == null) {
                        continue;
                    }
                    if (frag.isVisible()) {
                        if (onBackPressed(frag.getChildFragmentManager())) {
                            return true;
                        }
                    }
                }
            }
        }

        return false;
    }

    @Override
    public void onBackPressed() {

        FragmentManager fm = getSupportFragmentManager();
        if (onBackPressed(fm)) {
            return;
        }
        //super.onBackPressed();
/*
        try {
            FragmentManager.BackStackEntry backStackEntry = getSupportFragmentManager()
                    .getBackStackEntryAt(getSupportFragmentManager()
                            .getBackStackEntryCount() - 1);
            Fragment prev = getSupportFragmentManager().findFragmentByTag(backStackEntry.getName());
            if (prev == null) {
                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.content_frame, new FragmentProductList(), Global.FRAG_PRODUCT_LIST)
                        .commit();
            }
*/
//            FragmentManager.BackStackEntry backStackEntry = getSupportFragmentManager()
//                    .getBackStackEntryAt(getSupportFragmentManager()
//                            .getBackStackEntryCount() - 1);
//            Fragment prev = getSupportFragmentManager().findFragmentByTag(backStackEntry.getName());
//            getSupportFragmentManager()
//                    .beginTransaction()
//                    .replace(R.id.content_frame, prev, backStackEntry.getName())
//                    .addToBackStack(backStackEntry.getName())
//                    .commit();

                //**
//            if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
//                getSupportFragmentManager().popBackStackImmediate();
//                getSupportFragmentManager().beginTransaction().commit();
//            }

/*
        } catch(Exception e) {
            Log.e("BACK PRESSED ERROR", e.getMessage());
        }
*/
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    private void adjustDrawerWidth() {

        Display display = getWindowManager().getDefaultDisplay();

        Point size = new Point();
        display.getSize(size);

        DisplayMetrics metrics = new DisplayMetrics();
        display.getMetrics(metrics);

        try {

            DrawerLayout.LayoutParams layoutParams = (DrawerLayout.LayoutParams) mDrawerList.getLayoutParams();
            layoutParams.width = (size.x - 100);
            mDrawerList.setLayoutParams(layoutParams);

        } catch(Exception e) {
            e.printStackTrace();
        }

    }

    private void confirmLogout() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivity.this);
        dialog.setCancelable(false);
        dialog.setTitle("Confirm");
        dialog.setMessage("Are you sure you want to log out?");
        dialog.setPositiveButton("Log out", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                handleCustomerInfo();
            }
        });
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        dialog.create().show();

    }

    private void handleCustomerInfo() {
        prefs.editMode();
        prefs.putString("customerinfo", "");
        prefs.putBoolean("rememberme", false);
        prefs.commitMode();

        Intent i = new Intent(context, LoginActivity.class);
        startActivity(i);
    }

}

