package com.gulfcouae.ordermanagement;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.gulfcouae.ordermanagement.R;
import com.gulfcouae.ordermanagement.beans.Customer;
import com.gulfcouae.ordermanagement.interfaces.CustomerImpl;
import com.gulfcouae.ordermanagement.persistents.SharedPreferences;
import com.gulfcouae.ordermanagement.sync.InitTask;
import com.gulfcouae.ordermanagement.sync.ServiceSendPassword;
import com.gulfcouae.ordermanagement.sync.ServiceUserAuth;
import com.gulfcouae.ordermanagement.utils.FontUtils;

/**
 * Created by relly on 7/26/15.
 */
public class LoginActivity extends Activity {

    private Context context = this;
    private ProgressDialog dialog;
    private InitTask initTask;
    private EditText txtUsername;
    private EditText txtPassword;
    private Button btnLogin;
    private TextView lblRegister;
    private TextView lblForgot;
    private CheckBox chkRememberMe;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FontUtils.setDefaultFont(LoginActivity.this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        txtUsername = (EditText) findViewById(R.id.txtUsername);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        chkRememberMe = (CheckBox) findViewById(R.id.chkRememberMe);

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(loginClickListener);

        lblForgot = (TextView) findViewById(R.id.lblForgot);
        lblForgot.setOnClickListener(forgotPassClickListener);

        lblRegister = (TextView) findViewById(R.id.lblRegister);
        lblRegister.setOnClickListener(registerClickListener);

        SharedPreferences prefs = new SharedPreferences(context);
        if (prefs.isRememberMe()) {
            Intent mainIntent = new Intent(this, MainActivity.class);
            startActivity(mainIntent);
        }

    }

    private View.OnClickListener loginClickListener = new View.OnClickListener() {
        @SuppressLint("NewApi")
        @Override
        public void onClick(View v) {
            if (txtUsername.getText().toString().isEmpty()) {
                Toast.makeText(getApplicationContext(),
                        "Please Enter a User Name", Toast.LENGTH_SHORT)
                        .show();
                return;
            } else if (txtPassword.getText().toString().isEmpty()) {
                Toast.makeText(getApplicationContext(),
                        "Please Enter a Password", Toast.LENGTH_SHORT)
                        .show();
                return;
            }

            initTask = new InitTask();
            initTask.execute(context);

            hideKeyboard();
            
            dialog  = ProgressDialog.show(context, "", "Signing in...");

            Object[] params = { dialog,
                                txtUsername.getText().toString(),
                                txtPassword.getText().toString(),
                                chkRememberMe.isChecked() };
            ServiceUserAuth thread = new ServiceUserAuth(context, params);
            thread.start();
        }
    };

    private View.OnClickListener forgotPassClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            final Dialog dialog = new Dialog(LoginActivity.this);
            dialog.getWindow();
            dialog.setCancelable(false);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_forgot_password);

            final Window w = dialog.getWindow();
            w.setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            w.setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

            final TextView txtEmail = (TextView) w.findViewById(R.id.txtEmail);
            Button btnSubmit = (Button) w.findViewById(R.id.btnSubmit);
            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (TextUtils.isEmpty(txtEmail.getText())) {
                        Toast.makeText(LoginActivity.this, "Email should not be empty.", Toast.LENGTH_LONG).show();
                        return;
                    }

                    new ServiceSendPassword(LoginActivity.this, new CustomerImpl.Null() {
                        @Override
                        public void sendPasswordCallback(Boolean result) {
                            if (result) {
                                Toast.makeText(LoginActivity.this, "A password has been sent to your email.", Toast.LENGTH_LONG).show();
                            }
                        }
                    }).execute(txtEmail.getText().toString());
                    dialog.dismiss();
                    hideKeyboard();
                }
            });
            Button btnCancel = (Button) w.findViewById(R.id.btnCancelSubmit);
            btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    hideKeyboard();
                }
            });

            dialog.show();
        }
    };

    private View.OnClickListener registerClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intentRegister = new Intent(context, RegistrationActivity.class);
            startActivity(intentRegister);
        }
    };


    private void hideKeyboard() {
        View softview = getCurrentFocus();
        if (softview != null) {
            InputMethodManager im = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(softview.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    @Override
    public void onBackPressed() {
        //moveTaskToBack(true);
        //finish();
        //
        // android.os.Process.killProcess(android.os.Process.myPid());

        Intent startMain = new Intent(Intent.ACTION_MAIN);
        startMain.addCategory(Intent.CATEGORY_HOME);
        startMain.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(startMain);

    }

}
