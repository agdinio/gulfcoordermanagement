package com.gulfcouae.ordermanagement.helpers;

import android.content.Context;
import android.graphics.drawable.LayerDrawable;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gulfcouae.ordermanagement.FragmentMyCart;

import com.gulfcouae.ordermanagement.FragmentSearch;
import com.gulfcouae.ordermanagement.R;
import com.gulfcouae.ordermanagement.beans.Cart;
import com.gulfcouae.ordermanagement.beans.Customer;
import com.gulfcouae.ordermanagement.persistents.CartDataSource;
import com.gulfcouae.ordermanagement.persistents.SharedPreferences;
import com.gulfcouae.ordermanagement.statics.Global;
import com.gulfcouae.ordermanagement.utils.Utils;

import java.util.List;

/**
 * Created by relly on 7/27/15.
 */
public class BaseActionBar {

    private Context context;
    private View actionBarLayout;
    private ActionBar supportActionBar;
    private DrawerLayout mDrawerLayout;
    private ImageButton btnDrawer;
    private ImageButton btnSearch;
    private ImageButton btnCart;
    private LayerDrawable icon;
    private TextView txtActionBarTitle;
    private int mItemCount;


    public BaseActionBar(Context context, DrawerLayout mDrawerLayout) {
        this.context = context;
        this.mDrawerLayout = mDrawerLayout;

        actionBarLayout = ((ActionBarActivity)context).getLayoutInflater().inflate(R.layout.base_actionbar, null);
        supportActionBar = ((ActionBarActivity)context).getSupportActionBar();
        supportActionBar.setDisplayShowHomeEnabled(false);
        supportActionBar.setDisplayShowTitleEnabled(false);
        supportActionBar.setDisplayShowCustomEnabled(true);
        supportActionBar.setCustomView(actionBarLayout);

        Toolbar parent = (Toolbar) supportActionBar.getCustomView().getParent();
        parent.setContentInsetsAbsolute(0, 0);

        btnDrawer = (ImageButton) actionBarLayout.findViewById(R.id.btnDrawer);
        btnDrawer.setOnClickListener(btnDrawerClickListener);

        btnSearch = (ImageButton) actionBarLayout.findViewById(R.id.btnSearch);
        btnSearch.setOnClickListener(btnSearchClickListener);

        btnCart = (ImageButton) actionBarLayout.findViewById(R.id.btnCart);
        btnCart.setOnClickListener(btnCartClickListener);

        icon = (LayerDrawable) btnCart.getDrawable();

        txtActionBarTitle = (TextView) actionBarLayout.findViewById(R.id.txtActionBarTitle);

        updateBadge();
    }

    private void toggleDrawer() {
        if (mDrawerLayout.isDrawerOpen(Gravity.LEFT)) {
            mDrawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            mDrawerLayout.openDrawer(Gravity.LEFT);
        }
    }

    private void closeDrawer() {
        mDrawerLayout.closeDrawer(Gravity.LEFT);
    }

    public void toggleSearchButtonVisibility(int val) {
        if (val == 0) {
            btnSearch.setVisibility(View.INVISIBLE);
        } else {
            btnSearch.setVisibility(View.VISIBLE);
        }

    }
    public void refreshTitle(String title) {
        txtActionBarTitle.setText(title);
    }

    private void updateBadge() {
        SharedPreferences prefs = new SharedPreferences(context);
        String jsonString = prefs.getString("customerinfo", "");
        Customer cust = prefs.fromJson(jsonString, Customer.class);

        if (cust != null) {
            CartDataSource dao = new CartDataSource(context);
            dao.open(false);
            List<Cart> carts = dao.getCartByCustomer(cust.getId());
            dao.close();

            if (carts != null && carts.size() > 0) {
                mItemCount = carts.size();
            } else {
                mItemCount = 0;
            }

            Utils.setBadgeCount(context, icon, mItemCount);
        }

    }

    private View.OnClickListener btnDrawerClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Common.hideKeyboard(context);
            toggleDrawer();
        }
    };

    private View.OnClickListener btnSearchClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    FragmentManager fragmentManager = ((FragmentActivity) context).getSupportFragmentManager();

                    FragmentTransaction trans = fragmentManager.beginTransaction();


                    Fragment frag = fragmentManager.findFragmentByTag(Global.FRAG_SEARCH);
                    if (frag == null){
                        frag = new FragmentSearch();
                    }
                    trans.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left);
                    trans.replace(R.id.content_frame, frag, Global.FRAG_SEARCH);
                    trans.addToBackStack(Global.FRAG_SEARCH);
                    trans.commit();

                }
            }, 500);

            closeDrawer();
        }
    };

    private View.OnClickListener btnCartClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Common.hideKeyboard(context);

            FragmentManager fragmentManager = ((FragmentActivity)context).getSupportFragmentManager();
            Fragment frag = fragmentManager.findFragmentByTag(Global.FRAG_MYCART);
            if (frag == null) {
                frag = new FragmentMyCart();
            } else {
                fragmentManager.beginTransaction().remove(frag).commit();
                frag = new FragmentMyCart();
            }
            fragmentManager.
                    beginTransaction()
                    .replace(R.id.content_frame, frag, Global.FRAG_MYCART)
                    .addToBackStack(Global.FRAG_MYCART)
                    .commit();

            refreshTitle("MY CART");
            closeDrawer();
        }
    };

}
