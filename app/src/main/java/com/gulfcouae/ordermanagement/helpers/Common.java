package com.gulfcouae.ordermanagement.helpers;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.gulfcouae.ordermanagement.R;
import com.gulfcouae.ordermanagement.beans.Cart;
import com.gulfcouae.ordermanagement.beans.Customer;
import com.gulfcouae.ordermanagement.persistents.CartDataSource;
import com.gulfcouae.ordermanagement.persistents.SharedPreferences;
import com.gulfcouae.ordermanagement.utils.Utils;

import java.util.List;

/**
 * Created by relly on 8/2/15.
 */
public class Common {


    public static void hideKeyboard(Context context) {

        FragmentActivity activity = (FragmentActivity)context;

        View softview = activity.getCurrentFocus();
        if (softview != null) {
            InputMethodManager im = (InputMethodManager) activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            im.hideSoftInputFromWindow(softview.getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }

    public static void updateBadge(Context context) {
        ActionBar actionBar = ((ActionBarActivity)context).getSupportActionBar();
        View actionBarView = actionBar.getCustomView();

        ImageButton btnCart = (ImageButton) actionBarView.findViewById(R.id.btnCart);
        LayerDrawable icon = (LayerDrawable) btnCart.getDrawable();

        SharedPreferences prefs = new SharedPreferences(context);
        Customer cust = prefs.getCustomerInfo();

        int mItemCount = 0;

        if (cust != null) {
            CartDataSource dao = new CartDataSource(context);
            dao.open(false);
            List<Cart> carts = dao.getCartByCustomer(cust.getId());
            dao.close();

            if (carts != null && carts.size() > 0) {
                mItemCount = carts.size();
            } else {
                mItemCount = 0;
            }

            Utils.setBadgeCount(context, icon, mItemCount);
        }

    }

    public static String convertToPhone(String numString) {

        String phone = "";

        for (int i=0; i<numString.length(); i++) {
            char c = numString.charAt(i);
            switch (c) {
                case 'a':
                case 'b':
                case 'c':
                case 'A':
                case 'B':
                case 'C':
                    phone += "2";
                    break;
                case 'd':
                case 'e':
                case 'f':
                case 'D':
                case 'E':
                case 'F':
                    phone += "3";
                    break;
                case 'g':
                case 'h':
                case 'i':
                case 'G':
                case 'H':
                case 'I':
                    phone += "4";
                    break;
                case 'j':
                case 'k':
                case 'l':
                case 'J':
                case 'K':
                case 'L':
                    phone += "5";
                    break;
                case 'm':
                case 'n':
                case 'o':
                case 'M':
                case 'N':
                case 'O':
                    phone += "6";
                    break;
                case 'p':
                case 'q':
                case 'r':
                case 's':
                case 'P':
                case 'Q':
                case 'R':
                case 'S':
                    phone += "7";
                    break;
                case 't':
                case 'u':
                case 'v':
                case 'T':
                case 'U':
                case 'V':
                    phone += "8";
                    break;
                case 'w':
                case 'x':
                case 'y':
                case 'z':
                case 'W':
                case 'X':
                case 'Y':
                case 'Z':
                    phone += "9";
                    break;

            }

        }
        return phone;
    }


    public static Integer[] getQuantities() {

        int ctr = 99;
        Integer[] quantities = new Integer[ctr];

        for (int i = 0; i< ctr; i++) {
            quantities[i] = i + 1;
        }

        return quantities;
    }

    public static ArrayAdapter<Integer> quantityArrayAdapter(Context context) {
        ArrayAdapter<Integer> adapter = new ArrayAdapter<>(context,
                R.layout.dropdown_quantity,
                Common.getQuantities());
        return adapter;
    }

    public static boolean isNumeric(String str) {
        return TextUtils.isDigitsOnly(str);
    }

}
