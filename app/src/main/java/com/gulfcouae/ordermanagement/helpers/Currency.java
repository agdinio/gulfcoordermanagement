package com.gulfcouae.ordermanagement.helpers;

import java.text.DecimalFormat;

/**
 * Created by relly on 8/1/15.
 */
public class Currency {

    public static String format(double toFormat) {

        DecimalFormat df = new DecimalFormat("###,##0.00 AED");

        return df.format(toFormat);
    }

    public static String formatPercent(double toFormat) {

        DecimalFormat df = new DecimalFormat("##0.##%");

        return df.format(toFormat);
    }

}
