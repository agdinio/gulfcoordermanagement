package com.gulfcouae.ordermanagement.helpers;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.gulfcouae.ordermanagement.beans.Cart;
import com.gulfcouae.ordermanagement.statics.Global;

/**
 * Created by relly on 7/28/15.
 */
public class ThreadHandler extends Handler {

    private Context context;

    public ThreadHandler(Context context) {
        super(Looper.getMainLooper());
        this.context = context;
    }

    public static final int INVALID_ACCOUNT = 1;
    public static final int LOGIN_FAILED = 2;
    public static final int SERVER_FAILED = 3;
    public static final int CART_ADD_FAILED = 4;
    public static final int CART_ADD_SUCCESS = 5;
    public static final int CART_UPDATE_FAILED = 6;
    public static final int CART_UPDATE_SUCCESS = 7;
    public static final int CART_QUANTITY_ZERO = 8;
    public static final int REGISTRATION_FAILED = 9;
    public static final int UPDATE_SHIP_INFO_FAILED = 10;
    public static final int SHOW_PROGRESSBAR = 11;
    public static final int HIDE_PROGRESSBAR = 12;
    public static final int CART_UPDATE_POSITION_FAILED = 13;
    public static final int UPDATE_CUSTOMER_FAILED = 14;
    public static final int UPDATE_CUSTOMER_SUCCESS = 15;
    public static final int PROGRESSBAR_SHOW = 16;
    public static final int PROGRESSBAR_HIDE = 17;


    @Override
    public void handleMessage(Message msg) {

        switch (msg.what) {
            case INVALID_ACCOUNT:
                Toast.makeText(context, "Invalid username and/or password.", Toast.LENGTH_LONG).show();
                break;
            case LOGIN_FAILED:
                Toast.makeText(context, "Login failed.", Toast.LENGTH_LONG).show();
                break;
            case SERVER_FAILED:
                Toast.makeText(context, "Connection to server failed.", Toast.LENGTH_LONG).show();
                break;
            case CART_UPDATE_FAILED:
                Toast.makeText(context, "The changes were not saved. Please try again.", Toast.LENGTH_LONG).show();
                break;
            case CART_ADD_FAILED:
                Toast.makeText(context, "Add to Cart failed. Please try again.", Toast.LENGTH_LONG).show();
                break;
            case CART_ADD_SUCCESS:
                Cart c = (Cart) msg.obj;
                Toast.makeText(context, c.getProductName() + " has been added to your cart.", Toast.LENGTH_LONG).show();
                break;
            case CART_UPDATE_SUCCESS:
                Toast.makeText(context, "Cart updated successfully.", Toast.LENGTH_LONG).show();
                break;
            case CART_QUANTITY_ZERO:
                Toast.makeText(context, "Quantity should not be less than zero.", Toast.LENGTH_LONG).show();
                break;
            case REGISTRATION_FAILED:
                Toast.makeText(context, "ServiceRegistration failed. Please try again.", Toast.LENGTH_LONG).show();
                break;
            case UPDATE_SHIP_INFO_FAILED:
                Toast.makeText(context, "The changes were not saved. Please try again.", Toast.LENGTH_LONG).show();
                break;
            case SHOW_PROGRESSBAR:
                ProgressBar progressBar = (ProgressBar) msg.obj;
                progressBar.setVisibility(View.VISIBLE);
                break;
            case HIDE_PROGRESSBAR:
                progressBar = (ProgressBar) msg.obj;
                progressBar.setVisibility(View.INVISIBLE);
                progressBar.invalidate();
                break;
            case CART_UPDATE_POSITION_FAILED:
                Toast.makeText(context, "You have made changes on the wrong item. Please try again.", Toast.LENGTH_LONG).show();
                break;
            case UPDATE_CUSTOMER_FAILED:
                Toast.makeText(context, "The changes were not saved. Please try again.", Toast.LENGTH_LONG).show();
                break;

            case UPDATE_CUSTOMER_SUCCESS:
                Toast.makeText(context, "Customer Profile updated successfully.", Toast.LENGTH_LONG).show();
                break;
            case PROGRESSBAR_SHOW:
                if (Global.Bar != null) {
                    Global.Bar.setVisibility(View.VISIBLE);
                }
                break;
            case PROGRESSBAR_HIDE:
                if (Global.Bar != null) {
                    Global.Bar.setVisibility(View.GONE);
                }
                break;
        }

    }
}
