package com.gulfcouae.ordermanagement.interfaces;

import com.gulfcouae.ordermanagement.abstracts.BaseFragment;

/**
 * Created by relly on 8/20/15.
 */
public interface BackHandlerInterface {

    //void setSelectedFragment(BaseFragment baseFragment);

    void onBackPressed();

}
