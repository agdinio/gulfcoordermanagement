package com.gulfcouae.ordermanagement.interfaces;

/**
 * Created by relly on 8/11/15.
 */
public interface CustomerImpl {

    void closePassDialog();
    void sendPasswordCallback(Boolean result);

    public static class Null implements CustomerImpl {

        @Override
        public void closePassDialog() {

        }

        @Override
        public void sendPasswordCallback(Boolean result) {

        }
    }
}
