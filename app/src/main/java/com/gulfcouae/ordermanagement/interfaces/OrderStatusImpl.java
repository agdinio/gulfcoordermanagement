package com.gulfcouae.ordermanagement.interfaces;

import com.gulfcouae.ordermanagement.beans.OrderStatus;

import java.util.List;

/**
 * Created by relly on 8/16/15.
 */
public interface OrderStatusImpl {

    void setList(List<OrderStatus> list);
    void setStatusId(int statusId);

    public static class Null implements OrderStatusImpl {

        @Override
        public void setList(List<OrderStatus> list) {

        }

        @Override
        public void setStatusId(int statusId) {

        }
    }

}
