package com.gulfcouae.ordermanagement.interfaces;

import com.gulfcouae.ordermanagement.beans.Emirate;

import java.util.List;

/**
 * Created by relly on 8/4/15.
 */
public interface EmirateImpl {

    void refreshList(List<Emirate> emirates);
    void put(Emirate emirate);

    public static class Null implements EmirateImpl {

        @Override
        public void refreshList(List<Emirate> emirates) {

        }

        @Override
        public void put(Emirate emirate) {

        }
    }

}
