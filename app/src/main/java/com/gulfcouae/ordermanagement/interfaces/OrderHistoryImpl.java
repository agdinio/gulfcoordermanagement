package com.gulfcouae.ordermanagement.interfaces;

import com.gulfcouae.ordermanagement.beans.Order;
import com.gulfcouae.ordermanagement.beans.OrderDetailsHistory;

import java.util.List;

/**
 * Created by relly on 8/11/15.
 */
public interface OrderHistoryImpl {

    void showList(List<Order> orders);
    void showOrderDetailsList(List<OrderDetailsHistory> list);

    public static class Null implements OrderHistoryImpl {

        @Override
        public void showList(List<Order> orders) {

        }

        @Override
        public void showOrderDetailsList(List<OrderDetailsHistory> list) {

        }

    }

}
