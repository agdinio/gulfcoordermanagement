package com.gulfcouae.ordermanagement.interfaces;

/**
 * Created by relly on 8/2/15.
 */
public interface MyCartDetailsImpl {

    void refreshCartSubtotal(int items, double totalAmount);
    void refreshCartList();
    void refreshLayout(int items);

    public static class Null implements MyCartDetailsImpl {

        @Override
        public void refreshCartSubtotal(int items, double totalAmount) {

        }

        @Override
        public void refreshCartList() {

        }

        @Override
        public void refreshLayout(int items) {

        }
    }
}
