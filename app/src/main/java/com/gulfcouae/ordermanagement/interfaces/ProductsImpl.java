package com.gulfcouae.ordermanagement.interfaces;

import com.gulfcouae.ordermanagement.beans.Product;

import java.util.List;

/**
 * Created by relly on 8/16/15.
 */
public interface ProductsImpl {

    void showList(List<Product> products);

}
