package com.gulfcouae.ordermanagement.interfaces;

import com.gulfcouae.ordermanagement.beans.Area;

import java.util.List;

/**
 * Created by relly on 8/4/15.
 */
public interface AreaImpl {

    void refreshList(List<Area> areas);

}
