package com.gulfcouae.ordermanagement.interfaces;

import com.gulfcouae.ordermanagement.beans.Product;

/**
 * Created by relly on 8/1/15.
 */
public interface ProductDetailsImpl {

    void getProductFromWS(Product product);

}
