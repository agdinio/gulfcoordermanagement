package com.gulfcouae.ordermanagement.sync;

import android.content.Context;
import android.os.AsyncTask;

import com.gulfcouae.ordermanagement.beans.Emirate;
import com.gulfcouae.ordermanagement.helpers.ThreadHandler;
import com.gulfcouae.ordermanagement.interfaces.EmirateImpl;
import com.gulfcouae.ordermanagement.statics.Global;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by relly on 8/4/15.
 */
public class ServiceEmirates extends AsyncTask<Void, Void, List<Emirate>> {

    private Context context;
    private EmirateImpl callback;

    public ServiceEmirates(Context context, EmirateImpl callback) {
        this.context = context;
        this.callback = callback;

        new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_SHOW);
    }

    @Override
    protected List<Emirate> doInBackground(Void... params) {
        List<Emirate> list = null;

        SoapObject request = new SoapObject(Global.NAMESPACE, "GetEmirates");

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(Global.SERVICE_URL);
        try {
            androidHttpTransport.call(String.format("%s%s", Global.SOAP_ACTION, "IOrderManagement/GetEmirates"), envelope);

            SoapObject result = (SoapObject) envelope.getResponse();
            if (result != null && result.getPropertyCount() > 0) {
                list = new ArrayList<>();
                for (int i = 0; i<result.getPropertyCount(); i++) {
                    SoapObject item = (SoapObject) result.getProperty(i);

                    Emirate emirate = new Emirate();
                    emirate.setId(Integer.parseInt(item.getProperty("Id").toString()));
                    emirate.setCode(item.getProperty("Code") == null ? "" : item.getProperty("Code").toString());
                    emirate.setName(item.getProperty("Name") == null ? "" : item.getProperty("Name").toString());

                    list.add(emirate);
                }

            }

        } catch (Exception e) {
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_HIDE);
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.SERVER_FAILED);
        } finally {
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_HIDE);
        }

        return list;

    }

    @Override
    protected void onPostExecute(List<Emirate> emirates) {

        callback.refreshList(emirates);
    }
}

