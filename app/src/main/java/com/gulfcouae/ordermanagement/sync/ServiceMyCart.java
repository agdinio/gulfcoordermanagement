package com.gulfcouae.ordermanagement.sync;

import android.content.Context;
import android.os.AsyncTask;
import android.widget.ListView;

import com.gulfcouae.ordermanagement.adapters.CartAdapter;
import com.gulfcouae.ordermanagement.beans.Cart;
import com.gulfcouae.ordermanagement.beans.Customer;
import com.gulfcouae.ordermanagement.helpers.ThreadHandler;
import com.gulfcouae.ordermanagement.interfaces.MyCartDetailsImpl;
import com.gulfcouae.ordermanagement.persistents.CartDataSource;
import com.gulfcouae.ordermanagement.persistents.SharedPreferences;

import java.util.List;

/**
 * Created by relly on 7/29/15.
 */
public class ServiceMyCart extends AsyncTask<Void, Void, List<Cart>> {

    private Context context;
    private ListView listView;
    private MyCartDetailsImpl callback;

    public ServiceMyCart(Context context, ListView listView, MyCartDetailsImpl callback) {
        this.context = context;
        this.listView = listView;
        this.callback = callback;

        new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_SHOW);
    }

    @Override
    protected List<Cart> doInBackground(Void... params) {
        List<Cart> list = null;

        SharedPreferences prefs = new SharedPreferences(context);
        String jsonString = prefs.getString("customerinfo", "");
        Customer cust = prefs.fromJson(jsonString, Customer.class);
        if (cust != null) {
            CartDataSource dao = new CartDataSource(context);
            dao.open(false);
            list = dao.getCartByCustomer(cust.getId());
            dao.close();
        }

        return list;

    }

    @Override
    protected void onPostExecute(List<Cart> carts) {
        if (carts != null) {

            double totalAmount = 0;
            for (Cart c : carts) {
                totalAmount += (c.getPrice() * c.getQuantity());
            }

            callback.refreshCartSubtotal(carts.size(), totalAmount);

            CartAdapter adapter = new CartAdapter(context, callback, carts);

            listView.setAdapter(adapter);
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_HIDE);
        }
    }
}
