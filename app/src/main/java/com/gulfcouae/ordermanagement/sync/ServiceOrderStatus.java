package com.gulfcouae.ordermanagement.sync;

import android.os.AsyncTask;

import com.gulfcouae.ordermanagement.beans.OrderStatus;
import com.gulfcouae.ordermanagement.interfaces.OrderStatusImpl;
import com.gulfcouae.ordermanagement.statics.Global;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by relly on 8/16/15.
 */
public class ServiceOrderStatus extends AsyncTask<Void, Void, List<OrderStatus>> {

    private OrderStatusImpl callback;

    public ServiceOrderStatus(OrderStatusImpl callback) {
        this.callback = callback;
    }

    @Override
    protected List<OrderStatus> doInBackground(Void... params) {
         List<OrderStatus> list = null;

        SoapObject request = new SoapObject(Global.NAMESPACE, Global.METHOD_NAME_GETORDERSTATUS);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(Global.SERVICE_URL);
        try {
            androidHttpTransport.call(String.format("%s%s", Global.SOAP_ACTION, Global.SOAP_ACTION_GETORDERSTATUS), envelope);

            SoapObject result = (SoapObject) envelope.getResponse();
            if (result != null && result.getPropertyCount() > 0) {
                list = new ArrayList<>();
                for (int i = 0; i<result.getPropertyCount(); i++) {
                    SoapObject item = (SoapObject) result.getProperty(i);

                    OrderStatus status = new OrderStatus();
                    status.setId(Integer.parseInt(item.getProperty("Id").toString()));
                    status.setName(item.getProperty("Name") == null ? "" : item.getProperty("Name").toString());
                    status.setType(item.getProperty("Type") == null ? "" : item.getProperty("Type").toString());
                    list.add(status);
                }

            }



        } catch (Exception e) {
            return null;
        }


        return list;
    }

    @Override
    protected void onPostExecute(List<OrderStatus> list) {

        callback.setList(list);

    }
}
