package com.gulfcouae.ordermanagement.sync;

import android.content.Context;
import android.os.AsyncTask;

import com.gulfcouae.ordermanagement.helpers.ThreadHandler;
import com.gulfcouae.ordermanagement.interfaces.OrderHistoryImpl;
import com.gulfcouae.ordermanagement.statics.Global;
import com.gulfcouae.ordermanagement.statics.Order;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by relly on 7/29/15.
 */
public class ServiceOrderHistory extends AsyncTask<Integer, Void, List<com.gulfcouae.ordermanagement.beans.Order>> {

    private Context context;
    private OrderHistoryImpl callback;

    public ServiceOrderHistory(Context context, OrderHistoryImpl callback) {
        this.context = context;
        this.callback = callback;

        new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_SHOW);
    }

    @Override
    protected List<com.gulfcouae.ordermanagement.beans.Order> doInBackground(Integer... params) {
        List<com.gulfcouae.ordermanagement.beans.Order> list = null;
        Integer param = params[0];

        SoapObject request = new SoapObject(Global.NAMESPACE, Order.METHOD_NAME_GETORDERHISTORY);
        request.addProperty("customerId", param);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(Global.SERVICE_URL);
        try {
            androidHttpTransport.call(String.format("%s%s", Global.SOAP_ACTION, Order.SOAP_ACTION_GETORDERHISTORY), envelope);

            SoapObject result = (SoapObject) envelope.getResponse();
            if (result != null && result.getPropertyCount() > 0) {
                list = new ArrayList<>();

                for (int i = 0; i<result.getPropertyCount(); i++) {
                    SoapObject item = (SoapObject) result.getProperty(i);
                    if (item != null) {

                        SimpleDateFormat dateformatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");

                        com.gulfcouae.ordermanagement.beans.Order order = new com.gulfcouae.ordermanagement.beans.Order();
                        order.setId(Integer.parseInt(item.getProperty("Id").toString()));
                        order.setCustomerId(item.getProperty("CustomerId") == null ? 0 : Integer.parseInt(item.getProperty("CustomerId").toString()));
                        order.setOrderDate(item.getProperty("OrderDate") == null ? dateformatter.parse("1900-01-01 00:00") : dateformatter.parse(item.getProperty("OrderDate").toString().replace('T', ' ')));
                        order.setShippingAddress(item.getProperty("ShippingAddress") == null ? "" : item.getProperty("ShippingAddress").toString());
                        order.setShippingDate(item.getProperty("ShippingDate") == null ? dateformatter.parse("1900-01-01") : dateformatter.parse(item.getProperty("ShippingDate").toString().replace('T', ' ')));
                        order.setShippingStatus(item.getProperty("ShippingStatus") == null ? 0 : Integer.parseInt(item.getProperty("ShippingStatus").toString()));
                        order.setShippingStatusName(item.getProperty("ShippingStatusName") == null ? "" : item.getProperty("ShippingStatusName").toString());
                        order.setShippingStatusType(item.getProperty("ShippingStatusName") == null ? "" : item.getProperty("ShippingStatusType").toString());
                        order.setChannelId(item.getProperty("ChannelId") == null ? 0 : Integer.parseInt(item.getProperty("ChannelId").toString()));
                        order.setAgentId(item.getProperty("AgentId") == null ? 0 : Integer.parseInt(item.getProperty("AgentId").toString()));
                        order.setSalesExecutiveId(item.getProperty("SalesExecutiveId") == null ? 0 : Integer.parseInt(item.getProperty("SalesExecutiveId").toString()));
                        order.setTransactionNo(item.getProperty("TransactionNo") == null ? "" : item.getProperty("TransactionNo").toString());
                        order.setTotalAmount(item.getProperty("TotalAmount") == null ? 0 : Double.parseDouble(item.getProperty("TotalAmount").toString()));

//                        SoapObject soapDetails = (SoapObject) item.getProperty("OrderDetails");
//                        if (soapDetails != null && soapDetails.getPropertyCount() > 0) {
//                            List<OrderDetails> details = new ArrayList<>();
//                            for (int j=0; j<soapDetails.getPropertyCount(); j++) {
//                                SoapObject soapdetail = (SoapObject) soapDetails.getProperty(j);

//                                OrderDetails detail = new OrderDetails();
//                                detail.setId(Integer.parseInt(soapdetail.getProperty("Id").toString()));
//                                detail.setOrderId(soapdetail.getProperty("OrderId") == null ? 0 : Integer.parseInt(soapdetail.getProperty("OrderId").toString()));
//                                detail.setProductId(soapdetail.getProperty("ProductId") == null ? 0 : Integer.parseInt(soapdetail.getProperty("ProductId").toString()));
//                                detail.setQuantity(soapdetail.getProperty("Quantity") == null ? 0 : Integer.parseInt(soapdetail.getProperty("Quantity").toString()));
//                                detail.setConfirmed(soapdetail.getProperty("Confirmed") == null ? false : Boolean.parseBoolean(soapdetail.getProperty("Confirmed").toString()));
//
//                                details.add(detail);
//                            }
//
//                            order.setOrderDetails(details);
//                        }

                        list.add(order);
                    }
                }

            }

        } catch (Exception e) {
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_HIDE);
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.SERVER_FAILED);
            return null;
        } finally {
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_HIDE);
        }

        return list;

    }


    @Override
    protected void onPostExecute(List<com.gulfcouae.ordermanagement.beans.Order> orders) {
        callback.showList(orders);
    }
}
