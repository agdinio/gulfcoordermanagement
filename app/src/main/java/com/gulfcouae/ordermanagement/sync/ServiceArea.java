package com.gulfcouae.ordermanagement.sync;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Message;
import android.widget.ProgressBar;

import com.gulfcouae.ordermanagement.beans.Area;
import com.gulfcouae.ordermanagement.helpers.ThreadHandler;
import com.gulfcouae.ordermanagement.interfaces.AreaImpl;
import com.gulfcouae.ordermanagement.statics.Global;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by relly on 8/4/15.
 */
public class ServiceArea extends AsyncTask<Integer, Void, List<Area>> {

    private Context context;
    private AreaImpl callback;
    private ProgressBar progressBar;

    public ServiceArea(Context context, ProgressBar progressBar, AreaImpl callback) {
        this.context = context;
        this.callback = callback;
        this.progressBar = progressBar;

        new ThreadHandler(context).sendMessage(Message.obtain(null, ThreadHandler.SHOW_PROGRESSBAR, progressBar));
    }

    @Override
    protected List<Area> doInBackground(Integer... params) {
        List<Area> list = null;

        SoapObject request = new SoapObject(Global.NAMESPACE, "GetAreasByEmirateId");
        request.addProperty("emirateId", params[0]);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(Global.SERVICE_URL);
        try {
            androidHttpTransport.call(String.format("%s%s", Global.SOAP_ACTION, "IOrderManagement/GetAreasByEmirateId"), envelope);

            SoapObject result = (SoapObject) envelope.getResponse();
            if (result != null && result.getPropertyCount() > 0) {
                list = new ArrayList<>();
                for (int i = 0; i<result.getPropertyCount(); i++) {
                    SoapObject item = (SoapObject) result.getProperty(i);

                    Area area = new Area();
                    area.setId(Integer.parseInt(item.getProperty("Id").toString()));
                    area.setEmiratesId(item.getProperty("EmiratesId") == null ? 0 : Integer.parseInt(item.getProperty("EmiratesId").toString()));
                    area.setName(item.getProperty("Name") == null ? "" : item.getProperty("Name").toString());

                    list.add(area);
                }

            }

        } catch (Exception e) {
            new ThreadHandler(context).sendMessage(Message.obtain(null, ThreadHandler.HIDE_PROGRESSBAR, progressBar));
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.SERVER_FAILED);
            return null;
        } finally {
            new ThreadHandler(context).sendMessage(Message.obtain(null, ThreadHandler.HIDE_PROGRESSBAR, progressBar));
        }

        return list;

    }

    @Override
    protected void onPostExecute(List<Area> areas) {
        callback.refreshList(areas);

    }
}
