package com.gulfcouae.ordermanagement.sync;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.gulfcouae.ordermanagement.MainActivity;
import com.gulfcouae.ordermanagement.beans.Customer;
import com.gulfcouae.ordermanagement.helpers.ThreadHandler;
import com.gulfcouae.ordermanagement.persistents.SharedPreferences;
import com.gulfcouae.ordermanagement.statics.Global;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;


/**
 * Created by relly on 7/26/15.
 */
public class ServiceRegistration extends AsyncTask<Customer, Void, Void> {

    private SharedPreferences prefs;
    private Context context;

    public ServiceRegistration(Context context) {
        this.context = context;

        new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_SHOW);
    }


    @Override
    protected Void doInBackground(Customer... params) {

        Customer cust = params[0];

        JSONObject json = new JSONObject();
        try {
            json.put("firstname", cust.getFirstName());
            json.put("lastname", cust.getLastName());
            json.put("mobile", cust.getMobile());
            json.put("email", cust.getEmail());
            json.put("username", cust.getUsername());
            json.put("password", cust.getPassword());
            json.put("emiratesid", cust.getEmiratesId());
            json.put("areaid", cust.getAreaId());
            json.put("address", cust.getAddress());
            json.put("type", cust.getType());
            json.put("minamount", cust.getMinAmount());
        } catch (JSONException e) {
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_HIDE);
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.REGISTRATION_FAILED);
            return null;
        }

        SoapObject request = new SoapObject(Global.NAMESPACE, com.gulfcouae.ordermanagement.statics.Customer.METHOD_NAME_REGISTER);
        request.addProperty("jsonString", json.toString());
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(Global.SERVICE_URL);
        try {
            androidHttpTransport.call(String.format("%s%s", Global.SOAP_ACTION, com.gulfcouae.ordermanagement.statics.Customer.SOAP_ACTION_REGISTER), envelope);

            SoapPrimitive result = (SoapPrimitive) envelope.getResponse();
            if (result != null) {
                cust.setId(Integer.parseInt(result.toString()));
            }

            prefs = new SharedPreferences(context);
            String jsonString = prefs.toJson(cust);
            prefs.editMode();
            prefs.putString("customerinfo", jsonString);
            prefs.putBoolean("rememberme", false);
            prefs.commitMode();


            Intent intent = new Intent(context, MainActivity.class);
            context.startActivity(intent);

        } catch (Exception e) {
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_HIDE);
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.SERVER_FAILED);
            return null;
        } finally {
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_HIDE);
        }


        return null;

    }
}
