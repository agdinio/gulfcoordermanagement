package com.gulfcouae.ordermanagement.sync;

import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.gulfcouae.ordermanagement.R;

import java.io.InputStream;
import java.net.MalformedURLException;

/**
 * Created by relly on 8/1/15.
 */
public class DownloadImage extends AsyncTask<String, Void, Bitmap> {

    ImageView bmImage;
    Context context;

    public DownloadImage(ImageView bmImage, Context context) {
        this.bmImage = bmImage;
        this.context = context;
    }

    @Override
    protected Bitmap doInBackground(String... params) {

//        String url = params[0];
//        Bitmap bm = null;
//        try {
//            InputStream in = new java.net.URL(url).openStream();
//            bm = BitmapFactory.decodeStream(in);
//        } catch (MalformedURLException me) {
//            Log.e("LazyAdapter", "Bad URL Error");
//        }
//        catch (Exception e) {
//            Log.e("Bad URL Error", e.getMessage());
//            e.printStackTrace();
//        }
//        return bm;

        String url = params[0];
        Bitmap bm = null;
        try {

            AssetManager am = context.getAssets();
            bm = BitmapFactory.decodeStream(am.open("images/"+url));

        } catch (Exception e) {
            e.printStackTrace();
        }
        return bm;
    }

    @Override
    protected void onPostExecute(Bitmap result) {
        if (result == null) {
            bmImage.setImageResource(R.drawable.ic_noimage);
        } else {
            bmImage.setImageBitmap(result);
        }
    }
}