package com.gulfcouae.ordermanagement.sync;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.gulfcouae.ordermanagement.MainActivity;
import com.gulfcouae.ordermanagement.beans.Area;
import com.gulfcouae.ordermanagement.beans.Emirate;
import com.gulfcouae.ordermanagement.helpers.ThreadHandler;
import com.gulfcouae.ordermanagement.persistents.SharedPreferences;
import com.gulfcouae.ordermanagement.statics.Customer;
import com.gulfcouae.ordermanagement.statics.Global;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;


/**
 * Created by relly on 7/26/15.
 */
public class ServiceUserAuth extends Thread {

    private SharedPreferences prefs;
    private Context context;
    private ProgressDialog dialog;
    private String username;
    private String password;
    private boolean isChecked;
    private Object[] params;

    public ServiceUserAuth(Context context, Object[] params) {
        this.context = context;
        this.params = params;

        prefs = new SharedPreferences(context);
    }

    @Override
    public void run() {

        try {

            if (params != null && params.length > 0) {
                if (params[0] instanceof ProgressDialog) {
                    dialog = (ProgressDialog) params[0];
                }
                if (params[1] instanceof String) {
                    username = (String) params[1];
                }
                if (params[2] instanceof String) {
                    password = (String) params[2];
                }
                if (params[3] instanceof Boolean) {
                    isChecked = (boolean) params[3];
                }
            }

//            SoapObject request = new SoapObject(Global.NAMESPACE, Customer.METHOD_NAME_AUTHENTICATE);
//            request.addProperty("username", username);
//            request.addProperty("password", password);
//
//            SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//            envelope.dotNet = true;
//            envelope.setOutputSoapObject(request);
//
//            HttpTransportSE androidHttpTransport = new HttpTransportSE(Global.SERVICE_URL);
//            try {
//                androidHttpTransport.call(String.format("%s%s", Global.SOAP_ACTION, Customer.SOAP_ACTION_AUTHENTICATE), envelope);
//
//                SoapObject result = (SoapObject) envelope.getResponse();
//                if (result != null) {
//
//                    Emirate emirate = new Emirate();
//                    emirate.setId(result.getProperty("EmiratesId") == null ? 0 : Integer.parseInt(result.getProperty("EmiratesId").toString()));
//                    emirate.setCode(result.getProperty("EmiratesCode") == null ? "" : result.getProperty("EmiratesCode").toString());
//                    emirate.setName(result.getProperty("EmiratesName") == null ? "" : result.getProperty("EmiratesName").toString());
//
//                    Area area = new Area();
//                    area.setId(result.getProperty("AreaId") == null ? 0 : Integer.parseInt(result.getProperty("AreaId").toString()));
//                    area.setEmiratesId(result.getProperty("EmiratesId") == null ? 0 : Integer.parseInt(result.getProperty("EmiratesId").toString()));
//                    area.setName(result.getProperty("AreaName") == null ? "" : result.getProperty("AreaName").toString());
//
//                    com.gulfcouae.ordermanagement.beans.Customer customer = new com.gulfcouae.ordermanagement.beans.Customer();
//                    customer.setId(Integer.parseInt(result.getProperty("Id").toString()));
//                    customer.setFirstName(result.getProperty("FirstName") == null ? "" : result.getProperty("FirstName").toString());
//                    customer.setLastName(result.getProperty("LastName") == null ? "" : result.getProperty("LastName").toString());
//                    customer.setMobile(result.getProperty("Mobile") == null ? "" : result.getProperty("Mobile").toString());
//                    customer.setEmail(result.getProperty("Email") == null ? "" : result.getProperty("Email").toString());
//                    customer.setUsername(result.getProperty("Username") == null ? "" : result.getProperty("Username").toString());
//                    customer.setPassword(result.getProperty("Password") == null ? "" : result.getProperty("Password").toString());
//                    customer.setEmiratesId(result.getProperty("EmiratesId") == null ? 0 : Integer.parseInt(result.getProperty("EmiratesId").toString()));
//                    customer.setAreaId(result.getProperty("AreaId") == null ? 0 : Integer.parseInt(result.getProperty("AreaId").toString()));
//                    customer.setAddress(result.getProperty("Address") == null ? "" : result.getProperty("Address").toString());
//                    customer.setType(result.getProperty("Type") == null ? "" : result.getProperty("Type").toString());
//                    customer.setEmiratesCode(result.getProperty("EmiratesCode") == null ? "" : result.getProperty("EmiratesCode").toString());
//                    customer.setEmiratesName(result.getProperty("EmiratesName") == null ? "" : result.getProperty("EmiratesName").toString());
//                    customer.setAreaName(result.getProperty("AreaName") == null ? "" : result.getProperty("AreaName").toString());
//                    customer.setMinAmount(result.getProperty("MinAmount") == null ? 0 : Double.parseDouble(result.getProperty("MinAmount").toString()));
//                    customer.setEmirate(emirate);
//                    customer.setArea(area);
//
//                    String jsonString = prefs.toJson(customer);
//                    prefs.editMode();
//                    prefs.putString(Customer.CUSTOMER_INFO, jsonString);
//                    prefs.putBoolean(Customer.REMEMBER_ME, isChecked);
//                    prefs.commitMode();
//
//                    Intent intent = new Intent(context, MainActivity.class);
//                    context.startActivity(intent);
//
//                } else {
//                    new ThreadHandler(context).sendEmptyMessage(ThreadHandler.INVALID_ACCOUNT);
//                }
//
//            } catch (Exception ex) {
//                Log.e("LOGCAT", (ex.getMessage() == null) ? ex.getMessage() : ex.toString());
//                new ThreadHandler(context).sendEmptyMessage(ThreadHandler.SERVER_FAILED);
//            }

//-------------------------
            if (username.equalsIgnoreCase("admin") && password.equals("admin")) {

                Emirate emirate = new Emirate();
                emirate.setId(3);
                emirate.setCode("DXB");
                emirate.setName("Dubai");

                Area area = new Area();
                area.setId(1);
                area.setEmiratesId(3);
                area.setName("Academic City");

                com.gulfcouae.ordermanagement.beans.Customer customer = new com.gulfcouae.ordermanagement.beans.Customer();
                customer.setId(5);
                customer.setFirstName("Admin");
                customer.setLastName("Demo");
                customer.setMobile("0568546535");
                customer.setEmail("aurelio.dinio@al-majid.com");
                customer.setUsername("admin");
                customer.setPassword("admin");
                customer.setEmiratesId(3);
                customer.setAreaId(1);
                customer.setAddress("Al Quoz");
                customer.setType("perrier");
                customer.setEmiratesCode("DXB");
                customer.setEmiratesName("Dubai");
                customer.setAreaName("Academic City");
                customer.setMinAmount(200.0);
                customer.setEmirate(emirate);
                customer.setArea(area);

                String jsonString = prefs.toJson(customer);
                prefs.editMode();
                prefs.putString(Customer.CUSTOMER_INFO, jsonString);
                prefs.putBoolean(Customer.REMEMBER_ME, isChecked);
                prefs.commitMode();

                Intent intent = new Intent(context, MainActivity.class);
                context.startActivity(intent);

            } else {
                new ThreadHandler(context).sendEmptyMessage(ThreadHandler.INVALID_ACCOUNT);
            }


        } catch(Exception ex) {
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.LOGIN_FAILED);
        } finally {
            dialog.dismiss();
        }


    }


}
