package com.gulfcouae.ordermanagement.sync;

import android.content.Context;
import android.os.AsyncTask;

import com.gulfcouae.ordermanagement.interfaces.CustomerImpl;
import com.gulfcouae.ordermanagement.statics.Customer;
import com.gulfcouae.ordermanagement.statics.Global;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by relly on 8/24/15.
 */
public class ServiceSendPassword extends AsyncTask<String, Void, Boolean> {

    private Context context;
    private CustomerImpl callback;

    public ServiceSendPassword(Context context, CustomerImpl callback) {
        this.context = context;
        this.callback = callback;
    }

    @Override
    protected Boolean doInBackground(String... params) {
        String email = params[0];
        Boolean sent = false;

        SoapObject request = new SoapObject(Global.NAMESPACE, Customer.METHOD_NAME_EMAIL_PASSWORD);
        request.addProperty("email", email);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(Global.SERVICE_URL);
        try {
            androidHttpTransport.call(String.format("%s%s", Global.SOAP_ACTION, Customer.SOAP_ACTION_EMAIL_PASSWORD), envelope);

            SoapPrimitive result = (SoapPrimitive) envelope.getResponse();
            if (result != null) {
                sent = Boolean.parseBoolean(result.toString());
            }
        } catch(Exception e) {
            sent = false;
        }

        return sent;
    }

    @Override
    protected void onPostExecute(Boolean aBoolean) {
        callback.sendPasswordCallback(aBoolean);
    }
}
