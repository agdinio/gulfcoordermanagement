package com.gulfcouae.ordermanagement.sync;

import android.content.Context;
import android.os.AsyncTask;

import com.gulfcouae.ordermanagement.beans.OrderDetailsHistory;
import com.gulfcouae.ordermanagement.helpers.ThreadHandler;
import com.gulfcouae.ordermanagement.interfaces.OrderHistoryImpl;
import com.gulfcouae.ordermanagement.statics.Global;
import com.gulfcouae.ordermanagement.statics.Order;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by relly on 7/29/15.
 */
public class ServiceOrderDetailsHistory extends AsyncTask<Integer, Void, List<OrderDetailsHistory>> {

    private Context context;
    private OrderHistoryImpl callback;

    public ServiceOrderDetailsHistory(Context context, OrderHistoryImpl callback) {
        this.context = context;
        this.callback = callback;

        new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_SHOW);
    }

    @Override
    protected List<OrderDetailsHistory> doInBackground(Integer... params) {
        List<OrderDetailsHistory> list = null;
        Integer param = params[0];

        SoapObject request = new SoapObject(Global.NAMESPACE, Order.METHOD_NAME_GETORDERDETAILSHISTORY);
        request.addProperty("orderId", param);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(Global.SERVICE_URL);
        try {
            androidHttpTransport.call(String.format("%s%s", Global.SOAP_ACTION, Order.SOAP_ACTION_GETORDERDETAILSHISTORY), envelope);

            SoapObject result = (SoapObject) envelope.getResponse();
            if (result != null && result.getPropertyCount() > 0) {
                list = new ArrayList<>();

                for (int i = 0; i<result.getPropertyCount(); i++) {
                    SoapObject item = (SoapObject) result.getProperty(i);
                    if (item != null) {

                        SimpleDateFormat dateformatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");

                        OrderDetailsHistory od = new OrderDetailsHistory();
                        od.setOrderDetailsId(Integer.parseInt(item.getProperty("OrderDetailsId").toString()));
                        od.setOrderId(Integer.parseInt(item.getProperty("OrderId").toString()));
                        od.setOrderQuantity(item.getProperty("OrderQuantity") == null ? 0 : Integer.parseInt(item.getProperty("OrderQuantity").toString()));

                        od.setProductId(item.getProperty("ProductId") == null ? 0 : Integer.parseInt(item.getProperty("ProductId").toString()));
                        od.setProductName(item.getProperty("ProductName") == null ? "" : item.getProperty("ProductName").toString());
                        od.setProductDescription(item.getProperty("ProductDescription") == null ? "" : item.getProperty("ProductDescription").toString());
                        od.setProductQuantity(item.getProperty("ProductQuantity") == null ? 0 : Double.parseDouble(item.getProperty("ProductQuantity").toString()));
                        od.setProductPrice(item.getProperty("ProductPrice") == null ? 0 : Double.parseDouble(item.getProperty("ProductPrice").toString()));
                        od.setProductImage(item.getProperty("ProductImage") == null ? "" : item.getProperty("ProductImage").toString());
                        od.setProductCode(item.getProperty("ProductCode") == null ? "" : item.getProperty("ProductCode").toString());

                        od.setCategoryId(item.getProperty("CategoryId") == null ? 0 : Integer.parseInt(item.getProperty("CategoryId").toString()));
                        od.setCategoryName(item.getProperty("CategoryName") == null ? "" : item.getProperty("CategoryName").toString());
                        od.setCategoryDescription(item.getProperty("CategoryDescription") == null ? "" : item.getProperty("CategoryDescription").toString());

                        od.setUomId(item.getProperty("UOMId") == null ? 0 : Integer.parseInt(item.getProperty("UOMId").toString()));
                        od.setUomCode(item.getProperty("UOMCode") == null ? "" : item.getProperty("UOMCode").toString());
                        od.setUomName(item.getProperty("UOMName") == null ? "" : item.getProperty("UOMName").toString());

                        od.setBrandId(item.getProperty("BrandId") == null ? 0 : Integer.parseInt(item.getProperty("BrandId").toString()));
                        od.setBrandName(item.getProperty("BrandName") == null ? "" : item.getProperty("BrandName").toString());

                        od.setPromoId(item.getProperty("PromoId") == null ? 0 : Integer.parseInt(item.getProperty("PromoId").toString()));
                        od.setPromoName(item.getProperty("PromoName") == null ? "" : item.getProperty("PromoName").toString());
                        od.setPromoDetails(item.getProperty("PromoDetails") == null ? "" : item.getProperty("PromoDetails").toString());
                        od.setPromoDiscount(item.getProperty("PromoDiscount") == null ? 0 : Double.parseDouble(item.getProperty("PromoDiscount").toString()));

                        list.add(od);
                    }
                }

            }

        } catch (Exception e) {
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_HIDE);
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.SERVER_FAILED);
            return null;
        } finally {
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_HIDE);
        }

        return list;

    }


    @Override
    protected void onPostExecute(List<OrderDetailsHistory> orderDetailsHistories) {
        callback.showOrderDetailsList(orderDetailsHistories);
    }
}
