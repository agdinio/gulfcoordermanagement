package com.gulfcouae.ordermanagement.sync;

import android.content.Context;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;

import com.gulfcouae.ordermanagement.FragmentCheckout;
import com.gulfcouae.ordermanagement.R;
import com.gulfcouae.ordermanagement.beans.Area;
import com.gulfcouae.ordermanagement.beans.Customer;
import com.gulfcouae.ordermanagement.beans.Emirate;
import com.gulfcouae.ordermanagement.helpers.ThreadHandler;
import com.gulfcouae.ordermanagement.persistents.SharedPreferences;
import com.gulfcouae.ordermanagement.statics.Global;

import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by relly on 7/26/15.
 */
public class ShippingInfo extends AsyncTask<Customer, Void, Void> {

    private SharedPreferences prefs;
    private Context context;

    public ShippingInfo(Context context) {
        this.context = context;

        new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_SHOW);
    }


    @Override
    protected Void doInBackground(Customer... params) {

        Customer cust = params[0];

        JSONObject json = new JSONObject();
        try {
            json.put("id", cust.getId());
            json.put("mobile", cust.getMobile());
            json.put("email", cust.getEmail());
            json.put("emiratesid", cust.getEmiratesId());
            json.put("areaid", cust.getAreaId());
            json.put("address", cust.getAddress());
        } catch (JSONException e) {
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_HIDE);
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.UPDATE_SHIP_INFO_FAILED);
            return null;
        }

        SoapObject request = new SoapObject(Global.NAMESPACE, com.gulfcouae.ordermanagement.statics.Customer.METHOD_NAME_UPDATE_SHIPPING_INFO);
        request.addProperty("jsonString", json.toString());
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(Global.SERVICE_URL);
        try {
            androidHttpTransport.call(String.format("%s%s", Global.SOAP_ACTION, com.gulfcouae.ordermanagement.statics.Customer.SOAP_ACTION_UPDATE_SHIPPING_INFO), envelope);

            SoapPrimitive result = (SoapPrimitive) envelope.getResponse();
            if (result != null) {
                if (Integer.parseInt(result.toString()) > 0) {

                    SharedPreferences prefs = new SharedPreferences(context);

                    Emirate updateEmirate = new Emirate();
                    updateEmirate.setId(cust.getEmiratesId());
                    updateEmirate.setCode(cust.getEmiratesCode());
                    updateEmirate.setName(cust.getEmiratesName());

                    Area updateArea = new Area();
                    updateArea.setId(cust.getAreaId());
                    updateArea.setEmiratesId(cust.getEmiratesId());
                    updateArea.setName(cust.getAreaName());

                    Customer updateCust = prefs.getCustomerInfo();
                    updateCust.setAddress(cust.getAddress());
                    updateCust.setMobile(cust.getMobile());
                    updateCust.setEmail(cust.getEmail());
                    updateCust.setEmiratesId(cust.getEmiratesId());
                    updateCust.setEmiratesCode(cust.getEmiratesCode());
                    updateCust.setEmiratesName(cust.getEmiratesName());
                    updateCust.setAreaId(cust.getAreaId());
                    updateCust.setAreaName(cust.getAreaName());
                    updateCust.setEmirate(updateEmirate);
                    updateCust.setArea(updateArea);

                    prefs.editMode();
                    prefs.putString(com.gulfcouae.ordermanagement.statics.Customer.CUSTOMER_INFO, prefs.toJson(updateCust));
                    prefs.commitMode();

                    //success: go back to confirmation
                    FragmentManager fragmentManager = ((FragmentActivity)context).getSupportFragmentManager();
                    FragmentTransaction trans = fragmentManager.beginTransaction();

                    Fragment frag = new FragmentCheckout();
                    trans.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_right);
                    trans.replace(R.id.content_frame, frag);
                    trans.commit();

                } else {
                    new ThreadHandler(context).sendEmptyMessage(ThreadHandler.UPDATE_SHIP_INFO_FAILED);
                }
            } else {
                new ThreadHandler(context).sendEmptyMessage(ThreadHandler.UPDATE_SHIP_INFO_FAILED);
            }

        } catch (Exception e) {
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_HIDE);
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.SERVER_FAILED);
            return null;
        } finally {
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_HIDE);
        }


        return null;

    }
}
