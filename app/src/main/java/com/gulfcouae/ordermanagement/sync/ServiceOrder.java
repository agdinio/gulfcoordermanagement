package com.gulfcouae.ordermanagement.sync;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;

import com.gulfcouae.ordermanagement.MainActivity;
import com.gulfcouae.ordermanagement.beans.Cart;
import com.gulfcouae.ordermanagement.beans.Customer;
import com.gulfcouae.ordermanagement.helpers.ThreadHandler;
import com.gulfcouae.ordermanagement.interfaces.CheckoutImpl;
import com.gulfcouae.ordermanagement.persistents.CartDataSource;
import com.gulfcouae.ordermanagement.persistents.SharedPreferences;
import com.gulfcouae.ordermanagement.statics.Global;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * Created by relly on 7/26/15.
 */
public class ServiceOrder extends AsyncTask<Void, Void, String> {

    private Context context;
    private CheckoutImpl callback;


    public ServiceOrder(Context context, CheckoutImpl callback) {
        this.context = context;
        this.callback = callback;

        new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_SHOW);
    }


    @Override
    protected String doInBackground(Void... params) {

        SharedPreferences prefs = new SharedPreferences(context);
        JSONObject jMain = new JSONObject();
        Customer cust = prefs.getCustomerInfo();

        if (cust != null) {
            CartDataSource dao = new CartDataSource(context);
            dao.open(false);
            List<Cart> carts = dao.getCartByCustomer(cust.getId());
            dao.close();

            if (carts != null && carts.size() > 0) {

                JSONObject jOrder = new JSONObject();
                JSONArray jArray = new JSONArray();

                try {
                    SimpleDateFormat dateformatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    jOrder.put("customerid", cust.getId());
                    jOrder.put("orderdate", dateformatter.format(new Date()));
                    jOrder.put("shippingaddress", cust.getAddress());
                    jOrder.put("shippingdate", dateformatter.format(new Date()));
                    jOrder.put("shippingstatus", 0);

                    for (Cart cart : carts) {
                        JSONObject jOrderdetails = new JSONObject();
                        jOrderdetails.put("productid", cart.getProductId());
                        jOrderdetails.put("quantity", cart.getQuantity());
                        jOrderdetails.put("confirmed", false);
                        jOrderdetails.put("uomid", cart.getUomId());
                        jArray.put(jOrderdetails);
                        jMain.put("orderdetails", jArray);
                    }

                    jMain.put("order", jOrder);

                } catch (JSONException je) {
                    new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_HIDE);
                    new ThreadHandler(context).sendEmptyMessage(ThreadHandler.REGISTRATION_FAILED);
                    return null;
                }
            }
        }


        SoapObject request = new SoapObject(Global.NAMESPACE, com.gulfcouae.ordermanagement.statics.Order.METHOD_NAME_PLACEORDER);
        request.addProperty("jsonString", jMain.toString());

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(Global.SERVICE_URL);
        try {
            androidHttpTransport.call(String.format("%s%s", Global.SOAP_ACTION, com.gulfcouae.ordermanagement.statics.Order.SOAP_ACTION_PLACEORDER), envelope);

            SoapPrimitive result = (SoapPrimitive) envelope.getResponse();
            if (result != null) {

                return result.toString();
            }


            Intent intent = new Intent(context, MainActivity.class);
            context.startActivity(intent);

        } catch (Exception e) {
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_HIDE);
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.SERVER_FAILED);
            return null;
        } finally {
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_HIDE);
        }

        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        callback.redirectPage(result);
    }
}
