package com.gulfcouae.ordermanagement.sync;

import android.content.Context;
import android.os.AsyncTask;

import com.gulfcouae.ordermanagement.beans.Customer;
import com.gulfcouae.ordermanagement.helpers.ThreadHandler;
import com.gulfcouae.ordermanagement.interfaces.CustomerImpl;
import com.gulfcouae.ordermanagement.persistents.SharedPreferences;
import com.gulfcouae.ordermanagement.statics.Global;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by relly on 7/26/15.
 */
public class ServiceUpateCustomerPassword extends AsyncTask<Customer, Void, Void> {

    private SharedPreferences prefs;
    private Context context;
    private CustomerImpl callback;

    public ServiceUpateCustomerPassword(Context context, CustomerImpl callback) {
        this.context = context;
        this.callback = callback;

    }


    @Override
    protected Void doInBackground(Customer... params) {

        Customer cust = params[0];

        SoapObject request = new SoapObject(Global.NAMESPACE, com.gulfcouae.ordermanagement.statics.Customer.METHOD_NAME_UPDATE_PASSWORD);
        request.addProperty("customerId", cust.getId());
        request.addProperty("newPassword", cust.getPassword());
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(Global.SERVICE_URL);
        try {
            androidHttpTransport.call(String.format("%s%s", Global.SOAP_ACTION, com.gulfcouae.ordermanagement.statics.Customer.SOAP_ACTION_UPDATE_PASSWORD), envelope);

            SoapPrimitive result = (SoapPrimitive) envelope.getResponse();
            if (result != null) {
                if (Integer.parseInt(result.toString()) > 0) {
                    prefs = new SharedPreferences(context);
                    Customer currentCust = prefs.getCustomerInfo();
                    currentCust.setPassword(cust.getPassword());

                    String jsonString = prefs.toJson(currentCust);
                    prefs.editMode();
                    prefs.putString("customerinfo", jsonString);
                    prefs.commitMode();

                } else {
                    new ThreadHandler(context).sendEmptyMessage(ThreadHandler.UPDATE_CUSTOMER_FAILED);
                }
            }


        } catch (Exception e) {
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.SERVER_FAILED);
            return null;
        } finally {
        }


        return null;

    }

    @Override
    protected void onPostExecute(Void aVoid) {
        callback.closePassDialog();
    }
}
