package com.gulfcouae.ordermanagement.sync;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Message;
import android.widget.ProgressBar;

import com.gulfcouae.ordermanagement.helpers.ThreadHandler;
import com.gulfcouae.ordermanagement.interfaces.UsernameImpl;
import com.gulfcouae.ordermanagement.statics.Customer;
import com.gulfcouae.ordermanagement.statics.Global;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

/**
 * Created by relly on 8/23/15.
 */
public class ServiceUsername extends AsyncTask<String, Void, String> {

    private Context context;
    private ProgressBar progressBar;
    private UsernameImpl callback;

    public ServiceUsername(Context context, ProgressBar pb, UsernameImpl callback) {
        this.context = context;
        this.progressBar = pb;
        this.callback = callback;

        new ThreadHandler(context).sendMessage(Message.obtain(null, ThreadHandler.SHOW_PROGRESSBAR, progressBar));

    }

    @Override
    protected String doInBackground(String... params) {
        String result = null;

        SoapObject request = new SoapObject(Global.NAMESPACE, Customer.METHOD_NAME_GET_USERNAME);
        request.addProperty("userName", params[0]);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(request);

        HttpTransportSE androidHttpTransport = new HttpTransportSE(Global.SERVICE_URL);
        try {
            androidHttpTransport.call(String.format("%s%s", Global.SOAP_ACTION, Customer.SOAP_ACTION_GET_USERNAME), envelope);

            SoapPrimitive res = (SoapPrimitive) envelope.getResponse();
            if (res != null) {
                result = res.toString();
            }
            new ThreadHandler(context).sendMessage(Message.obtain(null, ThreadHandler.HIDE_PROGRESSBAR, progressBar));

        } catch(Exception e) {
            new ThreadHandler(context).sendMessage(Message.obtain(null, ThreadHandler.HIDE_PROGRESSBAR, progressBar));
            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.SERVER_FAILED);
            result = null;
        }

        return result;
    }

    @Override
    protected void onPostExecute(String result) {
        callback.getResult(result);
    }
}
