package com.gulfcouae.ordermanagement.sync;

import android.content.Context;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.widget.ListView;

import com.gulfcouae.ordermanagement.beans.Product;
import com.gulfcouae.ordermanagement.helpers.ThreadHandler;
import com.gulfcouae.ordermanagement.interfaces.ProductsImpl;
import com.gulfcouae.ordermanagement.statics.Global;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by relly on 7/29/15.
 */
public class ServiceProducts extends AsyncTask<String, Void, List<Product>> {

    private Context context;
    private ListView listView;
    private ProductsImpl callback;

    public ServiceProducts(Context context, ListView listView, ProductsImpl callback) {
        this.context = context;
        this.listView = listView;
        this.callback = callback;

        new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_SHOW);
    }

    @Override
    protected List<Product> doInBackground(String... params) {
        List<Product> list = new ArrayList<>();
//        String param = params[0];
//        SoapObject request = null;
//
//        if (param != null && !TextUtils.isEmpty(param)) {
//            request = new SoapObject(Global.NAMESPACE, com.gulfcouae.ordermanagement.statics.Product.METHOD_NAME_GETPRODUCTSBYKEYWORD);
//            request.addProperty("searchString", param);
//        } else {
//            request = new SoapObject(Global.NAMESPACE, com.gulfcouae.ordermanagement.statics.Product.METHOD_NAME_GETPRODUCTS);
//        }
//
//
//        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//        envelope.dotNet = true;
//        envelope.setOutputSoapObject(request);
//
//        HttpTransportSE androidHttpTransport = new HttpTransportSE(Global.SERVICE_URL);
//        try {
//            if (param != null && !TextUtils.isEmpty(param)) {
//                androidHttpTransport.call(String.format("%s%s", Global.SOAP_ACTION, com.gulfcouae.ordermanagement.statics.Product.SOAP_ACTION_GETPRODUCTSBYKEYWORD), envelope);
//            } else {
//                androidHttpTransport.call(String.format("%s%s", Global.SOAP_ACTION, com.gulfcouae.ordermanagement.statics.Product.SOAP_ACTION_GETPRODUCTS), envelope);
//            }
//
//            SoapObject result = (SoapObject) envelope.getResponse();
//            if (result != null && result.getPropertyCount() > 0) {
//                for (int i = 0; i<result.getPropertyCount(); i++) {
//                    SoapObject item = (SoapObject) result.getProperty(i);
//
//                    Product product = new Product();
//
//                    product.setId(Integer.parseInt(item.getProperty("Id").toString()));
//                    product.setName(item.getProperty("Name") == null ? "" : item.getProperty("Name").toString());
//                    product.setDescription(item.getProperty("Description") == null ? "" : item.getProperty("Description").toString());
//                    product.setCategoryId(item.getProperty("CategoryId") == null ? 0 : Integer.parseInt(item.getProperty("CategoryId").toString()));
//                    product.setCategoryName(item.getProperty("CategoryName") == null ? "" : item.getProperty("CategoryName").toString());
//                    product.setBrandId(item.getProperty("BrandId") == null ? 0 : Integer.parseInt(item.getProperty("BrandId").toString()));
//                    product.setBrandName(item.getProperty("BrandName") == null ? "" : item.getProperty("BrandName").toString());
//                    product.setImage(item.getProperty("Image") == null ? "" : item.getProperty("Image").toString());
//                    product.setCode(item.getProperty("Code") == null ? "" : item.getProperty("Code").toString());
//                    product.setIsPromo(item.getProperty("IsPromo") == null ? false : Boolean.parseBoolean(item.getProperty("IsPromo").toString()));
//
//                    list.add(product);
//                }
//
//            }
//
//        } catch (Exception e) {
//            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_HIDE);
//            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.SERVER_FAILED);
//            return null;
//        } finally {
//            new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_HIDE);
//        }

        int id[] = {9,8,2,1,11,10,6,5};
        String name[] = {"CATERIPLLAR ISA 8173","CATERPILLAR 3HD 2117","Falcon Lighter","Falcon-Nemesis","Fossil Mens' Blue Leather","Fossil Men's Brown Leather","Perrier 3000","Purie"};
        String desc[] = {"Cat Watches Powertech Reserve / Alarm","Cat Watches Texas","Falcon Lighters","Falcon Spring Drinking water.","Fossil Grant for Men Chronograph","Fossil Men's White Dial Leather Brand","Perrier 3000","Pure Water Technology "};
        int catId[] = {0,0,0,0,0,0,0,0,0,0};
        String catName[] = {"","","","","","","",""};
        int brandId[] = {0,0,1,1,0,0,1,1};
        String brandName[] = {"","","Perrier","Perrier","","","Perrier","Perrier"};
        String img[] = {"SetWidth290-SB.145.11.127.jpg","SetWidth290-SD.161.34.132.jpg","","Falcon.png","FS4835.jpg","FS4735.jpg","","purie.jpg"};
        String code[] = {"CAT","CAT","P0002","P0001","FOSSIL","FOSSIL","P0003","P0004"};
        boolean ispromo[] = {false,false,true,true,false,true,false,false};

        for (int i=0; i<id.length; i++) {
            Product product = new Product();

            product.setId(id[i]);
            product.setName(name[i]);
            product.setDescription(desc[i]);
            product.setCategoryId(catId[i]);
            product.setCategoryName(catName[i]);
            product.setBrandId(brandId[i]);
            product.setBrandName(brandName[i]);
            product.setImage(img[i]);
            product.setCode(code[i]);
            product.setIsPromo(ispromo[i]);

            list.add(product);
        }

        new ThreadHandler(context).sendEmptyMessage(ThreadHandler.PROGRESSBAR_HIDE);

        return list;

/*
        List<Product> list = new ArrayList<>();

        Product p1 = new Product();
        p1.setId(1);
        p1.setName("Computer");
        p1.setPrice(150.00);
        p1.setDescription("This is a computer.");
        Product p2 = new Product();
        p2.setId(2);
        p2.setName("Car");
        p2.setPrice(648.00);
        p2.setDescription("This is a sports car.");
        Product p3 = new Product();
        p3.setId(3);
        p3.setName("Bag");
        p3.setPrice(648.00);
        p3.setDescription("This is a bag.");
        Product p4 = new Product();
        p4.setId(4);
        p4.setName("laptop");
        p4.setPrice(648.00);
        p4.setDescription("This is a laptop.");
        Product p5 = new Product();
        p5.setId(5);
        p5.setName("iPhone");
        p5.setPrice(648.00);
        p5.setDescription("This is an iPhone.");

        list.add(p1);
        list.add(p2);
        list.add(p3);
        list.add(p4);
        list.add(p5);

        return list;
*/
    }

    @Override
    protected void onPostExecute(List<Product> products) {
        callback.showList(products);
    }
}
