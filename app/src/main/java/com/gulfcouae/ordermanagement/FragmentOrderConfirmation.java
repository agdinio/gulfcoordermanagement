package com.gulfcouae.ordermanagement;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.gulfcouae.ordermanagement.R;
import com.gulfcouae.ordermanagement.beans.Customer;
import com.gulfcouae.ordermanagement.interfaces.BaseFragmentImpl;
import com.gulfcouae.ordermanagement.persistents.SharedPreferences;
import com.gulfcouae.ordermanagement.statics.Global;

/**
 * Created by relly on 8/9/15.
 */
public class FragmentOrderConfirmation extends Fragment implements BaseFragmentImpl {

    private View rootView;
    private TextView txtConfirmDesc;
    private Button btnContinueShopping;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ActionBar actionBar = ((ActionBarActivity) getActivity()).getSupportActionBar();
        View actionBarView = actionBar.getCustomView();
        ((TextView) actionBarView.findViewById(R.id.txtActionBarTitle)).setText(getString(R.string.actionbar_title_orderconfirmation));

        rootView = inflater.inflate(R.layout.fragment_order_confirmation, container, false);

        txtConfirmDesc = (TextView) rootView.findViewById(R.id.txtConfirmDesc);
        btnContinueShopping = (Button) rootView.findViewById(R.id.btnContinueShopping);
        btnContinueShopping.setOnClickListener(gotoItemsClickListener);


        String trans_no = getArguments().getString("trans_no");

        SharedPreferences prefs = new SharedPreferences(rootView.getContext());
        Customer cust = prefs.getCustomerInfo();
        String desc = "Hi " + cust.getFirstName() + ", you order has been successfully placed.\n\n" +
                        "Your Transaction Number is " + trans_no + ". " +
                        "You shall receive an email with your order summary shortly. " +
                        "Kindly expect a call back from our customer service representative.\n\n" +
                        "Thank you for ordering from us. Have a great day.";
        txtConfirmDesc.setText(desc);

        return rootView;

    }

    private View.OnClickListener gotoItemsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            FragmentManager fragmentManager = ((FragmentActivity)rootView.getContext()).getSupportFragmentManager();
            FragmentTransaction trans = fragmentManager.beginTransaction();

            Fragment frag = new FragmentProductList();
            trans.setCustomAnimations(R.anim.fade_in, R.anim.fade_out);
            trans.replace(R.id.content_frame, frag);
            trans.commit();
        }
    };

}