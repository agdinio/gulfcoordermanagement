package com.gulfcouae.ordermanagement.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by relly on 9/13/2014.
 */
public class FontUtils {

    private static Map<String, Typeface> TYPEFACE = new HashMap<String, Typeface>();
    private static String defaultFont = "helvetica_neue.otf";

    public static String getDefaultFont() {
        return defaultFont;
    }

    public static void loadLayoutInflaterForFont(final Activity activity) {
        activity.getLayoutInflater().setFactory(new LayoutInflater.Factory() {

            @Override
            public View onCreateView(String name, Context context, AttributeSet attrs) {
                View v = tryInflate(name, context, attrs);
                if (v instanceof TextView) {
                    ((TextView) v).setTypeface(getFonts(context, defaultFont));
                } else if (v instanceof Button) {
                    ((Button) v).setTypeface(getFonts(context, defaultFont), Typeface.BOLD);
                } else if (v instanceof EditText) {
                    ((EditText) v).setTypeface(getFonts(context, defaultFont));
                }


                return v;
            }
        });

    }

    private static View tryInflate(String name, Context context, AttributeSet attrs) {
        LayoutInflater li = LayoutInflater.from(context);
        View v = null;
        try {
            v = li.createView(name, null, attrs);
        } catch (Exception e) {
            try {
                v = li.createView("android.widget." + name, null, attrs);
            } catch (Exception e1) {
            }
        }
        return v;
    }

    public static Typeface getFonts(Context context, String name) {
        Typeface typeface = TYPEFACE.get(name);
        if (typeface == null) {
            typeface = Typeface.createFromAsset(context.getAssets(), "fonts/"
                    + name);
            TYPEFACE.put(name, typeface);
        }
        return typeface;
    }

    public static void setDefaultFont(Context context) {

        try {
            final Typeface bold = Typeface.createFromAsset(context.getAssets(), "fonts/helvetica_neue_bold.otf");
            final Typeface italic = Typeface.createFromAsset(context.getAssets(), "fonts/helvetica_neue.otf");
            final Typeface boldItalic = Typeface.createFromAsset(context.getAssets(), "fonts/helvetica_neue_bold.otf");
            final Typeface regular = Typeface.createFromAsset(context.getAssets(),"fonts/helvetica_neue.otf");

            Field DEFAULT = Typeface.class.getDeclaredField("DEFAULT");
            DEFAULT.setAccessible(true);
            DEFAULT.set(null, regular);

            Field DEFAULT_BOLD = Typeface.class.getDeclaredField("DEFAULT_BOLD");
            DEFAULT_BOLD.setAccessible(true);
            DEFAULT_BOLD.set(null, bold);

            Field sDefaults = Typeface.class.getDeclaredField("sDefaults");
            sDefaults.setAccessible(true);
            sDefaults.set(null, new Typeface[]{
                    regular, bold, italic, boldItalic
            });

        } catch (NoSuchFieldException e) {
            Log.e("NO SUCHFIELD EXCEPTION", e.getMessage());
        } catch (IllegalAccessException e) {
            Log.e("ILLEGAL ACCESS", e.getMessage());
        } catch (Throwable e) {
            //cannot crash app if there is a failure with overriding the default font!
            Log.e("THROWABLE EXCEPTION", e.getMessage());
        }
    }

}


