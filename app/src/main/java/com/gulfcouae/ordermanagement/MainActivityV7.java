package com.gulfcouae.ordermanagement;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.gulfcouae.ordermanagement.R;
import com.gulfcouae.ordermanagement.drawer.NavDrawerAdapter;
import com.gulfcouae.ordermanagement.drawer.NavDrawerItemImpl;
import com.gulfcouae.ordermanagement.drawer.NavMenuItem;
import com.gulfcouae.ordermanagement.persistents.SharedPreferences;


/**
 * Created by relly on 7/27/15.
 */

@SuppressLint("NewApi")
public class MainActivityV7 extends ActionBarActivity {

    private SharedPreferences prefs;
    private Context context = this;
    private ActionBar supportActionBar;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;

    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] mMenuTitles;
    private Toolbar toolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().setDefaultDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().hide();

        mMenuTitles = getResources().getStringArray(R.array.menu_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        toolbar = (Toolbar) findViewById(R.id.drawerToolbar);

        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.mipmap.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        NavDrawerItemImpl[] menu = new NavDrawerItemImpl[] {
                NavMenuItem.create(101, "Welcome Relly!", 0, "home", "Home"),
                NavMenuItem.create(102,"About", 0, "about", "About"),
                NavMenuItem.create(103, "My Profile", 0, "myprofile", "My Profile"),
                NavMenuItem.create(104, "My Orders", 0, "myorders", "My Orders"),
                NavMenuItem.create(105, "Log Out", 0, "logout", "Log Out")};
        mDrawerList.setAdapter(new NavDrawerAdapter(this, 0, menu));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerClosed(View drawerView) {
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            @Override
            public void onDrawerOpened(View drawerView) {
                getSupportActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };


//        mDrawerToggle = new ActionBarDrawerToggle(
//                this,                  /* host Activity */
//                mDrawerLayout,         /* DrawerLayout object */
//                R.mipmap.ic_drawer,  /* nav drawer image to replace 'Up' caret */
//                R.string.drawer_open,  /* "open drawer" description for accessibility */
//                R.string.drawer_close  /* "close drawer" description for accessibility */
//        ) {
//            public void onDrawerClosed(View view) {
//                getSupportActionBar().setTitle(mTitle);
//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
//            }
//
//            public void onDrawerOpened(View drawerView) {
//                getSupportActionBar().setTitle(mDrawerTitle);
//                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
//            }
//        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            selectFragment(null, 0);
        }

        //////////////new BaseActionBar(MainActivity.this, mDrawerLayout);

    }

    @Override
    protected void onStart() {
        super.onStart();
        prefs = new SharedPreferences(context);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /* Called whenever we call invalidateOptionsMenu() */
    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        // If the nav drawer is open, hide action items related to the content view
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        // ActionBarDrawerToggle will take care of this.
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        // Handle action buttons
        switch(item.getItemId()) {
            case R.id.action_websearch:
                // create intent to perform web search for this planet
                Intent intent = new Intent(Intent.ACTION_WEB_SEARCH);
                intent.putExtra(SearchManager.QUERY, supportActionBar.getTitle());
                // catch event that there's no activity to handle intent
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                } else {
                    Toast.makeText(this, R.string.app_not_available, Toast.LENGTH_LONG).show();
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectFragment(parent, position);
        }
    }


    private void selectFragment(AdapterView<?> parent, int position) {

        Fragment frag;
        FragmentManager fragmentManager = getSupportFragmentManager();

        if (parent == null) {
            frag = new FragmentProductList();
            fragmentManager.beginTransaction().replace(R.id.content_frame, frag).commit();

        } else {
            NavMenuItem item = (NavMenuItem) parent.getAdapter().getItem(position);
            mTitle = item.getName();

            if (mTitle.toString().equalsIgnoreCase("HOME")) {
                frag = new FragmentProductList();
                fragmentManager.beginTransaction().replace(R.id.content_frame, frag).commit();
            } else if (mTitle.toString().equalsIgnoreCase("ABOUT")) {
                frag = new FragmentAbout();
                fragmentManager.beginTransaction().replace(R.id.content_frame, frag).commit();
            } else if (mTitle.toString().equalsIgnoreCase("MYPROFILE")) {
                frag = new FragmentMyProfile();
                fragmentManager.beginTransaction().replace(R.id.content_frame, frag).commit();
            } else if (mTitle.toString().equalsIgnoreCase("MYORDERS")) {
                frag = new FragmentMyOrderHistory();
                fragmentManager.beginTransaction().replace(R.id.content_frame, frag).commit();
            } else if (mTitle.toString().equalsIgnoreCase("LOGOUT")) {
                confirmLogout();
            }
        }

        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
        getSupportActionBar().setTitle(mTitle);
        mDrawerLayout.closeDrawer(mDrawerList);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggls
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onBackPressed() {

    }

    private void confirmLogout() {
        AlertDialog.Builder dialog = new AlertDialog.Builder(MainActivityV7.this);
        dialog.setCancelable(false);
        dialog.setTitle("Confirm");
        dialog.setMessage("Are you sure you want to log out?");
        dialog.setPositiveButton("Log out", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                handleCustomerInfo();
            }
        });
        dialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        dialog.create().show();

    }

    private void handleCustomerInfo() {
        prefs.editMode();
        prefs.putString("customerinfo", "");
        prefs.putBoolean("rememberme", false);
        prefs.commitMode();

        Intent i = new Intent(context, LoginActivity.class);
        startActivity(i);
    }
}

