package com.gulfcouae.ordermanagement.drawer;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gulfcouae.ordermanagement.R;


/**
 * Created by relly on 7/1/15.
 */
public class NavDrawerAdapter extends ArrayAdapter<NavDrawerItemImpl> {

    private LayoutInflater inflater;

    public NavDrawerAdapter(Context context, int resource, NavDrawerItemImpl[] objects) {
        super(context, resource, objects);
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;

        NavDrawerItemImpl drawerItem = this.getItem(position);
        if (drawerItem.getType() == NavMenuItem.ITEM_TYPE) {
            view = getItemView(convertView, parent, drawerItem);
        } else {
            view = getSectionView(convertView, parent, drawerItem);
        }

        return view;
    }

    public View getItemView(View convertView, ViewGroup parentView, NavDrawerItemImpl navDrawerItem) {
        NavMenuItem menuItem = (NavMenuItem) navDrawerItem;
        NavMenuItemHolder holder = null;
        if (convertView == null) {
            if (menuItem.getName().equalsIgnoreCase("PRODUCTLIST")) {
                convertView = inflater.inflate(R.layout.navdrawer_item_welcome, parentView, false);
                TextView lblWelcome = (TextView) convertView.findViewById(R.id.lblWelcome);
                lblWelcome.setText(menuItem.getLabel());
            } else if (menuItem.getName().equalsIgnoreCase("TOLLFREE")) {
                convertView = inflater.inflate(R.layout.navdrawer_item_tollfree, parentView, false);
                TextView lblWelcome = (TextView) convertView.findViewById(R.id.lblTollFree);
                lblWelcome.setText(menuItem.getLabel());
            } else if (menuItem.getName().equalsIgnoreCase("LOGOUT")) {
                convertView = inflater.inflate(R.layout.navdrawer_item_logout, parentView, false);
                TextView lblWelcome = (TextView) convertView.findViewById(R.id.lblLogout);
                lblWelcome.setText(menuItem.getLabel());
            } else {

                convertView = inflater.inflate(R.layout.navdrawer_item, parentView, false);
                TextView labelView = (TextView) convertView.findViewById(R.id.navmenuitem_label);
                ImageView iconView = (ImageView) convertView.findViewById(R.id.navmenuitem_icon);

                holder = new NavMenuItemHolder();
                holder.labelView = labelView;
                holder.iconView = iconView;
                convertView.setTag(holder);
            }

        }

        if (!menuItem.getName().equalsIgnoreCase("PRODUCTLIST")
                && !menuItem.getName().equalsIgnoreCase("TOLLFREE")
                && !menuItem.getName().equalsIgnoreCase("LOGOUT")) {
            if (holder == null) {
                holder = (NavMenuItemHolder) convertView.getTag();
            }

            holder.labelView.setText(menuItem.getLabel());
            holder.iconView.setImageResource(menuItem.getIcon());
        }



        return convertView;
    }


    public View getSectionView(View convertView, ViewGroup parentView, NavDrawerItemImpl navDrawerItem) {
        NavMenuSection menuSection = (NavMenuSection) navDrawerItem;
        NavMenuSectionHolder holder = null;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.navdrawer_section, parentView, false);
            TextView labelView = (TextView) convertView.findViewById(R.id.navmenusection_label);

            holder = new NavMenuSectionHolder();
            holder.labelView = labelView;
            convertView.setTag(holder);
        }

        if (holder == null) {
            holder = (NavMenuSectionHolder) convertView.getTag();
        }

        holder.labelView.setText(menuSection.getLabel());

        return convertView;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        return this.getItem(position).getType();
    }

    @Override
    public boolean isEnabled(int position) {
        return getItem(position).isEnabled();
    }

    private static class NavMenuItemHolder {
        private TextView labelView;
        private ImageView iconView;
    }

    private class NavMenuSectionHolder {
        private TextView labelView;
    }
}
