package com.gulfcouae.ordermanagement;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.gulfcouae.ordermanagement.R;
import com.gulfcouae.ordermanagement.beans.Area;
import com.gulfcouae.ordermanagement.beans.Customer;
import com.gulfcouae.ordermanagement.beans.Emirate;
import com.gulfcouae.ordermanagement.interfaces.AreaImpl;
import com.gulfcouae.ordermanagement.interfaces.EmirateImpl;
import com.gulfcouae.ordermanagement.interfaces.UsernameImpl;
import com.gulfcouae.ordermanagement.sync.ServiceArea;
import com.gulfcouae.ordermanagement.sync.ServiceEmirates;
import com.gulfcouae.ordermanagement.sync.ServiceRegistration;
import com.gulfcouae.ordermanagement.sync.ServiceUsername;
import com.gulfcouae.ordermanagement.utils.FontUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by relly on 7/26/15.
 */
public class RegistrationActivity extends ActionBarActivity {

    private Context context = this;
    private EditText txtFirstName;
    private EditText txtLastName;
    private EditText txtMobile;
    private EditText txtEmail;
    private EditText txtUsername;
    private EditText txtPassword;
    private EditText txtReenterPassword;
    private Spinner cboEmirates;
    private Spinner cboArea;
    private EditText txtAddress;
    private ProgressBar pbArea;
    private ProgressBar pbUsername;

    private Button btnRegister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        FontUtils.setDefaultFont(RegistrationActivity.this);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_registration);

        ActionBar bar = getSupportActionBar();
        bar.setHomeButtonEnabled(true);
        bar.setTitle("REGISTRATION");
        bar.setDisplayHomeAsUpEnabled(true);
        bar.show();


        txtFirstName = (EditText) findViewById(R.id.txtFirstName);
        txtLastName = (EditText) findViewById(R.id.txtLastName);
        txtMobile = (EditText) findViewById(R.id.txtMobile);
        txtEmail = (EditText) findViewById(R.id.txtEmail);
        txtUsername = (EditText) findViewById(R.id.txtUsername);
        txtUsername.setOnFocusChangeListener(usernameFocusListener);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        txtReenterPassword = (EditText) findViewById(R.id.txtReenterPassword);
        txtAddress = (EditText) findViewById(R.id.txtAddress);
        cboEmirates = (Spinner) findViewById(R.id.cboEmirate);
        cboArea = (Spinner) findViewById(R.id.cboArea);
        pbArea = (ProgressBar) findViewById(R.id.pbArea);
        pbUsername = (ProgressBar) findViewById(R.id.pbUsername);

        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(btnRegisterClickListener);

        initEmirate();
    }


    private void initEmirate() {
        new ServiceEmirates(context, new EmirateImpl.Null() {
            @Override
            public void refreshList(List<Emirate> emirates) {
                if (emirates != null && emirates.size() > 0) {

                    Emirate blank = new Emirate();
                    blank.setId(0);
                    blank.setName("None");
                    emirates.add(0, blank);



                    ArrayAdapter<Emirate> adapter = new ArrayAdapter(context,
                            R.layout.dropdown_emirate,
                            emirates);
                    cboEmirates.setAdapter(adapter);
                    cboEmirates.setOnItemSelectedListener(cboEmirateSelectedListener);

                }
                initEmptyArea();
            }
        }).execute();
    }

    private void initEmptyArea() {
        Area blank = new Area();
        blank.setId(0);
        blank.setName("None");
        List<Area> areas = new ArrayList();
        areas.add(blank);

        ArrayAdapter<Area> adapter = new ArrayAdapter(RegistrationActivity.this,
                R.layout.dropdown_area,
                areas);

        cboArea.setAdapter(adapter);

    }

    private void loadAreas(Emirate emirate) {

        if (emirate != null && emirate.getId() > 0) {

            new ServiceArea(RegistrationActivity.this, pbArea, new AreaImpl() {
                @Override
                public void refreshList(List<Area> areas) {
                    if (areas != null) {
                        ArrayAdapter<Area> adapter = new ArrayAdapter(RegistrationActivity.this,
                                R.layout.dropdown_area,
                                areas);

                        cboArea.setAdapter(adapter);
                    } else {
                        initEmptyArea();
                    }

                }
            }).execute(emirate.getId());

        } else {
            initEmptyArea();
        }
    }


    AdapterView.OnItemSelectedListener cboEmirateSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
            Emirate emr = (Emirate) parent.getItemAtPosition(position);
            loadAreas(emr);
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    };

    View.OnFocusChangeListener usernameFocusListener = new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                new ServiceUsername(RegistrationActivity.this, pbUsername, new UsernameImpl() {
                    @Override
                    public void getResult(String result) {
                        if (result != null && !TextUtils.isEmpty(result)) {
                            if (result.equalsIgnoreCase(txtUsername.getText().toString().trim())) {
                                new AlertDialog.Builder(RegistrationActivity.this)
                                        .setCancelable(false)
                                        .setTitle("Warning")
                                        .setMessage("Username already exists. Please try another username.")
                                        .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                                txtUsername.requestFocus();
                                            }
                                        })
                                        .create()
                                        .show();
                            }
                        }
                    }
                }).execute(txtUsername.getText().toString().trim());
            }
        }
    };

    View.OnClickListener btnRegisterClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            StringBuilder builder = new StringBuilder();

            if (TextUtils.isEmpty(txtFirstName.getText())) {
                builder.append("First Name\n");
            }
            if (TextUtils.isEmpty(txtLastName.getText())) {
                builder.append("Last Name\n");
            }
            if (TextUtils.isEmpty(txtMobile.getText())) {
                builder.append("Mobile Number\n");
            }
            if (TextUtils.isEmpty(txtEmail.getText())) {
                builder.append("Email Address\n");
            }

            if (TextUtils.isEmpty(txtUsername.getText())) {
                builder.append("Username\n");
            }
            if (TextUtils.isEmpty(txtPassword.getText())) {
                builder.append("Password\n");
            }
            if (TextUtils.isEmpty(txtReenterPassword.getText())) {
                builder.append("Re-enter Password\n");
            }

            Emirate emirate = (Emirate) cboEmirates.getSelectedItem();
            if (emirate == null || emirate.getId() < 1) {
                builder.append("Emirate\n");
            }
            Area area = (Area) cboArea.getSelectedItem();
            if (area == null || area.getId() < 1) {
                builder.append("Area\n");
            }
            if (TextUtils.isEmpty(txtAddress.getText())) {
                builder.append("Full Address\n");
            }

            if (builder.toString().length() > 5) {
                new AlertDialog.Builder(context)
                        .setCancelable(false)
                        .setTitle("Required")
                        .setMessage("Required field(s):\n\n" + builder.toString())
                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create()
                        .show();
                return;
            }
            if (!txtPassword.getText().toString().trim().equals(txtReenterPassword.getText().toString().trim())) {
                new AlertDialog.Builder(context)
                        .setCancelable(false)
                        .setTitle("Password mismatch")
                        .setMessage("Password do not match.")
                        .setNeutralButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                dialog.dismiss();
                            }
                        })
                        .create()
                        .show();
                return;
            }

            Emirate newEmirate = new Emirate();
            newEmirate.setId(emirate.getId());
            newEmirate.setCode(emirate.getCode());
            newEmirate.setName(emirate.getName());

            Area newArea = new Area();
            newArea.setId(area.getId());
            newArea.setEmiratesId(area.getEmiratesId());
            newArea.setName(area.getName());

            Customer newCustomer = new Customer();
            newCustomer.setFirstName(txtFirstName.getText().toString());
            newCustomer.setLastName(txtLastName.getText().toString());
            newCustomer.setMobile(txtMobile.getText().toString());
            newCustomer.setEmail(txtEmail.getText().toString());
            newCustomer.setUsername(txtUsername.getText().toString().trim());
            newCustomer.setPassword(txtPassword.getText().toString().trim());
            newCustomer.setEmiratesId(((Emirate) cboEmirates.getSelectedItem()).getId());
            newCustomer.setAreaId(((Area) cboArea.getSelectedItem()).getId());
            newCustomer.setEmiratesCode(((Emirate) cboEmirates.getSelectedItem()).getCode());
            newCustomer.setEmiratesName(((Emirate) cboEmirates.getSelectedItem()).getName());
            newCustomer.setAreaName(((Area) cboArea.getSelectedItem()).getName());
            newCustomer.setAddress(txtAddress.getText().toString());
            newCustomer.setMinAmount(200.00);
            newCustomer.setType("perrier");
            newCustomer.setEmirate(newEmirate);
            newCustomer.setArea(newArea);

            new ServiceRegistration(context).execute(newCustomer);
        }
    };

}
