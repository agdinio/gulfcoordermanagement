package com.gulfcouae.ordermanagement;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.gulfcouae.ordermanagement.R;
import com.gulfcouae.ordermanagement.beans.Order;
import com.gulfcouae.ordermanagement.beans.OrderDetailsHistory;
import com.gulfcouae.ordermanagement.helpers.Currency;
import com.gulfcouae.ordermanagement.helpers.ThreadHandler;
import com.gulfcouae.ordermanagement.interfaces.BaseFragmentImpl;
import com.gulfcouae.ordermanagement.interfaces.OrderHistoryImpl;
import com.gulfcouae.ordermanagement.sync.ServiceOrderDetailsHistory;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by relly on 8/15/15.
 */
public class FragmentMyOrderDetailsHistory extends Fragment implements BaseFragmentImpl {

    private View rootView;
    private LinearLayout prodListContainer;
    private TextView txtTransNo;
    private TextView txtOrderDate;
    private TextView txtShippingAddress;
    private TextView txtTotalAmount;
    private Button btnBackToOrderHistory;
    private ScrollView rootContainer;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        ActionBar actionBar = ((ActionBarActivity)getActivity()).getSupportActionBar();
        View actionBarView = actionBar.getCustomView();
        ((TextView) actionBarView.findViewById(R.id.txtActionBarTitle)).setText(getString(R.string.actionbar_title_orderhistorydetails));

        rootView = inflater.inflate(R.layout.fragment_myorderdetailshistory, container, false);
        rootContainer = (ScrollView) rootView.findViewById(R.id.rootContainer);
        prodListContainer = (LinearLayout) rootView.findViewById(R.id.prodListContainer);
        txtTransNo = (TextView) rootView.findViewById(R.id.txtTransNo);
        txtOrderDate = (TextView) rootView.findViewById(R.id.txtOrderDate);
        txtShippingAddress = (TextView) rootView.findViewById(R.id.txtShippingAddress);
        txtTotalAmount = (TextView) rootView.findViewById(R.id.txtTotalAmount);
        btnBackToOrderHistory = (Button) rootView.findViewById(R.id.btnBackToOrderHistory);
        btnBackToOrderHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Fragment frag = new FragmentMyOrderHistory();
                getFragmentManager()
                        .beginTransaction()
                        .setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_right)
                        .replace(R.id.content_frame, frag)
                        .commit();
            }
        });

        Bundle args = getArguments();
        final Order order = (Order) args.getSerializable("order");


        new ServiceOrderDetailsHistory(rootView.getContext(), new OrderHistoryImpl.Null() {
            @Override
            public void showOrderDetailsList(final List<OrderDetailsHistory> list) {
                if (list != null && list.size() > 0) {

                    new ThreadHandler(rootView.getContext()).postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            SimpleDateFormat dateformatter = new SimpleDateFormat("MMM dd, yyyy HH:mm");

                            rootContainer.setVisibility(View.VISIBLE);
                            txtTransNo.setText(order.getTransactionNo());
                            txtOrderDate.setText(dateformatter.format(order.getOrderDate()));
                            txtShippingAddress.setText(order.getShippingAddress());

                            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);

                            double total_amount = 0;
                            for (OrderDetailsHistory o : list) {

                                LayoutInflater inflater = (LayoutInflater) rootView.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                View v = inflater.inflate(R.layout.myorderdetailshistory_list_item, null);

                                TextView txtProductName = (TextView) v.findViewById(R.id.txtProductName);
                                TextView txtQuantity = (TextView) v.findViewById(R.id.txtQuantity);
                                TextView txtPrice = (TextView) v.findViewById(R.id.txtPrice);
                                TextView txtSubTotal = (TextView) v.findViewById(R.id.txtSubTotal);
                                TextView txtUOM = (TextView) v.findViewById(R.id.txtUOM);

                                txtProductName.setText(o.getProductName());
                                txtQuantity.setText(String.valueOf(o.getOrderQuantity()));
                                txtPrice.setText(Currency.format(o.getProductPrice()));
                                txtUOM.setText(o.getUomCode());
                                txtSubTotal.setText(Currency.format(Double.parseDouble(String.valueOf(o.getOrderQuantity() * o.getProductPrice()))) );
                                total_amount += (o.getOrderQuantity() * o.getProductPrice());

                                prodListContainer.addView(v, params);
                            }

                            txtTotalAmount.setText(Currency.format(total_amount));
                        }
                    }, 500);

                } else {
                    rootContainer.setVisibility(View.INVISIBLE);
                }
            }
        }).execute(order.getId());



        return rootView;
    }

    @Override
    public void onStart() {
        super.onStart();

    }

}
