package com.gulfcouae.ordermanagement;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.gulfcouae.ordermanagement.R;
import com.gulfcouae.ordermanagement.beans.Area;
import com.gulfcouae.ordermanagement.beans.Emirate;
import com.gulfcouae.ordermanagement.interfaces.AreaImpl;
import com.gulfcouae.ordermanagement.interfaces.BaseFragmentImpl;
import com.gulfcouae.ordermanagement.interfaces.EmirateImpl;
import com.gulfcouae.ordermanagement.sync.ServiceArea;
import com.gulfcouae.ordermanagement.sync.ServiceEmirates;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by relly on 8/4/15.
 */
public class FragmentCustomerDetails extends Fragment implements
                                            BaseFragmentImpl,
                                            AdapterView.OnItemSelectedListener,
                                            View.OnClickListener {

    private View rootView;
    private EditText txtFirstName;
    private EditText txtLastName;
    private EditText txtMobile;
    private EditText txtEmail;
    private EditText txtUsername;
    private EditText txtPassword;
    private EditText txtReenterPassword;
    private Spinner cboEmirates;
    private Spinner cboArea;
    private EditText txtAddress;
    private Button btnSave;
    private ProgressBar pbArea;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_customer_details, container, false);

        txtFirstName = (EditText) rootView.findViewById(R.id.txtFirstName);
        txtLastName = (EditText) rootView.findViewById(R.id.txtLastName);
        txtMobile = (EditText) rootView.findViewById(R.id.txtMobile);
        txtEmail = (EditText) rootView.findViewById(R.id.txtEmail);
        txtUsername = (EditText) rootView.findViewById(R.id.txtUsername);
        txtPassword = (EditText) rootView.findViewById(R.id.txtPassword);
        txtReenterPassword = (EditText) rootView.findViewById(R.id.txtReenterPassword);
        txtAddress = (EditText) rootView.findViewById(R.id.txtAddress);
        cboEmirates = (Spinner) rootView.findViewById(R.id.cboEmirate);
        cboArea = (Spinner) rootView.findViewById(R.id.cboArea);
        pbArea = (ProgressBar) rootView.findViewById(R.id.pbArea);

        btnSave = (Button) rootView.findViewById(R.id.btnSave);
        btnSave.setOnClickListener(FragmentCustomerDetails.this);

        initEmirate();
        initEmptyArea();

        return rootView;
    }

    private void initEmirate() {
        new ServiceEmirates(rootView.getContext(), new EmirateImpl.Null() {
            @Override
            public void refreshList(List<Emirate> emirates) {
                if (emirates != null && emirates.size() > 0) {

                    Emirate blank = new Emirate();
                    blank.setId(0);
                    blank.setName("None");
                    emirates.add(0, blank);

                    ArrayAdapter<Emirate> adapter = new ArrayAdapter(rootView.getContext(),
                            R.layout.dropdown_emirate,
                            emirates);
                    cboEmirates.setAdapter(adapter);
                    cboEmirates.setOnItemSelectedListener(FragmentCustomerDetails.this);

                }
            }
        }).execute();
    }


    private void loadAreas(Emirate emirate) {

        if (emirate != null && emirate.getId() > 0) {

            new ServiceArea(rootView.getContext(), pbArea, new AreaImpl() {
                @Override
                public void refreshList(List<Area> areas) {
                    if (areas != null) {
                        ArrayAdapter<Area> adapter = new ArrayAdapter<Area>(rootView.getContext(),
                                android.R.layout.simple_spinner_item,
                                areas);

                        adapter.setDropDownViewResource(R.layout.dropdown_emirate);
                        cboArea.setAdapter(adapter);
                    } else {
                        Area blank = new Area();
                        blank.setName("");
                        areas = new ArrayList<Area>();
                        areas.add(blank);
                        ArrayAdapter<Area> adapter = new ArrayAdapter<Area>(rootView.getContext(),
                                android.R.layout.simple_spinner_item,
                                areas);

                        adapter.setDropDownViewResource(R.layout.dropdown_emirate);
                        cboArea.setAdapter(adapter);
                    }
                }
            }).execute(emirate.getId());

        } else {
            initEmptyArea();
        }
    }

    private void initEmptyArea() {
        Area blank = new Area();
        blank.setId(0);
        blank.setName("None");
        List<Area> areas = new ArrayList();
        areas.add(blank);

        ArrayAdapter<Area> adapter = new ArrayAdapter(rootView.getContext(),
                R.layout.dropdown_area,
                areas);

        cboArea.setAdapter(adapter);

    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Emirate emr = (Emirate) parent.getItemAtPosition(position);
        loadAreas(emr);
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onClick(View v) {

    }
}
