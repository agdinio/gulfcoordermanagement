package com.gulfcouae.ordermanagement.abstracts;

import android.os.Bundle;
import android.support.v4.app.ListFragment;

import com.gulfcouae.ordermanagement.interfaces.BackHandlerInterface;


/**
 * Created by relly on 8/10/15.
 */
public abstract class BaseFragment extends ListFragment implements BackHandlerInterface {


    protected BackHandlerInterface backHandlerInterface;
    public abstract String getTagText();
//    public abstract boolean onBackPressed();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!(getActivity() instanceof BackHandlerInterface)) {
            throw new ClassCastException("Hosting activity must implement BackHandlerInterface");
        } else {
            backHandlerInterface = (BackHandlerInterface) getActivity();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
//        backHandlerInterface.setSelectedFragment(this);
    }


}
